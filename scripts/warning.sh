#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'

printf "\n\n\n${RED}                                                           
                      ██                                          
                    ██  ██                                        
                  ██      ██                                 
                ██          ██                                    
                ██          ██                                    
              ██              ██                                  
            ██      ${YELLOW}██████${RED}      ██                                
            ██      ${YELLOW}██████${RED}      ██                                
          ██        ${YELLOW}██████${RED}        ██                              
          ██        ${YELLOW}██████${RED}        ██                              
        ██          ${YELLOW}██████${RED}          ██                            
      ██            ${YELLOW}██████${RED}            ██                          
      ██            ${YELLOW}██████${RED}            ██                          
    ██              ${YELLOW}██████${RED}              ██                        
    ██                                  ██                        
  ██                ${YELLOW}██████${RED}                ██                      
  ██                ${YELLOW}██████${RED}                ██                      
██                  ${YELLOW}██████${RED}                  ██                    
██                                          ██                    
  ██████████████████████████████████████████ "

printf "\n\n      +-------------------------------+\n"
printf "      |  ${GREEN}THIS SCRIPT HAS BEEN MOVED!  ${RED}|\n"
printf "      +-------------------------------+\n"