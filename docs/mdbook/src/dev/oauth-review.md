# Testing Oauth in a Review App

## To run a review app with oauth functionality

Simply add --oauth-review to your commit message.

## The problem:

The [OAuth Specs](https://datatracker.ietf.org/doc/html/rfc6749#section-3.1.2) state that redirect uris must be absolute. This presents a problem as, in order to truly test things in the review environment, we need to be able to test OAuth functionality, and the review apps use a random url. This prevents us from truly testing all functionality in review.

## The Solution:

We now have a special oauth review app environment and url defined in the .gitlab-ci.yaml file (see the oauth-review job). Due to the fact that each redirect-uri must be defined in the application, we only have the one environment. Therefore, it is not recommended that you use the oauth review app unless you are:

1. Working on a branch that modifies oauth functionality
1. Are testing a feature that requires oauth functionality
1. Have fully tested oauth functionality in your local development environment and are ready to have your branch peer and PO reviewed.

## The Syntax:
In order to run a merge request with an oauth enabled review app, add `--oauth-review ` to the commit message.  