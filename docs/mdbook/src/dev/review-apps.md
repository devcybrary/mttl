# Viewing Changes in Review Apps

1. When you are ready to view your changes, create a new merge request from your branch
1. Allow the pipeline to build the MTTL -  this can take 10-15 minutes
1. Once the pipeline has run successfully, go to the merge request
1. Click on "Review App" to view your app
2. ***For a `push` to a branch without a merge request*** - the URL for the review app can be found in the review stage of the pipeline

NOTE: This documentation does not appear in the local build, the only way to view this MDBook is to either use an MDBook plugin with VSCode or view it on review apps.