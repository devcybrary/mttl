# How to setup your dev environment

Recommended Tools
- Docker
- Docker-Compose
- VScode with [Remote-Container](https://code.visualstudio.com/docs/remote/containers) Extension

Essential Tools
- Git
- Bash
- Python 3.7+
- MongoDB
- NodeJS 12.x

It is recommended that development is done with the Docker environment as there are many dependencies for the various facets of the MTTL website and its constituent components. 
[Docker has seemed to work most reliably in Linux environments - however, Docker in any OS should suffice.]

The must have tools necessary to run existing scripts for this project are in the 'Essential Tools' list above. Currently, most of the scripts for the CI/CD pipeline are developed in Python, while some are done in Bash. We use mongoDB as a service in the pipeline to help streamline data manipulation and querying of the datasets containing MTTL information and relationships. NodeJS is used to serve (for dev purpose), build/precompile the Angular applications.  ExpressJS is used to allow communication from the user (i.e. Angular) with the backend i.e. Python scripts and accessing databases.


Docker images have been created to package the 'Essential Tools' to make it easier to spin up a new development environment. Docker is also necessary for the CI/CD pipeline jobs so it is guaranteed that the images are always kept up to date with development evolution. Although it is not necessary to use docker, it is safest to make sure you are using the same tool versions as used in production.

The project is configured to use the [Remote-Container](https://code.visualstudio.com/docs/remote/containers) extension for the VSCode IDE. This extension uses docker to stage your development environment automatically. 


## Ubuntu Setup
1. Install and configure Docker and Docker-Compose
```sh
sudo apt update -y && \
sudo apt install docker.io -y && \
sudo usermod -aG docker $USER && \
reboot
```
2. sudo apt-get install curl

3. Install the latest docker-compose [here](https://docs.docker.com/compose/install/)

4. Verify that your user has the appropriate permissions by running ```docker ps```. You should not see a permission error. If you do run ```sudo usermod -aG docker $USER && reboot``` again. Once rebooted, you should be able to do a ```docker ps``` and will not get a permission error.
5. Configure your global ```.gitconfig``` file. This will tag your ```git commit``` messages with your information so Gitlab understands who's contributing.
```sh
git config --global user.name [FIRST LAST]
git config --global user.email [EMAIL used for Gitlab account]
```
6. Install VSCode
```sh
sudo snap install --classic code
```
7. Install the VSCode [Remote - Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) extension
   - You can find information on VSCode extension management and marketplace [here](https://code.visualstudio.com/docs/editor/extension-gallery)
8. Clone the MTTL project using Git, then open the project in vscode and open in remote-container. Once the project is open in VSCode you will automatically be prompted to 'Reopen in Container'. Otherwise, you can click the green button on the bottom left corner and do the same. 
```sh
git clone https://gitlab.com/90cos/cyt/training-systems/angular-mttl.git && \
cd mttl && code .
```
Note: Remote-Container will automatically pull the latest MTTL and Mongo images to stage your development environment. This may take 5 or more minutes depending on your host computer. Wait patiently, this will only happen once.

## Windows Setup
Docker requires a Linux environment to run. Any Windows solutions for Docker will have a Linux hypervisor under the hood. These options are listed below.

### 1. VirtualBox
[VirtualBox](https://www.virtualbox.org/) is a virtualization product used to instantiate and manage hypervisors. If you have [HyperV](https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/about/) enabled on your windows machine it is recommended you use either [Docker Desktop](#docker-desktop) or [WSL2](#wsl2)

**Requirements**
- [System requirements](https://www.virtualbox.org/wiki/End-user_documentation) on the VirtualBox End-User docs

**Setup Steps**
1. Download and install [VirtualBox](https://www.virtualbox.org/)
2. Download the [Ubuntu Desktop](https://ubuntu.com/#download) ISO image
3. Follow the [1.7. Creating Your First Virtual Machine](https://www.virtualbox.org/manual/ch01.html#gui-createvm) and [1.8. Running Your Virtual Machine](https://www.virtualbox.org/manual/ch01.html#intro-running) to start your Ubuntu VM.

Once your Ubuntu VM is running, follow the [Ubuntu Setup](#ubuntu-setup) above.

### 2. Docker Desktop
[Docker Desktop](https://www.docker.com/) is a native Windows application that enables HyperV on Windows 10 to uses a Linux hypervisor to give the user access to docker. Official Docker Desktop documentation can be found [here](https://docs.docker.com/docker-for-windows/).

**Requirements**
- Windows 10 64-bit: Pro, Enterprise, or Education (Build 16299 or later)
- Hyper-V and Containers Windows features must be enabled

**Setup Steps**
1. Follow the setup guide on the [Docker Desktop install](https://hub.docker.com/editions/community/docker-ce-desktop-windows) page.
2. Download and install [Git](https://git-scm.com/download/win) for Windows
3. Open Powershell and configure your global ```.gitconfig``` file. This will tag your ```git commit``` messages with your information so Gitlab understands who's contributing.
```sh
git config --global user.name [FIRST LAST]
git config --global user.email [EMAIL used for Gitlab account]
```
4. Download and install [VSCode](https://code.visualstudio.com/) on Windows
5. Install the VSCode [Remote - Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) extension
   - You can find information on VSCode extension management and marketplace [here](https://code.visualstudio.com/docs/editor/extension-gallery)
6. Open Powershell and follow ```step 6``` of the [Ubuntu Setup](#ubuntu-setup) above

### 3. WSL2
Windows Subsystem for Linux v2 is a Windows 10, version 2004 and above, feature that can be enabled to give users access to a Linux kernel. From here we can install Docker Desktop and allow it to use WSL2 instead of HyperV. WSL2 uses HyperV under the hood but it can be used by both Window 10 Home and Windows 10 Pro installations.

**Requirements**
- Windows 10, updated to version 2004, Build 19041 or higher (Check your Windows version by selecting the *Windows logo key + R*, type *winver*)

**Setup Steps**
1. Follow the WSL2 [installation guide](https://docs.microsoft.com/en-us/windows/wsl/install-win10). You should use the latest LTS release of Ubuntu.
2. Follow the setup guide on the [Docker Desktop install](https://hub.docker.com/editions/community/docker-ce-desktop-windows) page. During the installation process you will see that the option to use WSL2 as a backend will be enabled by default.
3. Open the WSL2 Ubuntu terminal and configure your global ```.gitconfig``` file. This will tag your ```git commit``` messages with your information so Gitlab understands who's contributing. Git should be preinstalled, but if it it not, run ```sudo apt install git```.
```sh
git config --global user.name [FIRST LAST]
git config --global user.email [EMAIL used for Gitlab account]
```
4. Download and install [VSCode](https://code.visualstudio.com/) on Windows
5. Install the VSCode [Remote - Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) extension
   - You can find information on VSCode extension management and marketplace [here](https://code.visualstudio.com/docs/editor/extension-gallery)
6. Open the WSL2 Ubuntu terminal and follow ```step 6``` of the [Ubuntu Setup](#ubuntu-setup) above


### Next Steps
Once the Docker container is up and running in VS Code. Open a terminal with Ctrl + &#96;
More information is available in the [README](https://gitlab.com/90cos/mttl/-/blob/master/README.md) in the starting directory.  
`build_frontend.sh` will be needed to build local working copies.   
***Please be aware for security purposes - some oauth related functionality will not be available.*** 
