## Table of Contents
- [Motivation for MongoDB and Gitlab-CI](#motivation-for-mongodb-and-gitlab-ci)
  - [Data file Directory Structure](#data-file-directory-structure)
  - [Change Management Workflow](#change-management-workflow)
  - [Roadmap JSON File](#roadmap-json-file)
  - [Rel-Links JSON Files](#rel-links-json-files)
  - [Pipeline Overview](#pipeline-overview)

# Motivation for MongoDB and Gitlab-CI 
We use the Git workflow extensively throughout many of our processes. Git provides the necessary processes for transparency and agility needed for change management, distribution of work, and more. 
Our data is currently, primarily stored in a Non-Relational Mongo database.  [Relational fucntionality is slowly being incorporated with Postgresql.] Leveraging Mongo JSON files provides necessary redundancy and transparency for working across disparate teams and stakeholders. 

In short, we are using [Git](https://git-scm.com/) and [Gitlab](https://gitlab.com/) for change and project management, [Gitlab continuous integration (CI) pipelines](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/) and a disposable [MongoDB](https://www.mongodb.com/) service in the pipeline to join the data and build the presentation layer. The presentation layer is generated using Angular. To do this we must first organize the data in a logical manner to promote scalability/sustainability. 


The items below detail the requirements for our approach to making the MTTL maximally accessible, transparent, and 'agile':
1. The MTTL repo does not contain a single MTTL file. Instead, the MTTL file (mttl.min.json) is an artifact that is produced in the repo's CI/CD pipeline. 
2. The core of the MTTL repo are the [knowledge, skill, ability, and task files](#knowledge-skill-ability-and-task-json-files), and the [WORK-ROLES.json and SPECIALIZATIONS.json files](#work-roles-and-specializations-json-files) that contain a list of all work-roles and specializations.
   - The [knowledge, skill, ability, and task files](#knowledge-skill-ability-and-task-json-files) are datasets of all the knowledge, skills, abilities, tasks that are required for the organization. These items only have the KSA&T to KSA&T relationship information, topic information, requirement source information, and available training and evaluation information. Proficiency of work roles/specializations are located inside the TTL files in the 'work-roles' folder respectively.
   - The [WORK-ROLES.json and SPECIALIZATIONS.json files](#work-roles-and-specializations-json-files) are dictionaries of work role/specialization specific information that contain work role/specialization repository URLs that contain progression information for the [MTTL Training Roadmap](https://90cos-mttl.90cos.com/roadmap)

## Data file Directory Structure

```sh
MTTL Repo
├── build_frontend.sh
├── CODEOWNERS
├── CONTRIBUTING.md
├── docker-compose.yml
├── Dockerfile-dev
├── Dockerfile
├── docs
├── .gitlab-ci.yml
├── mttl
│   ├── database
│   ├── scripts
│   └── src
├── mttl.py
├── README.md
├── requirements.txt
├── scripts
└── server.js
```

- **build_frontend.sh**: *used for local development* - collates the MTTL json from disparate JSONs, generates the Angular app, and starts the Node server
-  **docker-compose.yml**: partially responsible for configuring Docker container for local development 
- **Dockerfile-dev**: partially responsible for configuring Docker container for local development 
- **Dockerfile**: defines Docker container for review and production
- **docs**: MDBooks user and developer documentation. The definitive documentation for the MTTL project. 
- **mttl**: directory housing the MTTL Angular app and scripts for App and Database functionality 
- **mttl/database/requirements**: directory with all MTTL KSAT JSON files
- **mttl/database/rel-links**: directory with training/eval relationship-link files
- **mttl/database/work-roles**: directory with work-role JSON files containing requirement mappings
- **mttl/scripts**: directory with all source CI/CD,  Python, and bash scripts
- **.gitlab-ci.yml**: Gitlab CI/CD configuration file
- **CODEOWNERS**: Gitlab ownership file to designate necessary approval for changes to specific files
- **mttl.py**: Entry point maintenance script
- **scripts**: deprecated directory maintained for legacy reasons 
- **README.md**: a quick start guide for MTTL developers and other contributors
- **requirements.txt**: Python packages required by local scripts 
- **server.js**: defines the ExpressJS API calls e.g. calls to 3rd party websites and the local database   
  
---  

## Change Management Workflow
The change management workflow is depicted below.

```mermaid
graph TD;
  NEW_REQUEST[NEW CUSTOMER REQUEST]-->|ASSIGN POC| REVIEW(REVIEW REQUEST);
  REVIEW-->CLARIFY(CLARIFY REQUEST);
  CLARIFY-->RESPONSE{CUSTOMER RESPONSE};
  RESPONSE-- Revise -->CLARIFY;
  RESPONSE-- Accept -->CREATE(CREATE MERGE REQUEST);
  CREATE-->UPDATE(MAKE UPDATES);
  UPDATE-->GOVREVIEW(GOV/CUSTOMER REVIEW);
  GOVREVIEW-->APPROVED{CHANGES APPROVED};
  APPROVED-- No -->UPDATE;
  APPROVED-- Yes -->DONE[MERGED TO MASTER]
  APPROVED-- Rejected -->DEAD[CLOSED WITHOUT MERGE]
```

## Roadmap JSON File

This section will describe the [Roadmap.json](https://gitlab.com/90cos/cyt/training-systems/angular-mttl/-/blob/master/Roadmap.json) file. This file currently house information about the work role progression within the squadron which is used to generate the [Training Roadmap](https://90cos-mttl.90cos.com/roadmap)

## Rel-Links JSON Files

This section describes the *.rel-links.json file; rel is short for relation. There are many of these files inside the 'rel-links' folder that describe the KSAT and proficiency required by training and evaluation materials. These files are used in the pipeline to create metrics and visualizations for the MTTL. The template for these json objects are in the [json_templates.py](https://gitlab.com/90cos/cyt/training-systems/angular-mttl/-/blob/master/mttl/scripts/json_templates.py) file under the `trn_rel_link_item_template()` and `evl_rel_link_item_template()` functions.

## Pipeline Overview

```mermaid
graph TD;
  START-->STAGE_VALIDATE[Stage: Validate]

  STAGE_VALIDATE-- Job -->VALIDATE_docs([Docs])
  STAGE_VALIDATE-- Job -->VALIDATE_K([Knowledge])
  STAGE_VALIDATE-- Job -->VALIDATE_S([Skills])
  STAGE_VALIDATE-- Job -->VALIDATE_A([Abilities])
  STAGE_VALIDATE-- Job -->VALIDATE_T([Tasks])
  STAGE_VALIDATE-- Job -->VALIDATE_TRN_REL_LINKS([Training Rel-Links])
  STAGE_VALIDATE-- Job -->VALIDATE_EVL_REL_LINKS([Eval Rel-Links])
  STAGE_VALIDATE-- Job -->VALIDATE_WORK_ROLES([Work-Roles])

  VALIDATE_docs-->STAGE_PRE_BUILD[Stage: Pre-Build]
  VALIDATE_K-->STAGE_PRE_BUILD[Stage: Pre-Build]
  VALIDATE_S-->STAGE_PRE_BUILD[Stage: Pre-Build]
  VALIDATE_A-->STAGE_PRE_BUILD[Stage: Pre-Build]
  VALIDATE_T-->STAGE_PRE_BUILD[Stage: Pre-Build]
  VALIDATE_TRN_REL_LINKS-->STAGE_PRE_BUILD[Stage: Pre-Build]
  VALIDATE_EVL_REL_LINKS-->STAGE_PRE_BUILD[Stage: Pre-Build]
  VALIDATE_WORK_ROLES-->STAGE_PRE_BUILD[Stage: Pre-Build]

  STAGE_PRE_BUILD-- Job -->PRE_BUILD_PREP([Build Prep])

  PRE_BUILD_PREP-->STAGE_BUILD[Stage: Build]

  STAGE_BUILD-- Job -->BUILD_MTTL([MTTL Dataset])
  STAGE_BUILD-- Job -->BUILD_TTL([TTL Dataset])
  STAGE_BUILD-- Job -->BUILD_EXTRAS([Extra Datasets])
  STAGE_BUILD-- Job -->BUILD_METRICS([MTTL Metrics])
  STAGE_BUILD-- Job -->BUILD_ROADMAP([Roadmap])
  STAGE_BUILD-- Job -->BUILD_Docs([Docs])

  BUILD_MTTL-->STAGE_TEST[Stage: Test]
  BUILD_TTL-->STAGE_TEST[Stage: Test]
  BUILD_EXTRAS-->STAGE_TEST[Stage: Test]
  BUILD_METRICS-->STAGE_TEST[Stage: Test]
  BUILD_ROADMAP-->STAGE_TEST[Stage: Test]
  BUILD_Docs-->STAGE_TEST[Stage: Test]

  STAGE_TEST-- Job -->TEST_DUPS([Test Identical KSATs])
  STAGE_TEST-- Job -->TEST_FIND([Test MTTL find])
  STAGE_TEST-- Job -->TEST_INSERT([Test MTTL insert])
  STAGE_TEST-- Job -->TEST_MODIFY([Test MTTL modify])
  STAGE_TEST-- Job -->TEST_DELETE([Test MTTL delete])
  STAGE_TEST-- Job -->TEST_PAGES([Test Pages])

  TEST_DUPS-->STAGE_DEPLOY[Stage: Deploy]
  TEST_FIND-->STAGE_DEPLOY[Stage: Deploy]
  TEST_INSERT-->STAGE_DEPLOY[Stage: Deploy]
  TEST_MODIFY-->STAGE_DEPLOY[Stage: Deploy]
  TEST_DELETE-->STAGE_DEPLOY[Stage: Deploy]
  TEST_PAGES-->STAGE_DEPLOY[Stage: Deploy]

  STAGE_DEPLOY-- Job -->DEPLOY_PAGES([Deploy to Kubernetes using GitLab-CI])

  DEPLOY_PAGES-->FINISH>User can see MTTL changes]
```