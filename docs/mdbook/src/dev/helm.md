## Custom Helm Chart for Deployment

While we do not currently utilize this feature, it is something that we are planning on using in future development. The default helm chart that is used by auto-devops is defined [here](https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/-/tree/master/assets/auto-deploy-app)