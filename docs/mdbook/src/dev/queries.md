# Basics of Mongo Querying
MongoDB queries are dictionaries of key values pairs. The key indicating the attribute and the value indicating the value of the attribute that you want documents returned to have.
### Helpful find queries
- find document by specific attribute: ```'{"_id": "5ee59b309f0e466920062099"}'```
- find document with multiple attributes: ```'{"$and":[{"_id": "5ee59b309f0e466920062099"}, {"course": "IDF"}]}'```
### Helpful update queries
- update attributes: ```'{"description": "This is my new description"}'```
- update array by pushing a new item: ```'{"$push": {"parent": "T0001"}}'```
- update array by removing an existing item: ```'{"$pull": {"parent": "T0001"}}'```
### MongoDB querying references

MongoDB find queries
- https://docs.mongodb.com/manual/reference/method/db.collection.find/

MongoDB update queries
- https://docs.mongodb.com/manual/reference/method/db.collection.update/

Add element to MongoDB document array
- https://docs.mongodb.com/manual/reference/operator/update/push/
- https://www.w3resource.com/mongodb/mongodb-array-update-operator-$push.php

Remove element from MongoDB document array
- https://docs.mongodb.com/manual/reference/operator/update/pull/
- https://www.w3resource.com/mongodb/mongodb-array-update-operator-$pull.php

Aggregation pipelines
- https://docs.mongodb.com/manual/reference/operator/aggregation-pipeline/

Aggregation to get single document array element
- https://blog.fullstacktraining.com/retrieve-only-queried-element-in-an-object-array-in-mongodb-collection/
