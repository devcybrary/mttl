## The supplementary file format 

Previously, rel-links files were utilized to map training that wasn't a part of a course. However, this led to these files erroneously being treated as courses in the data parsing.  

In order to better group similar data, the supplementary resources file format has been added. These files were converted from rel-links files using the `mttl/scripts/utils/convert_rellinks_to_supplementary.py` script.  

These files are for mapping external training that does not belong to a specific work role course.  

``` json
[
    {
        url: "https://www.google.com",
        name: "Google",
        desc: "Just Google it",
        KSATs: [
            {
                ksat_id: "K0001",
                proficiency: "1"
            },
            {
                ksat_id: "K0002",
                proficiency: "2"
            },
            {
                ksat_id: "K0003",
                proficiency: "3"
            }
        ] 
    }
]
```
