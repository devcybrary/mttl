## The Problem

Currently, our training links are all associated with courses such as IDF, PSPO, ACTP, and there is no way to add a training link to a KSAT without assigning the KSAT to a course and then mapping the link. This is likely not desired behavior. 

There are some links mapped in files such as TAE & SEE. This is a problem because there is no differentiation between a course file and relevant links, this leads to a [problem where the VTRs say that there is a TAE Course](https://gitlab.com/90cos/cyt/training-systems/vtr-system/-/issues/49).

## Links Data Structure

There are two identified options for creating a data structure that will allow us to track links mapped to KSATs - one that preserves current rel-links and works along side them and another that creates a logically distinct data structure for logically distinct data

### Option 1: A urls.rel-links.json file

This would utilize existing systems for mapping training and eval links, would reduce the amount of folders in the databases folder, but could be considered technical debt. With this solution, we would create a new rel-link file for mapping misc links. The data structure would be identical to the current rel-link data structure, but there would be some data in the data structure that we might not want.

```json
{
	"_id": {
		"$oid": "5ee9250a761cf87371cf30a4"
	},
	"KSATs": [
		{
			"ksat_id": {
				"$oid": "5f457f1e1ea90ba9adb32db8"
			},
			"item_proficiency": "3",
			"url": "https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/Pointers_Arrays/index.html"
		}
	],
	"course": "IDF",
	"module": "C-Programming",
	"topic": "Pointers_Arrays",
	"subject": "Memory Operators",
	"lecture_time": 45,
	"perf_demo_time": 60,
	"references": [
		"https://www.tutorialspoint.com/cprogramming/c_pointers.htm",
		"https://en.wikipedia.org/wiki/Magic_number_%28programming%29",
		"https://en.wikipedia.org/wiki/Hexspeak"
	],
	"lesson_objectives": []
}
```

Pros:

 - This solution would be up and running the fastest
 - We would reduce the number of folders

Cons: 
 - It's bad
 - Many of the fields required for the rel-link.json data are not required for simple link mapping
 - We would need to finagle the VTR script to ignore this file so that we do not end up with an issue like https://gitlab.com/90cos/cyt/training-systems/vtr-system/-/issues/49


### Option 2: A new data structure located elsewhere

Potentially, we could make a training links file (or several) at `./mttl/database/training-links.json`. Here is the proposed data structure

```json
[
    {
        url: "https://www.google.com",
        name: "Google",
        desc: "Just Google it",
        KSATs: [
            {
                ksat_id: "K0001",
                proficiency: "1"
            },
            {
                ksat_id: "K0002",
                proficiency: "2"
            },
            {
                ksat_id: "K0003",
                proficiency: "3"
            }
        ] 
    }
]
```

### Pros
 - Only necessary data is mapped
 - Allows for easy differentiation between course and link mappings

### Cons
 - More work needed to get data mapped into the MTTL - may need to add another pipeline job, although that will run parallel with other jobs

 ### Additional Considerations

1. Should we map proficiency data here? If our plan is to automate a lot of the process of adding a new training link, should we ask proficiency of the user? Should we ask the customer for proficiency level? Should we assign/work with the customer to assign proficiency data?

2. We could also have some sort of `type` key? This would allow us to further categorize our mappings - it could be an article, a video, an interactive exercise or even something else. Just a possible idea that could facilitate future features.
