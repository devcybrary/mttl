**Determining Data Structure for Sequences, Units, and Modules**

New naming standard: 
- Module: 
  - {mod_[0-9]{4} : Human readable title}
- Unit:
  - {unit_[0-9]{4} : Human readable title}
- Sequence: 
  - {seq_[0-9]{4} : Human readable title}

Suggested location of Sequence JSON files:
- */mttl/database/sequences/*

Suggested location of Unit JSON files:
- */mttl/database/sequences/units*

Sequences will contain a unique identifier, work role identifier, and a list of units to be completed in a sequential order.
The work role identifier will help in locating the relative data that is necessary to complete training for the desired role.
Each sequence item will contain a unique unit identifier to adhere to the new naming standard. 

An example of the json format is as follows:
```json
{
	"name": "py_seq",
	"uid": "seq_0001",
	"fullName": "Python Training Sequence",
	"description":"Training sequence designed for example",
	"work_role": "CCD",
	"sequence": 
	[
		{
			"name": "Fundamentals",
			"unit": "unit_0001",
			"seq_num": 1
		},
		{
			"name": "Design",
			"units": "unit_0002",
			"seq_num": 2
		}
	]
}```

Units will contain information breaking down the criteria to complete said unit.
Each unit may contain multiple modules to further break down the list of ksats leading to completion.
Units are relatively simple data structures only meant for categorizing modules.
Each module item will contain a unique module identifier containing all relevant ksats.

Unit data structure json example: 
```json
{
    "name": "Python Fundamentals",
    "uid": "unit_0001",
    "estimated_time_to_complete": "3h",
    "modules":
    [
        {
            "name": "Python features",
            "uid":"mod_0011"
        },
        {
            "name": "Python functions",
            "uid":"mod_0013"
        }
    ]
}```


*** Note: the sequences previously identified in the module 'pre-requisite' files can be found in `/mttl/database/modular_approach/modules/legacy_modules_with_seq_data`  ***

