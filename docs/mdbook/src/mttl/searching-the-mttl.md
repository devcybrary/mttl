# Searching the MTTL

![search](../uploads/mttl_search_bar.png)
The MTTL has *search* and *filter* options.   
  
---  
## *Search* 
Using the ***search*** field, items containing matches to the search term in description, topic, or comment fields are displayed in the table below the search bar. 
<!-- ![search](../uploads/mttl_search_results_example.png) -->
<img src="../uploads/mttl_search_results_example.png" alt="Search Bar" height="200"/>
  
--- 
## *Filter* 
There are multiple ***filter by***  options for the MTTL including: 
- Topic
- Owner
- Source
- Parent
- KSAT ID

The ***filter by** options are based on corresponding columns / fields in the MTTL database.   

After selecting a ***filter by*** option, each ***filter by*** option presents its own autopopulated list. 

For example,  **Topic** produces the following list:  
<img src="../uploads/mttl_autopopulate_topics.png" alt="Topics" width="150"/>  

