**Plan on How to Categorize VTRs into Milestones**

There is a [docx](../uploads/Plans_for_VTRs.docx) and a [PDF](../uploads/Plans_for_VTRs.pdf) available for this plan. 

**Plan on How to Categorize VTRs into Milestones**

At the highest level, VTRs will need to be organized into top level, i.e. structural or overarching, milestones. As a first step, the approximate number of KSATs would be identified. Given the relative average complexity and time expected to complete KSATs, an approximate number of KSATs appropriate to a milestone will be determined. This is not an exact number but rather an approximation.

There are many reasons that the number of KSATs per milestone should not be exact including: given the nature of learning and human behavior, the number would be arbitrary and inexact anyway; KSAT complexity and required proficiency will vary; similarly, time and effort needed to display appropriate proficiency will necessarily vary.

Consequently, the realization of KSATs into milestones will differ across work roles. For example, the Scrum Master role may have many tasks of similar complexity and time requirement and may have similarly sized milestones. By contrast, the Cyber Capabilities Developer (CCD) may have more issues of a lower complexity at a lower proficiency in initial milestones, whereas later milestones may have KSATs of greater complexity at a higher proficiency and consequently fewer such KSATs in the milestone.

Also, milestones will necessarily be conceptually and thematically organized. It would make little sense to group in unrelated KSATs. Networking KSATs would most commonly be grouped with other networking KSATs. Of course, there is overlap and no readily binary or pigeonhole categorization of KSATs required by a work role.

Additionally, milestone design should remain cognizant that where modules have been predefined, they can be used to create milestones. There will need to be a way to disaggregate module created milestones and more structural i.e. overarching milestones.

The exact milestones can and should be refined over time especially given changing requirements (including the addition, removal, and change of KSATs and expected proficiencies) as well as in response to iterative feedback from mentors and apprentices.

**The Relationship between Modules and Milestones**

The overarching milestones will be generated first when a VTR is created. These milestones are the overarching organizational topics for a work role they do not impeded nor are they impeded by the allocation of work role issues (i.e. KSATs) to a pathway module. [Modules may or may not be mapped to milestones in future - regardless there is no effect on overarching milestones. Though there is a small caveat that overarching milestones must be generated first.]

**Work Roles and Milestones**

Looking at the work role / KSAT Proficiency table, marked differences in work role mappings are readily apparent. CCD has the most mapped KSATs with 645 currently mapped KSATs; whereas, SEE has the fewest with 28 mapped KSATs. Further, many work roles only have one or no currently mapped KSATs.

| **Work Role** | **Proficiencies / KSATs** |
| --- | --- |
| CCD | Cyber Capability Developer645 &quot;work-role&quot;: &quot;CCD&quot;,328 &quot;proficiency&quot;: &quot;B&quot;120 &quot;proficiency&quot;: &quot;2&quot;75 &quot;proficiency&quot;: &quot;A&quot;69 &quot;proficiency&quot;: &quot;3&quot;34 &quot;proficiency&quot;: &quot;1&quot;19 &quot;proficiency&quot;: &quot;C&quot; |
| SCCD-L | 114 &quot;work-role&quot;: &quot;SCCD-L&quot;,45 &quot;proficiency&quot;: &quot;2&quot;28 &quot;proficiency&quot;: &quot;B&quot;20 &quot;proficiency&quot;: &quot;C&quot;9 &quot;proficiency&quot;: &quot;&quot;8 &quot;proficiency&quot;: &quot;3&quot;2 &quot;proficiency&quot;: &quot;A&quot;2 &quot;proficiency&quot;: &quot;1&quot; |
| SCCD-W | 158 &quot;work-role&quot;: &quot;SCCD-W&quot;,49 &quot;proficiency&quot;: &quot;2&quot;44 &quot;proficiency&quot;: &quot;&quot;26 &quot;proficiency&quot;: &quot;B&quot;21 &quot;proficiency&quot;: &quot;C&quot;10 &quot;proficiency&quot;: &quot;3&quot; 6 &quot;proficiency&quot;: &quot;A&quot;2 &quot;proficiency&quot;: &quot;1&quot; |
| MCCD | 30 &quot;work-role&quot;: &quot;MCCD&quot;,11 &quot;proficiency&quot;: &quot;C&quot;11 &quot;proficiency&quot;: &quot;3&quot;5 &quot;proficiency&quot;: &quot;D&quot;2 &quot;proficiency&quot;: &quot;2&quot;1 &quot;proficiency&quot;: &quot;B&quot; |
| Scrum Master | 24 &quot;work-role&quot;: &quot;SM&quot;,14 &quot;proficiency&quot;: &quot;B&quot;8 &quot;proficiency&quot;: &quot;C&quot;1 &quot;proficiency&quot;: &quot;A&quot;1 &quot;proficiency&quot;: &quot;2&quot; |
| Product Owner | 136 &quot;work-role&quot;: &quot;PO&quot;,52 &quot;proficiency&quot;: &quot;B&quot;33 &quot;proficiency&quot;: &quot;2&quot;21 &quot;proficiency&quot;: &quot;&quot;13 &quot;proficiency&quot;: &quot;A&quot;11 &quot;proficiency&quot;: &quot;C&quot;5 &quot;proficiency&quot;: &quot;3&quot;1 &quot;proficiency&quot;: &quot;1&quot; |
| TAE | 83 &quot;work-role&quot;: &quot;TAE&quot;,58 &quot;proficiency&quot;: &quot;C&quot;23 &quot;proficiency&quot;: &quot;3&quot;2 &quot;proficiency&quot;: &quot;2&quot; |
| SEE | 28 &quot;work-role&quot;: &quot;SEE&quot;,28 &quot;proficiency&quot;: &quot;3&quot; |

**Work Roles with Existing Overarching Milestone Categories**

_ **CCD Overarching Milestones** _

Currently, CCD has a framework very much aligned to overarching work role milestones. These milestones can be mapped to overarching topics identified for CCD in the IDF course.

As defined in the MTTL documentation ([https://90cos-mttl.90cos.com/documentation/files/training\_rellinks.html](https://90cos-mttl.90cos.com/documentation/files/training_rellinks.html)), the &#39;module&#39; in the rel-link.json files &quot;is the overarching unit comprised of many topically related lessons.&quot;

There are currently 10\* overarching modules identified for the CCD work role in IDF.rel-links.json. The number of topics, which are being mapped to individual _modules_, for training purposes varies. C-Programming has 18 topics that can / will be broken into milestones. For overarching organizational purposes, 18 would be too many milestones to identify as top level - however 1 would be too few as there would be a very large disparity between the C-Programming milestone and the Agile milestone. In order to balance transparency, comparability, and transparency it may be best to divide larger overarching-&#39;modules&#39; into more transparent organizational components.

| **Module** | **Number of Topics** |
| --- | --- |
| C-Programming | 18 |
| Python | 10 |
| Network Programming | 5 |
| Pseudocode | 4 |
| Assembly | 4 |
| Introduction to Git | 1 |
| Cryptography | 1 |
| Algorithms | 1 |
| Agile | 1 |
| Debugging | 1 |

The Overarching Milestones for CCD would then become:

- Agile
- Introduction to Git
- Pseudocode
- Introduction to Python
- Intermediate Python
- (Python) Networking
- Introduction to C
- Intermediate C
- C Mastery
- Assembly
- Special Topics

The subdivisions of Overarching Milestones are listed in the Overarching milestones table below.

**Overarching Milestones and Corresponding Topics for CCD**

| Overarching Milestone | Module and Topic |
| --- | --- |
| Agile | Agile&quot;agile, agile&quot; |
| Introduction to Git | Introduction to Git&quot;introduction-to-git, introduction-to-git&quot; |
| Pseudocode | Pseudocode&quot;pseudocode, Checkpoints&quot;&quot;pseudocode, Decisions&quot;&quot;pseudocode, Input\_Validation&quot;&quot;pseudocode, Pseudocode\_Functions&quot; |
| Introduction to Python | &quot;python, Data Structures&quot;&quot;python, Data\_Types&quot;&quot;python, flow\_control&quot;&quot;python, functions&quot;&quot;python, python\_features&quot; |
| Intermediate Python | &quot;python, oop&quot;&quot;python, Advanced&quot;&quot;python, Advanced&quot;&quot;python, Advanced&quot;&quot;python, Algorithms&quot; |
| (Python) Networking | &quot;network-programming, Python\_Networking&quot;&quot;network-programming, Sockets&quot;&quot;network-programming, advanced-functionality&quot;&quot;network-programming, intro-to-networking&quot;&quot;network-programming, intro-to-sockets&quot; |
| Introduction to C | &quot;C-Programming, C-Debugging&quot;&quot;C-Programming, C\_compiler&quot;&quot;C-Programming, Control\_flow&quot;&quot;C-Programming, Error\_handling&quot;&quot;C-Programming, Functions&quot;&quot;C-Programming, Introduction&quot;&quot;C-Programming, Operators\_expressions&quot;&quot;C-Programming, Variables&quot; |
| Intermediate C | &quot;C-Programming, Pointers\_Arrays&quot;&quot;C-Programming, Data\_Structures&quot;&quot;C-Programming, Array\_Strings&quot;&quot;C-Programming, Bitwise\_operators&quot; |
| C Mastery | &quot;C-Programming, IO\_Part\_1&quot;&quot;C-Programming, IO\_part\_2&quot;&quot;C-Programming, Preprocessor&quot;&quot;C-Programming, Structs&quot;&quot;C-Programming, Memory\_Management&quot;&quot;C-Programming, Multi\_Threading&quot; |
| Assembly | &quot;assembly, ASM\_Control\_flow&quot;&quot;assembly, ASM\_SystemCalls&quot;&quot;assembly, ASM\_basic\_ops&quot;&quot;assembly, Intro\_to\_ASM&quot; |
| Special Topics | &quot;Debugging, Debugging&quot;&quot;cryptography, cryptography&quot;&quot;algorithms, Intro-to-algorithms&quot; |

_ **Senior CCD Linux** __ **Overarching Milestones** _

The Senior Linux Work Role does not have rel-link.json, nor does it have modules. However, a senior Linux developer and proficiency evaluator indicated that the Senior CCD Linux work role might best be divided into the following overarching milestones:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Organizational (including Mission Process, Project Management, Software Engineering) | A0004, A0018, K0459, K0497, K0498, K0499, K0670, K0671, K0672, K0673, K0675, K0676, K0678, K0679, K0680, K0683, K0707, S0015, S0381, S0395, S0396, S0398, T0001, T0002, T0003, T0004, T0005, T0006, T0007, T0012, T0013, T0023, T0025, T0026, T0032, T0034, T0035, T0037,T0005 |
| General (including Linux System Architecture, Build Environments) | K0558, K0559, K0560, K0561, K0562, K0563, K0564, K0565, S0130, S0394, K0558, K0559, K0560, K0561, K0562, K0563, K0564, K0565 |
| User Mode (including Processes, Threads, Synchronization, memory Management, Asynchronous IO, Static and Dynamic Libraries, C APIs, Network Programming [BSD sockets and netfilter]) | S0394, K0695, K0689, K0687, K0685, K0681, K0565, K0564, K0563, K0562, K0561, K0560, K0559, K0558, K0182, K0184, T0009, K0689, T0019, S0397, K0700, K0499, K0498, K0497, K0496, K0308 |
| Kernel Mode
 (including Linux Kernel Modules, Device Drivers, Linux Security Modules, Reverse Engineering, Vulnerability Research) | T0009, T0008, K0511, K0510, K0509, K0508, K0507, K0506, A0554, A0544, A0543 |
| Reverse Engineering
 (includes Assembly) | T0017, K0691, S0105, S0130, S0133, S0135, S0137, S0140, S0142, T0021, T0020, T0018, K0694, K0693, K0692 |
| Vulnerability Research | T0036, T0022 |
| CNO(including stealth, rootkits, hooking, code injection, persistence, anti-Virus/ Self defense, Language runtime considerations) | K0704, K0705 |

**Tentative Overarching Milestone Categories**

_ **Senior CCD Windows** __ **Overarching Milestones** _

The Senior CCD Windows Work role is currently mapped to the following KSATs:

- K0071, K0182, K0184, K0308, K0318, K0459, K0483, K0484, K0485, K0486, K0488, K0489, K0490, K0491, K0492, K0493, K0494, K0541, K0599, K0600, K0601, K0602, K0603, K0604, K0605, K0606, K0607, K0608, K0609, K0670, K0671, K0672, K0673, K0675, K0676, K0678, K0679, K0680, K0681, K0682, K0683, K0684, K0685, K0687, K0688, K0689, K0690, K0691, K0692, K0693, K0694, K0695, K0700, K0701, K0702, K0703, K0704, K0705, K0707, K0708, K0709
- S0125, S0131, S0133, S0134, S0135, S0137, S0140, S0141, S0142, S0179, S0180, S0244, S0245, S0274, S0275, S0278, S0279, S0280, S0281, S0282, S0283, S0286, S0287, S0288, S0289, S0290, S0291, S0292, S0293, S0381, S0395, S0396, S0397, S0398, S0399, S0400, S0401, S0402, S0403, S0404, S0406, S0407
- A0343, A0344, A0466, A0479, A0508, A0564, A0609, A0610, A0611, A0612, A0614, A0615, A0616, A0621, A0622, A0623,, S0015, S0105
- T0001, T0002, T0003, T0004, T0005, T0006, T0007, T0008, T0009, T0010, T0011, T0012, T0013, T0014, T0015, T0016, T0017, T0018, T0019, T0020, T0021, T0022, T0023, T0024, T0025, T0026, T0027, T0028, T0029, T0030, T0031, T0032, T0033, T0034, T0035, T0036, T0037

Whereby the expectations for SCCD-W are:

| **KSAT** | **Description** | **Topic** |
| --- | --- | --- |
| T0005 | (U) Interpret customer requirements and evaluate resource and system constraints to create solution design specifications. | Agile |
| T0006 | (U) Reference capability repositories and other sources to identify existing capabilities which fully/partially meet customer requirements (with or without modification). | Agile |
| T0007 | (U) Collaborate with stakeholders to identify and/or develop appropriate cyberspace solutions. | Agile |
| K0691 | (U) Explain the purpose and usage of elements of Assembly language (e.g., x86 and x86\_64, ARM, PowerPC). | Assembly Programming |
| S0105 | Create and use C datatypes: Word, DoubleWord, and Quadword. | Assembly Programming |
| S0125 | Utilize general purpose instructions. | Assembly Programming |
| S0131 | Utilize WinDBG debugger to identify errors in a program. | Assembly Programming |
| S0133 | Utilize the stack frame | Assembly Programming |
| S0134 | Utilize registers and sub-registers | Assembly Programming |
| S0135 | Implement branch instructions. | Assembly Programming |
| S0137 | Utilize name mangling | Assembly Programming |
| S0140 | Implement protection mechanisms | Assembly Programming |
| S0141 | Execute instructions using raw opcodes | Assembly Programming |
| S0142 | Implement byte ordering | Assembly Programming |
| T0017 | (U) Analyze, modify, develop, debug and document software and applications using assembly languages. | Assembly Programming |
| K0182 | Understand how to handle race conditions in multi-threading programs. | C |
| K0184 | Understand how to implement a thread-safe multi-threading program. | C |
| K0600 | Understand basic concepts on how to remediate format string vulnerabilities | C |
| K0689 | (U) Explain the purpose and usage of elements and components of the C programming language | C |
| T0009 | (U) Analyze, modify, develop, debug and document software and applications in C programming language. | C |
| S0015 | Register a Gitlab runner. | CI/CD |
| S0395 | Configure Gitlab CI YAML files to define stages and jobs. | CI/CD |
| S0396 | Configure Gitlab CI YAML files generate and store artifacts. | CI/CD |
| S0274 | Inject Position Independent Code (PIC) into memory | CNO User Mode Development |
| S0275 | Execute Position Independent Code (PIC) | CNO User Mode Development |
| K0704 | (U) Describe common pitfalls surrounding the use, design, and implementation of cryptographic facilities. | Cryptography |
| K0705 | (U) Demonstrate understanding of pervasive cryptographic primitives. | Cryptography |
| K0672 | (U) Describe cyber mission force equipment taxonomy (Platform-Access-Payloads/Toolset), capability development process and repository | Cyber Mission Forces |
| K0308 | Knowledge of complex data structures. | Data Structures |
| K0700 | (U) Explain the concepts and terminology of data structures and associated algorithms (e.g., search, sort, traverse, insert, delete). | Data Structures |
| S0397 | Create and use complex Data Structures. | Data Structures |
| T0024 | (U) Utilize data structures to organize, sort, and manipulate elements of information | Data Structures |
| T0020 | (U) Perform static and dynamic analysis in order to find errors and flaws. | Debugging |
| T0014 | (U) Develop, modify, and utilize automation technologies to enable employment of capabilities as efficiently as possible (e.g. TDD, CI/CD, etc.) | DevOps |
| K0599 | Hacking fuzzing fundamentals | Hacking Methodologies |
| T0027 | (U) Employ tradecraft to provide confidentiality, integrity, availability, authentication, and non-repudiation of developed accesses, payloads and tools. | Implement Security Measures |
| T0028 | (U) Apply cryptography primitives to protect the confidentiality and integrity of sensitive data. | Implement Security Measures |
| T0008 | (U) Analyze, modify, develop, debug and document software and applications which run in kernel and user space. | Kernel Development |
| T0037 | (U) Document and communicate tradecraft, best practices, TTPs, training, briefings, presentations, papers, studies, lessons learned, etc. to both technical and non-technical audiences | Lessons Learned |
| K0708 | (U) List the capabilities and limitations of MetaTotal. | MetaTotal |
| K0709 | (U) List the capabilities and limitations of Mockingbird. | MetaTotal |
| S0403 | Integrate MetaTotal into CI pipeline. | MetaTotal |
| S0404 | Import MetaTotal images into OpenStack environment. | MetaTotal |
| S0406 | View and interpret MetaTotal scan results. | MetaTotal |
| S0407 | Submit files to Mockingbird. | MetaTotal |
| K0680 | (U) Describe relevant mission processes including version control processes, release processes, documentation requirements, and testing requirements. | Mission |
| T0001 | (U) Modify cyber capabilities to detect and disrupt nation-state cyber threat actors across the threat chain (kill chain) of adversary execution | Mission |
| T0002 | (U) Create or enhance cyberspace capabilities to compromise, deny, degrade, disrupt, destroy, or manipulate automated information systems. | Mission |
| T0003 | (U) Create or enhance cyberspace solutions to enable surveillance and reconnaissance of automated information systems. | Mission |
| T0004 | (U) Create or enhance cyberspace solutions to enable the defense of national interests. | Mission |
| T0011 | (U) Analyze, modify, develop, debug and document software and applications utilizing standard, non-standard, specialized, and/or unique network communication protocols. | Network Analysis/Programming |
| K0702 | (U) Describe capability OPSEC analysis and application (attribution, sanitization, etc.) | Network Security |
| K0703 | (U) Explain standard, non-standard, specialized, and/or unique communication techniques used to provide confidentiality, integrity, availability, authentication, and non-repudiation . | Network Security |
| K0071 | Describe the purpose of OSI Layer 3 | Networking Concepts |
| K0685 | (U) Explain terms and concepts of operating system fundamentals (e.g. File systems, I/O, Memory Management, Process Abstraction, etc.) | Operating System |
| K0459 | Real-Time Operations and Innovation (RTO&amp;I) Development Requirements | Organization Overview |
| K0671 | (U) Describe mission and organizational partners, including information needs, mission, structure, capabilities, etc. | Organizational Partner Info |
| K0541 | Data Structures | PowerShell injection/injection techniques |
| T0019 | (U) Make use of compiler attributes and platform-specific features. | Programming |
| K0683 | (U) Explain your organizations project management, timeline estimation, and software engineering philosophy (e.g. CI/CD, TDD, etc.) | Project Management |
| T0033 | (U) Implement project management, software engineering philosophies, modern capability development methodologies (Agile, TDD, CI/CD, etc) | Project Management |
| T0035 | (U) Enter work into Task and project management tools used for software development (e.g. Jira, Confluence, Trac, MediaWiki, etc.) | Project Management Tools |
| A0564 | Multi-threading | Python |
| K0690 | (U) Explain the purpose and usage of elements and components of the Python programming language | Python |
| T0010 | (U) Analyze, modify, develop, debug and document software and applications in Python programming language. | Python |
| A0343 | Conduct static analysis of a binary to determine its functionality. | Reverse Engineering |
| A0344 | Conduct dynamic analysis of a binary to determine its functionality. | Reverse Engineering |
| A0609 | WinDbg Dynamic Analysis | Reverse Engineering |
| A0610 | Disassemble Binary | Reverse Engineering |
| A0611 | Static Analysis | Reverse Engineering |
| A0612 | Dynamic Analysis | Reverse Engineering |
| A0614 | Disassembly and C Programming | Reverse Engineering |
| A0615 | Combine Dynamic and Static Analysis | Reverse Engineering |
| A0616 | Static/Dynamic Tools | Reverse Engineering |
| K0693 | (U) Explain what it is to decompile, disassemble, analyze, and reverse engineer compiled binaries. | Reverse Engineering |
| K0694 | (U) Identify commonly used tools to decompile, disassemble, analyze, and reverse engineer compiled binaries. | Reverse Engineering |
| S0179 | Reverse engineer a binary using IDA Pro. | Reverse Engineering |
| S0180 | Reverse engineer a binary using WinDBG. | Reverse Engineering |
| T0018 | (U) Utilize tools to decompile, disassemble, analyze, and reverse engineer compiled binaries. | Reverse Engineering |
| T0021 | (U) Conduct hardware and/or software static and dynamic analysis to reverse engineer malicious or benign systems | Reverse Engineering |
| K0701 | (U) Explain terms and concepts of secure coding practices. | Secure Coding |
| T0029 | (U) Perform code review and analysis to inform OPSEC analysis and application (attribution, sanitization, etc.) | Secure Coding |
| T0030 | (U) Produce artifacts to inform risk analysis, acceptance testing, and legal review. | Secure Coding |
| K0682 | (U) Describe modern software development methodologies (e.g. Continuous Integration (CI), Continuous Delivery (CD), Test Driven Development (TDD), etc.) | Software Development Process |
| S0381 | Peer Review code and provide critical feedback to developer. | Software Development Process |
| K0681 | (U) Explain how to apply modern software engineering practices. | Software Engineering |
| K0692 | (U) Explain the use and application of static and dynamic program analysis. | Software Engineering |
| K0695 | (U) Explain software optimization techniques. | Software Engineering |
| T0012 | (U) Analyze, modify, develop, debug and document custom interface specifications between interconnected systems. | Software Engineering |
| T0023 | (U) Design and develop data requirements, database structure, process flow, systematic procedures, algorithms, data analysis, and file structures. | Software Engineering |
| T0025 | (U) Design and develop user interfaces (e.g. web pages, GUIs, CLIs, Console Interfaces) | Software Engineering |
| T0026 | (U) Utilize secure coding techniques during development of software and applications | Software Engineering |
| T0034 | (U) Apply software engineering best practices to enable sustainability and extensibility. | Software Engineering |
| T0036 | (U) Enhance capability design strategies and tactics by synthesizing information, processes, and techniques in the areas of malicious software, vulnerabilities, reverse engineering, secure software engineering, and exploitation. | Software Engineering |
| K0318 | Knowledge of programming language structures and logic. | Software Engineering Fundamentals |
| K0687 | (U) Explain procedural and object-oriented programming paradigms. | Software Engineering Fundamentals |
| K0684 | (U) Describe principles, methodologies, and tools used to improve quality of software (e.g. regression testing, test coverage, code review, pair programming, etc.) | Software Testing |
| T0015 | (U) Build, test, and modify prototypes using representative environments. | Software Testing |
| T0016 | (U) Analyze and correct technical problems encountered during testing and implementation of cyberspace solutions. | Software Testing |
| K0688 | (U) Explain how to utilize reference documentation for C, Python, assembly, and other international technical standards and specifications (IEEE, ISO, IETF, etc.) | Standards |
| T0031 | (U) Locate and utilize technical specifications and industry standards (e.g. Internet Engineering Task Force (IETF), IEEE, IEC, International Standards Organization (ISO)). | Standards |
| K0678 | (U) Describe supported organizations requirements processes. | Supported Organization Processes |
| K0679 | (U) Describe the supported organization&#39;s approval process for operational use of a capability. | Supported Organization Processes |
| T0032 | (U) Deliver technical documentation (e.g. user guides, design documentation, test plans, change logs, training materials, etc.) to end users. | Technical Writing |
| K0670 | (U) Explain US Cyber Command authorities, responsibilities, and contributions to the National Cyber Mission. | USCC Basic Info |
| K0673 | (U) Describe USCYBERCOM legal, policy, and compliance requirements relevant to capability development. | USCC Basic Info |
| K0675 | (U) Describe sources and locations of cyber capability registries and repositories (E.g. Cyber Capability Registry (CCR), Agency and service repositories, etc.) | USCC Basic Info |
| K0676 | (U) Describe sources and locations (public and classified) of capability development TTPs and tradecraft information/intelligence used by the US Gov and others. | USCC Basic Info |
| K0707 | (U) Identify cyber development conferences used to share development and community of interest information (e.g. CNEDev, MTEM, etc.) | USCC Basic Info |
| T0013 | (U) Manage source code using version control (e.g. Git) | Version Control |
| A0621 | Implement Shell Code for Unique Exploitation Environments | Vulnerability Research |
| A0622 | Structured Exception Handling (SEH) Overwrite | Vulnerability Research |
| A0623 | Heap Spraying/Heap Corruption | Vulnerability Research |
| K0601 | Common Software Exploitation Goals | Vulnerability Research |
| K0602 | Modern Protection Mechanisms | Vulnerability Research |
| K0603 | Modern Circumvention Techniques | Vulnerability Research |
| K0604 | Heuristic Design to Achieve Sufficient Targeting Granularity | Vulnerability Research |
| K0605 | Triaging Exceptions | Vulnerability Research |
| K0606 | Heap Grooming | Vulnerability Research |
| K0607 | Return-Oriented Programming (ROP) | Vulnerability Research |
| K0608 | Defeating Address Space Layout Randomization (ASLR) | Vulnerability Research |
| K0609 | Data Execution Prevention (DEP) | Vulnerability Research |
| S0399 | Discover a basic known vulnerability in source | Vulnerability Research |
| S0400 | Implement a basic fuzzing algorithm. | Vulnerability Research |
| S0401 | Implement a simple buffer overflow. | Vulnerability Research |
| S0402 | Generate shell code. | Vulnerability Research |
| T0022 | (U) Research, reference, discover, analyze, modify, develop and document known and new vulnerabilities in computer systems. | Vulnerability Research |
| A0508 | Develop a Simple Driver | Windows Kernel Development |
| K0488 | Alternate Driver Architectures | Windows Kernel Development |
| K0489 | Advanced Configuration and Power Interface (ACPI) Plug &amp; Play | Windows Kernel Development |
| K0490 | Direct Memory Access (DMA) | Windows Kernel Development |
| K0491 | Windows Graphics Subsystem | Windows Kernel Development |
| K0492 | Shadow System Service Dispatch Table (SSDT) | Windows Kernel Development |
| K0493 | Windows Executive Systems | Windows Kernel Development |
| K0494 | Cygwin | Windows Kernel Development |
| S0278 | Debug a kernel application | Windows Kernel Development |
| S0279 | Utilize synchronization mechanisms | Windows Kernel Development |
| S0280 | Utilize paging in memory management | Windows Kernel Development |
| S0281 | Utilize Interrupt Request Levels (IRQLs) | Windows Kernel Development |
| S0282 | Utilize key Kernel Structures | Windows Kernel Development |
| S0283 | Defer kernel work to avoid deadlocks | Windows Kernel Development |
| S0286 | Manage input/output request packets (IRP) | Windows Kernel Development |
| S0287 | Utilize the input/output subsystem | Windows Kernel Development |
| S0288 | Manage Windows memory | Windows Kernel Development |
| S0289 | Utilize the Object Manager for input/output resources | Windows Kernel Development |
| S0290 | Utilize System Service Dispatch Table (SSDT) to access kernel routines. | Windows Kernel Development |
| S0291 | Analyze a Windbg Crash Dump. | Windows Kernel Development |
| S0292 | Utilize the Windows scheduler | Windows Kernel Development |
| S0293 | Interface with both documented and undocumented API&#39;s | Windows Kernel Development |
| A0466 | Develop an application using Win32 Project | Windows Operating System |
| A0479 | Develop a Win32 application that is Win64 compatible | Windows Operating System |
| K0483 | Basic Administration Using The Graphic User Interface (GUI) | Windows Operating System |
| K0484 | Basic Administration Using Command Line (CLI) | Windows Operating System |
| K0485 | Registry | Windows Operating System |
| K0486 | Vectored Exception Handling | Windows Operating System |
| S0244 | Utilize the user file Application Programming Interface (API) | Windows Operating System |
| S0245 | Utilize the Windows registry Application Programming Interface (API) | Windows Operating System |
| S0398 | Modify the Master Training Task List (MTTL). | TBD |

A preliminary organization of the SCCD-W work role into overarching milestones could resemble:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Organizational (including Mission Process, Project Management, Software Engineering) | K0459, K0670, K0671, K0672, K0673, K0675, K0676, K0678, K0679, K0680, K0682, K0683, K0684, K0707, S0015, S0381, S0395, S0396, S0398, T0001, T0002, T0003, T0004, T0005, T0006, T0007, T0012, T0013, T0023, T0025, T0026, T0032, T0034, T0035, T0037, K0681, K0687, K0688, K0689, K0691, K0692, K0695, K0700, T0014, T0015, T0016, T0031, T0033 |
| General (including Windows System Architecture, Build Environments) | K0685, A0466, A0479, K0483, K0484, K0485, K0690, T0010, K0318, T0019 |
| User Mode (including error handling, characters and strings, structured exception handling, windows processes, threads, synchronization, memory management, asynchronous IO, Windows security objects and attributes, static and dynamic libraries, Win32 APIs, Networking Programming) | A0564, K0071, K0486, S0244, S0245, K0182, K0184, K0308, K0600, S0397, T0009, T0024, S0131 |
| Kernel Mode
 (including Windows Kernel Modules, Driver Programming) | A0508, K0488, K0489, K0490, K0491, K0492, K0493, K0494, S0278, S0279, S0280, S0281, S0282, S0283, S0286, S0287, S0288, S0289, S0290, S0291, S0292, S0293, T0008 |
| Reverse Engineering
 (includes Assembly) | A0343, A0344, A0609, A0610, A0611, A0612, A0614, A0615, A0616, K0693, K0694, S0179, S0180, T0018, T0021, T0020, S0105, S0125, S0133, S0134, S0135, S0137, S0140, S0141, S0142, T0017 |
| Vulnerability Research | T0036, T0022, K0599, A0621, A0622, A0623, K0601, K0602, K0603, K0604, K0605, K0606, K0607, K0608, K0609, S0399, S0400, S0401, S0402 |
| CNO(including stealth, rootkits, hooking, code injection, persistence, anti-Virus/ Self defense, Language runtime considerations) | K0704, K0705, K0701, T0029, T0030, K0702, K0703, S0274, S0275, T0011, K0541, T0027, T0028 |
| MetaTotal | K0708, K0709, S0403, S0404, S0406, S0407 |

_ **Master CCD Overarching Milestones** _

Currently, 30 KSATs have been mapped to the MCCD work role across 19 topics.

| T0005 | (U) Interpret customer requirements and evaluate resource and system constraints to create solution design specifications. | Agile |
| --- | --- | --- |
| T0006 | (U) Reference capability repositories and other sources to identify existing capabilities which fully/partially meet customer requirements (with or without modification). | Agile |
| K0689 | (U) Explain the purpose and usage of elements and components of the C programming language | C |
| K0699 | (U) Explain data serialization formats (e.g. XML, JSON, etc.) | Data Serialization |
| T0037 | (U) Document and communicate tradecraft, best practices, TTPs, training, briefings, presentations, papers, studies, lessons learned, etc. to both technical and non-technical audiences | Lessons Learned |
| K0702 | (U) Describe capability OPSEC analysis and application (attribution, sanitization, etc.) | Network Security |
| K0696 | (U) Explain terms and concepts of networking protocols (e.g. Ethernet, IP, DHCP, ICMP, SMTP, DNS, TCP/OSI, etc.) | Networking Concepts |
| K0697 | (U) Describe and explain physical and logical network infrastructure, to include hubs, switches, routers, firewalls, etc. | Networking Concepts |
| K0698 | (U) Explain network defense mechanisms. (e.g., encryption, authentication, detection, perimeter protection). | Networking Concepts |
| K0685 | (U) Explain terms and concepts of operating system fundamentals (e.g. File systems, I/O, Memory Management, Process Abstraction, etc.) | Operating System |
| T0019 | (U) Make use of compiler attributes and platform-specific features. | Programming |
| T0033 | (U) Implement project management, software engineering philosophies, modern capability development methodologies (Agile, TDD, CI/CD, etc) | Project Management |
| K0706 | (U) Explain Task and project management tools used for software development (e.g. Jira, Confluence, Trac, MediaWiki, etc.) | Project Management |
| K0690 | (U) Explain the purpose and usage of elements and components of the Python programming language | Python |
| T0018 | (U) Utilize tools to decompile, disassemble, analyze, and reverse engineer compiled binaries. | Reverse Engineering |
| K0693 | (U) Explain what it is to decompile, disassemble, analyze, and reverse engineer compiled binaries. | Reverse Engineering |
| K0694 | (U) Identify commonly used tools to decompile, disassemble, analyze, and reverse engineer compiled binaries. | Reverse Engineering |
| T0029 | (U) Perform code review and analysis to inform OPSEC analysis and application (attribution, sanitization, etc.) | Secure Coding |
| T0030 | (U) Produce artifacts to inform risk analysis, acceptance testing, and legal review. | Secure Coding |
| T0036 | (U) Enhance capability design strategies and tactics by synthesizing information, processes, and techniques in the areas of malicious software, vulnerabilities, reverse engineering, secure software engineering, and exploitation. | Software Engineering |
| K0686 | (U) Explain basic programming concepts (e.g., levels, structures, compiled vs. interpreted languages). | Software Engineering Fundamentals |
| T0015 | (U) Build, test, and modify prototypes using representative environments. | Software Testing |
| K0684 | (U) Describe principles, methodologies, and tools used to improve quality of software (e.g. regression testing, test coverage, code review, pair programming, etc.) | Software Testing |
| T0032 | (U) Deliver technical documentation (e.g. user guides, design documentation, test plans, change logs, training materials, etc.) to end users. | Technical Writing |
| K0673 | (U) Describe USCYBERCOM legal, policy, and compliance requirements relevant to capability development. | USCC Basic Info |
| K0675 | (U) Describe sources and locations of cyber capability registries and repositories (E.g. Cyber Capability Registry (CCR), Agency and service repositories, etc.) | USCC Basic Info |
| K0676 | (U) Describe sources and locations (public and classified) of capability development TTPs and tradecraft information/intelligence used by the US Gov and others. | USCC Basic Info |
| K0707 | (U) Identify cyber development conferences used to share development and community of interest information (e.g. CNEDev, MTEM, etc.) | USCC Basic Info |
| T0022 | (U) Research, reference, discover, analyze, modify, develop and document known and new vulnerabilities in computer systems. | Vulnerability Research |
| T0038 | Create a masterpiece |
 |

These topics can be consolidated to fewer overarching milestones. Given the complexity and depth required at the master level, there are fewer KSATs per milestone. Additionally, the MCCD is still being developed.

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| USCC Basic Info | K0673, K0675, K0676, K0707, |
| Software Testing | T0015, K0684 |
| Project Management (including Agile) | T0005, T0006, T0033, K0706, T0032 |
| Secure Coding | T0029, T0030 |
| Software Engineering | K0686, T0036 |
| Vulnerability Research | T0022 |
| Reverse Engineering | T0018, K0693, K0694 |
| Cross Contextual Programming Proficiency | K0690, K0689, T0019, K0685, K0699 |
| Network Programming and Security | K0696, K0697, K0698, K0702 |
| Masterpiece | T0037, T0038 |

_ **Product Owner** __ **Overarching Milestones** _

The KSATs currently mapped to the Product Owner work role are:

| **KSAT** | **Description** | **Topic** |
| --- | --- | --- |
| K0431 | Understand different types of acceptance criteria. | 90 MQT |
| K0441 | Understand the advantages and disadvantages of using burn-down charts. | 90 MQT |
| S0361 | Explain the relationships between users stories, acceptance criteria, and the definition of done. | 90 MQT |
| S0363 | Facilitate an effective Project Kickoff Meeting in accordance with squadron standards. | 90 MQT |
| S0364 | Create a release plan. | 90 MQT |
| S0365 | Conduct an effective Product Backlog Grooming Meeting in accordance with squadron standards. | 90 MQT |
| S0366 | Facilitate an effective Sprint Planning Meeting in accordance with squadron standards. | 90 MQT |
| S0367 | Develop and present a brief for the DO meeting in accordance with squadron standards. | 90 MQT |
| S0368 | Utilize velocity data to forecast future delivery of product features. | 90 MQT |
| S0369 | Create a product burn-down chart. | 90 MQT |
| A0459 | Create, Maintain, and Refine the Product Backlog | Agile |
| K0260 | Identify steps to create or refine a product backlog item | Agile |
| K0261 | Describe Agile Development Concept | Agile |
| K0262 | Describe Scrum Terms and Methodology | Agile |
| K0366 | Describe the responsibilities of the Product Owner. | Agile |
| K0367 | Distinguish between the responsibilities of the Product Owner and the rest of the scrum team. | Agile |
| K0371 | Describe techniques to collaborate with the key stakeholders. | Agile |
| K0372 | Understand decision-making approaches a Product Owner might use. | Agile |
| K0374 | Identify the different types of project stakeholders. | Agile |
| K0376 | Identify in a scenario when the Product Owner should not act as the facilitator for the stakeholders. | Agile |
| K0377 | Identify alternatives to open discussion. | Agile |
| K0382 | Describe how the Product Owner collaborates with the Developers for activities such as backlog creation, refinement, and ordering. | Agile |
| K0383 | Define technical debt and explain why the Product Owner should be cautious about accumulating technical debt. | Agile |
| K0386 | When working with multiple teams, describe how much time a PO should spend with each team. | Agile |
| K0387 | Identify challenges of being a Product Owner with multiple teams. | Agile |
| K0392 | Understand product vision. | Agile |
| K0395 | Understand strategies for the incremental delivery of a product. | Agile |
| K0396 | Understand techniques to plan product delivery over time. | Agile |
| K0400 | Understand why a Product Owner performs discovery and validation work. | Agile |
| K0401 | Understand approaches for segmenting customers and users. | Agile |
| K0402 | Understand aspects of product discovery and identify how each contributes to successful product outcomes. | Agile |
| K0403 | Understand how to connect the Developers directly to customers and users. | Agile |
| K0404 | Identify benefits of Developers direct interactions. | Agile |
| K0406 | Understand how Scrum supports validating product assumptions. | Agile |
| K0412 | Understand the relationship between outcome and output. | Agile |
| K0413 | Understand attributes of a Product Backlog item that help assess maximizing outcome. | Agile |
| K0415 | Understand value and techniques to measure value. | Agile |
| K0416 | Describe value from the perspective of stakeholder groups. | Agile |
| K0419 | Understand criteria for ordering the Product Backlog. | Agile |
| K0420 | Identify sources of Product Backlog items. | Agile |
| K0421 | Understand techniques for generating new Product Backlog items | Agile |
| K0422 | Understand the pros and cons of a &#39;just-in-time&quot; approach for Product Backlog refinement vs. an &quot;all-at-once&quot; approach. | Agile |
| K0452 | Understand the three Scrum Team accountabilities. | Agile |
| K0453 | Describe the purpose, composition, and goal of each Agile event. | Agile |
| K0454 | Understand the six scrum principles. | Agile |
| K0455 | Define scrum. | Agile |
| K0456 | Understand the three Scrum pillars. (Transparency, inspection, and adaptation) | Agile |
| K0622 | Understand the goal of the Product Backlog Grooming Meeting. | Agile |
| K0623 | Understand the composition and purpose of the Daily Scrum Event | Agile |
| K0627 | Explain the proper structure of a User Story | Agile |
| K0649 | Outline relationship between the PO and other scrum members. | Agile |
| K0650 | Demonstrate knowledge of presentation and spreadsheet software. (PowerPoint/Excel, etc.) | Agile |
| K0651 | Illustrate ability to use software technologies that aid the role of a PO. | Agile |
| K0651 | Illustrate ability to use software technologies that aid the role of a PO. | Agile |
| K0652 | Explain Basic Facts and methodologies associated with Software testing | Agile |
| K0656 | Understand how agile process is used to mitigate risk with customer | Agile |
| K0660 | Summarize local 90 COS standard operating procedures (SOP&#39;s) for development tasks | Agile |
| K0663 | Interpret velocity and how it used to create the sprint goal. | Agile |
| K0664 | Distinguish between user stories, tasks, and subtasks. | Agile |
| K0667 | Understand Cyber Development and Front-Door policy within the 90th COS | Agile |
| K0668 | Understand the 12 principles of agile (refer to manifesto) | Agile |
| K0669 | Identify the difference between the roles of a Product Owner and a Project Manager | Agile |
| K0889 | Identify the four Agile values. | Agile |
| K0892 | Identify scrum accountabilities and responsibilities | Agile |
| K0905 | Describe the purpose and composition of an Agile Development Sprint | Agile |
| K0906 | Identify artifacts of a Sprint Planning meeting. | Agile |
| K0907 | Describe what actions to take if the Developers cannot complete work by the end of the Sprint. | Agile |
| K0924 | Know where to locate the Management and Development Processes (MADP) Agile Process Improvement Coaching (APIC) templates on Confluence. | Agile |
| K0925 | Understand the purpose and contents of a Project Charter. | Agile |
| K0927 | Understand the Agile Definition of Done and who creates it. | Agile |
| K0930 | Describe an Epic in Agile Scrum. | Agile |
| K0931 | Understand the different types of project stakeholders. | Agile |
| K0932 | Understand the purpose and contents of a Scrum team Working Agreement. | Agile |
| K0933 | Understand the purpose and goal of the Project Kickoff Meeting. | Agile |
| K0934 | Understand the purpose of the Product Road Map. | Agile |
| S0188 | Demonstrate techniques to provide transparency to stakeholders on progress toward goals. | Agile |
| S0194 | Prioritize between conflicting customer (or user) needs. | Agile |
| S0200 | Create Product Backlog item that includes description of desired outcome and value. | Agile |
| S0337 | Develop hypotheses for a target customer/user segment and create a plan to test one hypothesis. | Agile |
| S0339 | Integrate feedback to generate and order Product Backlog items. | Agile |
| S0341 | Utilize tools associated with JIRA to manage development team projects | Agile |
| S0341 | Utilize tools associated with JIRA to manage development team projects | Agile |
| S0344 | Develop and manage Epic/user story criteria | Agile |
| S0345 | Create documentation relaying leadership/customer vision | Agile |
| S0346 | Collaborate with team members to create project charter | Agile |
| S0349 | Relate and demonstrate tools commonly used by 90 COS dev teams for communication (e.g. Slack) | Agile |
| S0350 | Develop and Construct a rapport with leadership | Agile |
| S0352 | Prioritize backlog items | Agile |
| S0354 | Collaborate with tool champion to identify team requirements | Agile |
| S0356 | Formulate acceptance criteria. | Agile |
| S0357 | Apply the required principles to create a definition of done | Agile |
| S0380 | Create a sufficient number of &#39;ready&quot; Product Backlog items for the upcoming Sprint. | Agile |
| S0392 | Use the Management and Development Processes (MADP) Agile Process Improvement Coaching (APIC) templates to create a project charter. | Agile |
| S0393 | Use the Management and Development Processes (MADP) Agile Process Improvement Coaching (APIC) templates to create a project kickoff meeting agenda. | Agile |
| T0007 | (U) Collaborate with stakeholders to identify and/or develop appropriate cyberspace solutions. | Agile |
| K0466 | Cyber Mission Forces (CMF) Teams | Cyber Mission Forces |
| K0467 | 90 COS CMF-Roles | Cyber Mission Forces |
| K0468 | Interaction Between CMF Teams | Cyber Mission Forces |
| K0469 | 90 COS CMF Teams | Cyber Mission Forces |
| K0470 | 90 COS CMF Team Back-Shop Support Requests/Receipt | Cyber Mission Forces |
| K0263 | Describe Development Operations (DevOps) | Development Operations |
| S0412 | Derive and apply the proper security classification for a project from the CNF or other provided requirements. | DevOps |
| S0413 | Derive and apply the proper Security Classification Guide (SCG) for a project from the CNF or other provided requirements. | DevOps |
| K0658 | Identify features associated with Git | Git |
| A0650 | Utilize Jira for project and task management. | Jira |
| A0745 | Analyze and interpret data from Agile reports: Burndown Chart, Control Chart, Cumulative Flow Diagram, Epic Burndown, Epic Report, Sprint Report, Release Burndown, Velocity Chart, Version Report. | Jira |
| S0371 | Explain the effects of configuring estimation and time tracking on Scrum boards | Jira |
| S0372 | Explain the concepts of using epics and versions in a Software project. | Jira |
| S0373 | Describe unique characteristics of different issue types when used in a Jira Software context (epics, standard issue types, sub-task issue types). | Jira |
| S0374 | Explain and predict the impacts of modifications to an active sprint (scope changes). | Jira |
| S0375 | Explain the concept and impact of ranking issues. | Jira |
| K0457 | 90 COS Mission and Vision | Organization Overview |
| K0458 | 90 COS Organizational Structure | Organization Overview |
| K0460 | Capability Development Initiation | Organization Overview |
| K0461 | Flight Missions | Organization Overview |
| K0462 | 90 COS-Supported Organizations | Organization Overview |
| K0463 | Organizations Supporting 90 COS | Organization Overview |
| K0464 | Mission Area Responsibilities of Supported/Supporting Organizations | Organization Overview |
| K0473 | Executive Order | Policy and Compliance |
| K0474 | DoD Directive | Policy and Compliance |
| K0475 | NSA/CSS Policy | Policy and Compliance |
| K0476 | Federal Act | Policy and Compliance |
| K0477 | Signals Intelligence (SIGINT) | Policy and Compliance |
| K0478 | USSID | Policy and Compliance |
| K0479 | Classification Markings | Policy and Compliance |
| K0480 | United States Code Titles | Policy and Compliance |
| K0481 | Legal Targeting | Policy and Compliance |
| T0035 | (U) Enter work into Task and project management tools used for software development (e.g. Jira, Confluence, Trac, MediaWiki, etc.) | Project Management Tools |
| K0941 | Understand each scrum artifact&#39;s related commitment. | Scrum |
| K0942 | Understand the composition and purpose of the product goal. | Scrum |
| K0943 | Understand the purpose of an increment in scrum. | Scrum |
| K0944 | Understand the composition and purpose of a scrum team. | Scrum |
| K0945 | Understand the composition and purpose of the sprint backlog | Scrum |
| K0946 | Identify time-boxes associated with each Scrum event. | Scrum |
| K0947 | Understand the composition and capabilities of the Scrum team. | Scrum |
| K0948 | Understand the three artifacts of scrum. | Scrum |

As 85 of the issues mapped to the Product Owner work role are simply categorized as &#39;Agile&#39;, and given that there is overlap across topics (e.g. Scrum and Agile), the Agile subtopic needs to be split up.

Consequently, tentatively proposed overarch milestones for the Product Owner work role are:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| 90 MQT | K0431, K0441, S0361, S0363, S0364, S0365, S0366, S0367, S0368, S0369 |
| Agile Basics | A0459, K02611, K0382, K0622, K0623, K0627, K0395, K0668, K0889, K0905, K0906, K0927 |
| Agile Proficiency | K0377, S0357, S0356, K0392, K0396, K0422, K0649, K0652, K0663, K0644, K0925, K0933, K0934, S0346 |
| Agile for POs PT. 1 (Processes) | K0366, K0367, K0372, K0260, T0007, S0392, S0393, S0380, S0354, K0383, K0386, K0387, K0400,K0402, K0404, K0412, K0413, K0419, K0420, K0421, K0651, K0656, K0669, K0907, S0200, S0339, S0344, S0352 |
| Agile for POs PT. 2 (Stakeholders) | K0401, K0403, K0415, K0416, K0931, S0188, S0194, S0337, S0345, S0350, K0371, K0374, K0376 |
| Scrum Specifics | K0941, K0942, K0943, K0944, K0945, K0946, K0947, K0262, K0452, K0453, K0454, K0455, K0456, K0406, K0892, K0930, K0932 |
| Project Management Tools (especially Jira) | T0035, A0650, A0745, S0371, S0372, S0373, S0374, S0375, K0658, K0650, S0341, S0349 |
| 90 COS | K0457, K0458, K0460, K0461, K0667, K0462, K0463, K0464, K0660 |
| Cyber Mission Forces | K0466, K0467, K0468, K0469, K0470 |
| Policy and Compliance | K0473, K0474, K0475, K0476, K0477, K0478, K0479, K0480, K0481, S0412, S0413, K0924 |

_ **Senior Product Owner** __ **Overarching Milestones** _

The KSATs currently mapped to the Senior Product Owner work role are:

| S0339 | Integrate feedback to generate and order Product Backlog items. | Agile |
| --- | --- | --- |
| A0459 | Create, Maintain, and Refine the Product Backlog | Agile |
| A0461 | Organize and facilitate a session with stakeholders to break down a solution or feature as progressively smaller items that may be completed in Sprints. | Agile |
| K0371 | Describe techniques to collaborate with the key stakeholders. | Agile |
| K0374 | Identify the different types of project stakeholders. | Agile |
| K0376 | Identify in a scenario when the Product Owner should not act as the facilitator for the stakeholders. | Agile |
| K0378 | Identify indicators when a group is engaged in divergent thinking and indicators where a group is engaged in convergent thinking. | Agile |
| K0380 | Describe ways a group of stakeholders could reach their final decision. | Agile |
| K0379 | Describe challenges of integrating multiple perspectives. | Agile |
| K0383 | Define technical debt and explain why the Product Owner should be cautious about accumulating technical debt. | Agile |
| K0384 | Identify development practices that will help Scrum Teams deliver a high-quality product Increment and reduce technical debt each Sprint. | Agile |
| K0388 | Understand different scaling frameworks or approaches. | Agile |
| K0386 | When working with multiple teams, describe how much time a PO should spend with each team. | Agile |
| K0385 | Identify ways development practices may impact the Product Owner&#39;s ability to maximize business value each Sprint. | Agile |
| K0390 | Understand techniques for visualizing, managing, or reducing dependencies or constraints. | Agile |
| K0389 | Understand the benefits of managing dependencies when compared to reducing/removing dependencies. | Agile |
| K0397 | Developing Practical Product Strategies | Agile |
| K0398 | Discuss a real-world example of how product strategy is operationalized and evolves over time in an Agile organization. | Agile |
| K0409 | Identify biases that may impact the Product Owner&#39;s capability to effectively deliver business value. | Agile |
| K0410 | Understand approaches to validate assumptions by their cost and the quality of learning. | Agile |
| K0424 | Describe one benefit of maximizing outcomes and impact while minimizing output | Agile |
| S0187 | Explain the importance of Scrum and Product Ownership. | Agile |
| S0189 | Demonstrate techniques to interact with stakeholders over multiple Sprints. | Agile |
| S0192 | Identify purpose or define strategy. | Agile |
| S0190 | Demonstrate facilitative listening techniques. | Agile |
| S0193 | Plan a product release. | Agile |
| S0196 | Use proper techniques to connect teams directly to customers and users. | Agile |
| S0197 | Demonstrate techniques of product discovery. | Agile |
| S0198 | Visualize and communicate product strategy, product ideas, features, and assumptions. | Agile |
| S0199 | Incorporate validated assumptions into the Scrum framework. | Agile |
| S0202 | Model and measure value. | Agile |
| S0203 | Organize and filter a Product Backlog to link to product goals or strategy. | Agile |
| S0335 | Assess the skills, capabilities and practices of a Product Owner to help the organization realize value. | Agile |
| S0336 | Appraise how effectively the Sprint Review is used to inspect and adapt based on the product Increment that was built in the Sprint. | Agile |
| S0339 | Integrate feedback to generate and order Product Backlog items. | Agile |
| K0457 | 90 COS Mission and Vision | Organization Overview |
| K0460 | Capability Development Initiation | Organization Overview |
| K0458 | 90 COS Organizational Structure | Organization Overview |
| K0462 | 90 COS-Supported Organizations | Organization Overview |
| K0463 | Organizations Supporting 90 COS | Organization Overview |
| K0481 | Legal Targeting | Policy and Compliance |

The KSATs currently mapped to the Senior Product Owner work role are mapped into three overarching categories: Agile, Organizational Overview, and Policy and Compliance.

There are 34 KSATs mapped to Agile, 5 to Organization Overview, and 1 to Policy Compliance. Given the disproportionate number of Agile KSATs, the Agile topic has been split into three milestones.

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Agile 1 (Processes) | S0339, A0459, K0378, K0380, K0388, K0398, K0410, S0187, S0192, S0193, S0199, S0203, S0339 |
| Agile 2 (Leadership) | A0461, K0371, K0374, K0376, K0383, K0386, K0385, K0397, K0409, S0189, S0190, S0196, S0198 |
| Agile 3 (Troubleshooting/Efficiency) | K0379, K0384, K0390, K0389, K0424, S0197, S0202, S0335, S0336 |
| Organization Overview | K0457, K0460, K0458, K0462, K0463 |
| Policy and Compliance | K0481 |

_ **Test Automation Engineer Overarching Milestones** _

The KSATs currently mapped to the Test Automation Engineer work role are:

| A0350 | Utilize Containerization to build infrastructure as code | Containerization |
| --- | --- | --- |
| A0351 | Build a CI pipeline using containers | Development Operations |
| A0742 | Create a service in docker (http, database, etc.) with persistent storage | Docker |
| A0741 | Judge when it is appropriate to use a docker container instead of a virtual machine or bare metal installation. | Docker |
| K0836 | Summarize the purpose of Docker | Docker |
| K0845 | Identify the purpose of the EXPOSE instruction | Docker |
| K0837 | List the limitations of Docker containers | Docker |
| K0838 | Summarize Docker vs Virtual Machines | Docker |
| K0842 | Identify the purpose of the FROM instruction | Docker |
| K0840 | Summarize what a container is | Docker |
| K0839 | Summarize Docker vs other container technologies/options | Docker |
| K0846 | Identify the purpose of the ENV instruction | Docker |
| K0848 | Identify the purpose of the COPY instruction | Docker |
| K0847 | Identify the purpose of the ADD instruction | Docker |
| K0849 | Summarize the purpose of the Dockerfile | Docker |
| K0841 | Recall the difference between a docker container and a docker image | Docker |
| K0851 | Identify the purpose of the ENTRYPOINT instruction | Docker |
| K0850 | Summarize the difference between ADD and COPY instructions | Docker |
| K0843 | Identify the purpose of the RUN instruction | Docker |
| K0852 | Identify the purpose of the VOLUME instruction | Docker |
| K0844 | Identify the purpose of the CMD instruction | Docker |
| K0853 | Identify the purpose of the USER instruction | Docker |
| K0854 | Identify the purpose of the WORKDIR instruction | Docker |
| K0856 | Identify the purpose of the HEALTHCHECK instruction | Docker |
| K0855 | Identify the purpose of the ARGS instruction | Docker |
| K0857 | Identify the purpose of the SHELL instruction | Docker |
| K0860 | Identify the purpose of the &#39;login&#39; command | Docker |
| K0862 | Identify the purpose of the &#39;push&#39; CLI command | Docker |
| K0861 | Identify the purpose of the &#39;pull&#39; CLI command | Docker |
| K0863 | Identify the purpose of the &#39;rm&#39; CLI command | Docker |
| K0867 | Identify the purpose of the &#39;save&#39; CLI command | Docker |
| K0858 | Identify the purpose of the &#39;images&#39; CLI command | Docker |
| K0871 | Identify the purpose of the &#39;--name&#39; flag in the run command | Docker |
| K0869 | Identify the purpose of the &#39;history&#39; CLI command | Docker |
| K0865 | Identify the purpose of the &#39;tag&#39; CLI command | Docker |
| K0872 | Identify the purpose of the &#39;--rm&#39; flag in the run command | Docker |
| K0870 | Identify the purpose of the &#39;commit&#39; CLI command | Docker |
| K0866 | Identify the purpose of the &#39;exec&#39; CLI command | Docker |
| K0873 | Identify the purpose of the &#39;--volume&#39; flag in the run command | Docker |
| K0868 | Identify the purpose of the &#39;load&#39; CLI command | Docker |
| K0859 | Identify the purpose of the &#39;ps&#39; CLI command | Docker |
| K0875 | Identify the purpose of the &#39;-e&#39; flag in the run command | Docker |
| K0876 | Identify the purpose of the &#39;-p&#39; flag in the run command | Docker |
| K0874 | Identify the purpose of the &#39;--user&#39; flag in the run command | Docker |
| K0864 | Identify the purpose of the &#39;rmi&#39; CLI command | Docker |
| K0877 | Identify the purpose of runtime privilege flags in the run command | Docker |
| K0879 | Identify the purpose of the network settings flags in the run command | Docker |
| K0878 | Identify the purpose of the runtime constraints flags in the run command | Docker |
| K0880 | Identify docker container best practices and why they&#39;re important | Docker |
| K0884 | Recall how to interact with the docker daemon using systemd | Docker |
| K0882 | List what docker orchestration tools are available when using docker in a production environment | Docker |
| K0885 | Identify the four different security areas when considering Docker security | Docker |
| K0888 | Summarize how Docker leverages runc to provide container isolation | Docker |
| K0887 | Summarize how Docker leverages Containerd to manage container lifecycles | Docker |
| K0886 | Summarize how Docker in swarm mode can be used to scale applications | Docker |
| K0881 | Identify dockerfile best practices and why they&#39;re important | Docker |
| K0883 | Recall how to configure the docker daemon | Docker |
| S0207 | Carry out the docker build command to build a container image | Docker |
| S0206 | Design a dockerfile that meets certain requirements | Docker |
| S0208 | Carry out the docker run command to start a docker container | Docker |
| S0209 | Carry out the docker exec command to interact with an already running container | Docker |
| S0210 | Use docker documentation to solve docker problem sets | Docker |
| S0211 | Carry out the configuration of docker commands to use persistent storage | Docker |
| S0212 | Carry out the configuration of docker networking | Docker |
| K0730 | Clarify how to configure a .gitlab-ci.yml file to cache files in a GitLab pipeline | GitLab |
| A0738 | Demonstrate the ability to leverage automated testing frameworks and tools to orchestrate multiple parallel tests at scale. | Testing Process |
| A0736 | Demonstrate the ability to train team members on how to use &amp; configure automated frameworks &amp; tools. | Testing Process |
| A0739 | Demonstrate the ability to build and configure virtual machines, containers, and/or hardware systems used as target devices/systems. | Testing Process |
| A0737 | Demonstrate the ability to integrate individual tests, jobs, and/or scripts into testing frameworks, tools, and CI pipelines. | Testing Process |
| A0740 | Demonstrate the ability to design and document operationally representative environments consisting of multiple target devices and systems and necessary networking. | Testing Process |
| K0830 | Identify and understand how to utilize orchestration technologies and tools. | Testing Process |
| K0833 | Identify and describe proper data collection requirements for test scripts and plans. | Testing Process |
| K0831 | Identify and describe the differences between automation and orchestration. | Testing Process |
| K0832 | Describe the process for scheduling computing resources for systems at the 90th. | Testing Process |
| S0162 | Configure automated testing frameworks &amp; tools within a projects CI pipeline. | Testing Process |
| S0164 | Develop individual automated tests, jobs, and/or scripts to validate/satisfy acceptance criteria. | Testing Process |
| S0163 | Create automated functional tests. | Testing Process |
| S0165 | Document how automated testing satisfies test requirements within test plans and reports. | Testing Process |
| S0166 | Produce necessary artifacts to support test report generation. | Testing Process |
| S0167 | Manage and version control the automation, orchestration, and configurations for target systems and representative environments. | Testing Process |
| S0168 | Schedule DART test plan batches. | Testing Process |
| S0170 | Incorporate analysis of collected data into automated CI/CD process. | Testing Process |
| S0169 | Conduct analysis of collected data. | Testing Process |

Of the currently mapped KSATs, 62 are mapped to Docker, 18 Testing Process, 1 GitLab, 1 Development Operations, 1 Containerization.

Owing to the number of KSATs mapped to Docker, it is split into multiple overarching milestones. Further, TAE is still in development - given the expectations for TAE milestones for CI/CD, Agile, and Unit Tests (writing, revising, and reporting on) have been added.

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Testing Process | A0738, A0736, A0739, A0737, A0740, K0830, K0833, K0831, K0832, S0162, S0164, S0163, S0165, S0166, S0167, S0168, S0170, S0169 |
| DevOps / CI/CD | K0730, A0351 |
| Foundations of Containers | K0836, K0837, A0741, K0838, K0840, K0839, K0849, K0851, K0843, S0208, S0207, K0882, K0885, K0884 |
| Docker Foundations | K0845, K0846, K0848, K0847, K0841, K0850, K0852, S0209, S0211, S0212, K0844, K0853, K0854, K0856, K0855, K0857, K0860 |
| Docker Use | K0860, K0862, K0861, K0863, K0867, K0858, K0871, K0869, K0870, K0866, K0873, K0868, K0859, K0875, K0876, K0874, K0864 |
| Applied Docker | A0742, K0842, S0210, S0206, K0883, K0881, K0886, K0887, K0888, K0880, K0878, K0879, K0877 |
| Agile | TBD |
| Unit Tests (writing, revising, and reporting on) | TBD |

_ **90 COS Overarching Milestones** _

There are currently 41 mapped KSATs for expectations of all 90th COS members distributed across 17 topics: 8 Organization Overview, 5 USCC Basic Info, 4 Cyber Mission Forces, 4 Agile, 3 Project Management, 3 Policy and Compliance, 2 Threats &amp; Vulnerabilities, 2 Supported Organization Processes, 2 Risk Management, 1 Standards, 1 Project Management Tools, 1 Privacy, 1 Organizational Security, 1 Network Security, 1 Mission, 1 Development Operations, 1 CYT Requirements

| K0452 | Understand the three Scrum Team accountabilities. | Agile |
| --- | --- | --- |
| K0455 | Define scrum. | Agile |
| K0667 | Understand Cyber Development and Front-Door policy within the 90th COS | Agile |
| S0393 | Use the Management and Development Processes (MADP) Agile Process Improvement Coaching (APIC) templates to create a project kickoff meeting agenda. | Agile |
| K0466 | Cyber Mission Forces (CMF) Teams | Cyber Mission Forces |
| K0467 | 90 COS CMF-Roles | Cyber Mission Forces |
| K0468 | Interaction Between CMF Teams | Cyber Mission Forces |
| K0469 | 90 COS CMF Teams | Cyber Mission Forces |
| K0921 | Describe the software systems used by the flight to perform it&#39;s mission | CYT Requirements |
| A0359 | Utilize ticketing tool to structure team activities | Development Operations |
| K0680 | (U) Describe relevant mission processes including version control processes, release processes, documentation requirements, and testing requirements. | Mission |
| K0702 | (U) Describe capability OPSEC analysis and application (attribution, sanitization, etc.) | Network Security |
| K0457 | 90 COS Mission and Vision | Organization Overview |
| K0458 | 90 COS Organizational Structure | Organization Overview |
| K0459 | Real-Time Operations and Innovation (RTO&amp;I) Development Requirements | Organization Overview |
| K0460 | Capability Development Initiation | Organization Overview |
| K0461 | Flight Missions | Organization Overview |
| K0462 | 90 COS-Supported Organizations | Organization Overview |
| K0463 | Organizations Supporting 90 COS | Organization Overview |
| K0464 | Mission Area Responsibilities of Supported/Supporting Organizations | Organization Overview |
| A0733 | Ability to apply cybersecurity and privacy principles to organizational requirements (relevant to confidentiality, integrity, availability, authentication, non-repudiation). | Organizational Security |
| K0473 | Executive Order | Policy and Compliance |
| K0479 | Classification Markings | Policy and Compliance |
| K0481 | Legal Targeting | Policy and Compliance |
| K0313 | Knowledge of cybersecurity and privacy principles and organizational requirements (relevant to confidentiality, integrity, availability, authentication, non-repudiation). | Privacy |
| K0683 | (U) Explain your organizations project management, timeline estimation, and software engineering philosophy (e.g. CI/CD, TDD, etc.) | Project Management |
| K0706 | (U) Explain Task and project management tools used for software development (e.g. Jira, Confluence, Trac, MediaWiki, etc.) | Project Management |
| T0033 | (U) Implement project management, software engineering philosophies, modern capability development methodologies (Agile, TDD, CI/CD, etc) | Project Management |
| T0035 | (U) Enter work into Task and project management tools used for software development (e.g. Jira, Confluence, Trac, MediaWiki, etc.) | Project Management Tools |
| K0310 | Knowledge of organization&#39;s enterprise information security architecture. | Risk Management |
| K0311 | Knowledge of organization&#39;s evaluation and validation requirements. | Risk Management |
| T0031 | (U) Locate and utilize technical specifications and industry standards (e.g. Internet Engineering Task Force (IETF), IEEE, IEC, International Standards Organization (ISO)). | Standards |
| K0678 | (U) Describe supported organizations requirements processes. | Supported Organization Processes |
| K0679 | (U) Describe the supported organization&#39;s approval process for operational use of a capability. | Supported Organization Processes |
| K0307 | Knowledge of specific operational impacts of cybersecurity lapses. | Threats &amp; Vulnerabilities |
| K0345 | Knowledge of Application Security Risks (e.g. Open Web Application Security Project Top 10 list) | Threats &amp; Vulnerabilities |
| K0670 | (U) Explain US Cyber Command authorities, responsibilities, and contributions to the National Cyber Mission. | USCC Basic Info |
| K0673 | (U) Describe USCYBERCOM legal, policy, and compliance requirements relevant to capability development. | USCC Basic Info |
| K0675 | (U) Describe sources and locations of cyber capability registries and repositories (E.g. Cyber Capability Registry (CCR), Agency and service repositories, etc.) | USCC Basic Info |
| K0676 | (U) Describe sources and locations (public and classified) of capability development TTPs and tradecraft information/intelligence used by the US Gov and others. | USCC Basic Info |
| K0707 | (U) Identify cyber development conferences used to share development and community of interest information (e.g. CNEDev, MTEM, etc.) | USCC Basic Info |

These have been consolidated to a more concise number of overarching milestones.

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Organizational Overview | K0680, K0457, K0458, K0459, K0460, K0461, K0462, K0463, K0464, K0921 |
| Policy and Compliance | K0473, K0479, K0481, T0031, K0678, K0679 |
| USCC Basic Info | K0670, K0673, K0675, K0676, K0707 |
| Project Management (including Agile) | K0452, K0455, K0667, S0393, K0683, K0706, T0033, T0035, A0359 |
| OPSEC | K0702, K0313, K0310, K0311, K0307, K0345 |
| Cyber Mission Forces | K0466, K0467, K0468, K0469 |

_ **SEE Overarching Milestones** _

There are currently 28 KSATs mapped to the SEE work role. They are primarily mapped to &quot;Stan/Eval&quot; (with 1 mapped to &#39;see&#39;).

| S0213 | Ensure Examiner Go/No-Go requirements have been met | Stan/Eval |
| --- | --- | --- |
| S0214 | Ensure Examinee Go/No-Go requirements have been met | Stan/Eval |
| S0217 | Brief scenario requirements to include Go/No-Go scenario | Stan/Eval |
| S0216 | Brief the overall conduct of the evaluation to include when the evaluation begins/ends | Stan/Eval |
| S0218 | Brief grading criteria and overall grade definitions | Stan/Eval |
| S0219 | Brief examiner/examinee evaluation roles and responsibilities | Stan/Eval |
| S0220 | Identify and document all discrepancies in the appropriate evaluation area | Stan/Eval |
| S0221 | Assign proper area grades based on discrepancy documentation and criteria guidance | Stan/Eval |
| S0222 | Factor all cumulative deviations in the examinee&#39;s performance when assigning overall grade | Stan/Eval |
| S0215 | Obtain all required evaluation material | Stan/Eval |
| S0225 | Assigned sufficient additional training that will ensure examinee would achieve proper qualification level | Stan/Eval |
| S0224 | Assign additional training for discrepancies where a debrief is not the proper remedial action. | Stan/Eval |
| S0223 | Award the appropriate overall grade based on criteria guidance and governing guidance | Stan/Eval |
| S0226 | Debrief a summary of overall evaluation to include the scenario and examinee&#39;s tasks supporting the scenario | Stan/Eval |
| S0227 | Debrief all key scenario events, providing instruction and references as required | Stan/Eval |
| S0228 | Reconstruct key scenario events and examinee&#39;s role in each event | Stan/Eval |
| S0231 | Debrief overall evaluation grade | Stan/Eval |
| S0229 | Debriefed all discrepancies to include correct actions of each discrepancy | Stan/Eval |
| S0233 | Verify accuracy/completion of AF Form 4418 prior to debrief | Stan/Eval |
| S0230 | Debrief each evaluation area grade and, if warranted, additional training to include additional training due date | Stan/Eval |
| S0234 | Debrief supervisor of examinee&#39;s performance to include discrepancies, remedial action, area/overall grades, and additional training (including due date) | Stan/Eval |
| S0232 | Correctly complete evaluation gradesheet prior to debrief | Stan/Eval |
| S0237 | Evaluate all criteria areas | Stan/Eval |
| S0238 | Administer task scenario as required. | Stan/Eval |
| S0235 | Conduct evaluation as briefed to the examinee | Stan/Eval |
| S0236 | Does not detract or disrupt the examinee&#39;s performance during the evaluation | Stan/Eval |
| S0239 | If applicable, take over examinee position if/when examinee performance exhibited breaches of safety | Stan/Eval |
| T0043 | Conduct an Evaluation. | see |

These can be roughly grouped into Pre-eval, Eval, and Post-Eval (Debrief and Other).

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| **Pre-Eval** | S0213, S0214, S0217, S0216, S0218, S0219, S0225 |
| **Eval** | T0043, S0236, S0235, S0238, S0220, S0215, S0239 |
| **Post Eval - Other** | S0221, S0222, S0223, S0233, S0234, S0232, S0237 |
| **Post Eval - Debrief Tester** | S0224, S0226, S0227, S0228, S0231, S0229, S0230 |

_ **Scrum Master Overarching Milestones** _

Currently, only 24 KSATs are mapped to the Scrum Master work role across 3 topics (Scrum, Jira, and Agile).

| K0941 | Understand each scrum artifact&#39;s related commitment. | Scrum |
| --- | --- | --- |
| K0942 | Understand the composition and purpose of the product goal. | Scrum |
| K0943 | Understand the purpose of an increment in scrum. | Scrum |
| K0944 | Understand the composition and purpose of a scrum team. | Scrum |
| K0945 | Understand the composition and purpose of the sprint backlog | Scrum |
| K0946 | Identify time-boxes associated with each Scrum event. | Scrum |
| K0947 | Understand the composition and capabilities of the Scrum team. | Scrum |
| K0948 | Understand the three artifacts of scrum. | Scrum |
| A0650 | Utilize Jira for project and task management. | Jira |
| K0260 | Identify steps to create or refine a product backlog item | Agile |
| K0261 | Describe Agile Development Concept | Agile |
| K0262 | Describe Scrum Terms and Methodology | Agile |
| K0366 | Describe the responsibilities of the Product Owner. | Agile |
| K0367 | Distinguish between the responsibilities of the Product Owner and the rest of the scrum team. | Agile |
| K0395 | Understand strategies for the incremental delivery of a product. | Agile |
| K0452 | Understand the three Scrum Team accountabilities. | Agile |
| K0453 | Describe the purpose, composition, and goal of each Agile event. | Agile |
| K0454 | Understand the six scrum principles. | Agile |
| K0455 | Define scrum. | Agile |
| K0456 | Understand the three Scrum pillars. (Transparency, inspection, and adaptation) | Agile |
| K0621 | Scrum Event Distinction and Process Order | Agile |
| K0623 | Understand the composition and purpose of the Daily Scrum Event | Agile |
| K0624 | Scrum Advantages to Rapid Capability Development | Agile |
| K0668 | Understand the 12 principles of agile (refer to manifesto) | Agile |

As the Scrum master work role is being defined and mapped to KSATs, suggested overarching milestones are based upon the more thoroughly documented PO role - with the expectation for revision and elaboration.

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Agile Basics | K0366, K0367, K0395, K0453, K0668 |
| Agile Proficiency | K0261 |
| Agile for Scrum Masters | K0624 |
| Scrum Specifics | K0941, K0942, K0943, K0944, K0262, K0452, K0454, K0455, K0456, K0621, K0623 |
| Project Management Tools (especially Jira) | A0650 |
| 90 COS | TBD |
| Policy and Compliance | TBD |

_ **Senior Scrum Master Overarching Milestones** _

The Senior Scrum Master does not yet have KSATs mapped to it. Based on descriptions of the expected proficiencies of a Senior Scrum Master and the Senior PO role the milestones could be divided into:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Agile 1 (Processes) | TBD |
| Agile 2 (Leadership/Mentoring) | TBD |
| Agile 3 (Managing many Teams) | TBD |
| Organization Overview | TBD |
| Policy and Compliance | TBD |

_ **Agile Coach Overarching Milestones** _

The Agile Coach does not yet have KSATs mapped to it. Based on descriptions of the expected proficiencies of a Senior Scrum Master and the Senior PO role the milestones could be divided into:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Agile 1 (Processes) | TBD |
| Agile 2 (Leadership/Mentoring) | TBD |
| Agile 3 (Managing many Teams) | TBD |
| Organization Overview | TBD |
| Policy and Compliance | TBD |
| Affecting Organizational Change | TBD |

_**Client Systems Technician (CST) Milestones**_

The CST does not yet have KSATs mapped to it. Based on descriptions of the expected proficiencies of a CST, the overarching milestones could be divided into:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Basic Hardware | TBD |
| Basic System Administration | TBD |
| Error Identification and Troubleshooting | TBD |
| Policy and Compliance | TBD |
| Basic System Hardening | TBD |
| Basic Networking | TBD |
| OPSEC | TBD |
| 90 COS | TBD |

_ **Junior Site Reliability Engineer Overarching Milestones** _

Currently, only 12 KSATs are mapped to the JSRE work role.

| K0896 | Identify the high-level purpose of the main AWS services | Cloud Services |
| --- | --- | --- |
| K0897 | Identify the properties of virtualized compute resources (EC2, AMI, ECR, ECS, Elastic Beanstalk) | Cloud Services |
| K0898 | Identify the properties of block level storage (EBS) | Cloud Services |
| K0899 | Identify the properties of object level storage (S3) | Cloud Services |
| K0900 | Understand the relationship between VPCs, subnets, and security groups | Cloud Services |
| A0743 | Configure a virtual machine (EC2) with a block storage volume in a VPC | Cloud Services |
| A0744 | Configure object storage (S3) with correct permissions | Cloud Services |
| K0895 | Explain the steps that should be taken to mitigate potential downtime when introducing new services | Day 2 Operations Concept |
| K0901 | Explain the purpose of security risk guides (such as OWASP, etc.) and how to apply their recommendations to cloud applications | Securing Cloud Services |
| K0902 | Explain the implications of the vulnerabilities described in the OWASP top 10 | Securing Cloud Services |
| K0903 | Explain the implications of the factors in the 12 factor application methodology | Software Architecture |
| K0904 | Explain the purpose of different software architecture methodologies (12 factor, etc.) and how to apply them to cloud applications | Software Architecture |

Many additional KSATs may be mapped to the JSRE work role. Given, the current mapped KSATs and the work role description the overarching milestones could be:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Cloud Services | K0896, K0897, K0898, K0899, K0900, A0743, A0744 |
| Securing Cloud Services | K0901, K0902 |
| Day 2 Operations | K0895 |
| Software Architecture | K0903, K0904 |
| Networking Fundamentals | TBD |
| Advanced Networking / Network Security | TBD |

_**Senior Site Reliability Engineer (SSRE) Overarching Milestones**_

Currently, no KSATs have been mapped to the SSRE work role. Based on the JSRE work role and work role expectations, the overarching milestones could be:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Cloud Services | TBD |
| Securing Cloud Services | TBD |
| Day 2 Operations | TBD |
| Software Architecture | TBD |
| Networking Fundamentals | TBD |
| Advanced Networking / Network Security | TBD |
| Mentoring | TBD |

_**Special Area Developer (SAD) Overarching Milestones**_

Currently, only 2 KSATs are mapped to SAD.

| K0766 | Understand the purpose of a ROP gadget | Vulnerability Research |
| --- | --- | --- |
| S0204 | Configure Ghidra client to use scripts. | Vulnerability Research |

Given the description of the SAD work role and the 2 mapped KSATs, possible overarching milestones could include:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Vulnerability Research | K0766, S0204 |
| Remote Exploits | TBD |
| Mission | TBD |

_ **Sysadmin Overarching Milestones** _

Currently no KSATs are mapped to the sysadmin work role.

Based on work role expectations, overarching milestones could be:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Infrastructure | TBD |
| Platforms | TBD |
| Networking | TBD |
| Services | TBD |

_ **Instructor Overarching Milestones** _

Currently no KSATs are mapped to the instructor work role.

Based on work role expectations, overarching milestones could be:

| **Overarching Milestone** | **Attached KSAT Issues** |
| --- | --- |
| Pedagogy (e.g. planned pacing, scaffolding, higher level questions, etc.) | TBD |
| Learning Environment (e.g. Preparation of Materials) | TBD |
| Delivery (e.g. actual pacing, scaffolding, chunking, wait time, higher level questions, responsiveness to learners, etc.) | TBD |
| Curriculum (e.g. Syllabi and Units) | TBD |

![](RackMultipart20210616-4-ry6yve_html_237499165a11f2b9.gif)

**Plan on how to add the Milestones to the Data when adding KSATs (to VTRs)**

Tracking overarching milestones for work roles (and consequently VTRs) will require a slight modification of our JSON data to allow for overarching milestones and their proficiencies. Cf., this proposed format:

| [{&quot;ksat\_id&quot;:&quot;A0001&quot;,&quot;work-role&quot;: &quot;CCD&quot;,&quot;proficiency&quot;: &quot;3&quot;,&quot;overarching-milestone&quot;: &quot;C&quot;&quot;comments&quot;: &quot;tentative&quot;}] |
| --- |

With this change to our work role data, every KSAT added to a work role, new or otherwise, will require an overarching milestone. As requirements could be added or removed from a work role outside of KSAT creation, this change will ensure that KSATs stay mapped to overarching milestones.

When a KSAT is added, we should add the KSAT with an overarching milestone to a work role JSON.

Changes are dependent upon codeowner approval ensuring legitimacy.

As the new data format with overarching milestones is added, then the existing VTR update scripts will need to be updated to reflect this change in data structure.