# Using the Legacy MTTL

On May 17, 2021 the legacy MTTL, a static gitlab page, was replaced with a newer version with increased functionality. It is, however, still possible to pull and build the Legacy MTTL locally.

In order to download and run the Legacy MTTL, you must first follow the instructions for [setting up your development environment](../dev/set-up-env.html).

### To download the Legacy MTTL

1. Clone the MTTL Repository using git:

`git clone https://gitlab.com/90cos/mttl.git`

2. Switch to the `legacy-mttl` tagged release of the MTTL

`git checkout tags/legacy-mttl`

If everything was done correctly, you now have the source for the MTTL.

### To build locally: 
After following the instructions above:
1. Launch VSCode within the newly cloned directory and select "run in container" (this step may take a minute)
1. Press ctrl+` or select View->Terminal.

In the terminal, you will want to:
1. Run the pipeline script `./scripts/run_pipeline_scripts.sh`
1. Build and run the frontend using:
```bash 
cd frontend
npm install yarn #this may or may not be necessary 
yarn install
yarn start
```
If everything was done correctly, a local build of the MTTL should be available at http://localhost:3000/mttl  
