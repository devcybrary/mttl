# How to update performance questions with KSAT lists
If changes to the KSATs are made you will need to update the KSAT lists in the question prompts to ensure they are synchronized. This will create a doc string in testfile.py or a block comment in TestCode.c containing a list of the KSATs the question covers. If neither file is found, a text file containing the KSAT list will be created in the question's directory instead.
1. Ensure the database is running and is properly seeded with MTTL information
```
docker run -it --network host mongo

./mttl/scripts/before_script.sh
```
2. Clone repositories that need to be updated
3. Create a new branch for the changes
4. Run the prompt_ksats script on that repository's performance directory
```
python3 prompt_ksats repository/performance/
```
5. Push the changes to the repository
