#!/bin/bash

#This file ensures that serverjs keeps running after crash
while true
do
    node server.js |& tee -a log.txt
    # will only reach this point if the server crashes.
    echo "+-------------------------------------------------------+"
    echo "|     !!!!NODE SERVER CRASHED!!!!                       |"
    echo "|     RESTARTING SERVER.                                |"
    echo "|     Sending Ticket to GitLab                          |"
    echo "+-------------------------------------------------------+"
    cp log.txt ./mttl/scripts/backend
    cp log.txt crash_log.txt
    python3 ./mttl/scripts/backend/node_crashed.py
    rm ./mttl/scripts/backend/log.txt
done