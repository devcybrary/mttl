#!/usr/bin/python3

import os
import sys
import argparse
import pymongo
import subprocess
sys.path.insert(1, os.path.join(sys.path[0], './mttl/scripts/'))

global verb
verb = False

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))


def clear_mongo_database(client):
    return client.drop_database('mttl')


def before_script():
    if verb:
        print("Before Script Running...")
    cmd = 'bash ./mttl/scripts/before_script.sh'
    return subprocess.run(
        cmd.split(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )


def after_script():
    if verb:
        print("After Script Running...")
    cmd = 'bash ./mttl/scripts/after_script.sh'
    return subprocess.run(
        cmd.split(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )


def main():
    client = pymongo.MongoClient(HOST, PORT)

    cmds = ['find', 'insert', 'modify', 'delete']
    parser = argparse.ArgumentParser(
        description=f'entrance script to {"/".join(cmds)} ksat/rel-link data')
    parser.add_argument('cmd', choices=cmds,
                        help='all Commands you can run')
    parser.add_argument('args', nargs=argparse.REMAINDER,
                        help='arguments for command')
    parser.add_argument('-v', "--verbose", action="store_true",
                        help="increase output verbosity")
    parsed_args = parser.parse_args()
    verb = parsed_args.verbose

    if parsed_args.verbose:
        script = [
            "python3",
            os.path.join(
                'mttl',
                'scripts',
                'mttl_cmds',
                parsed_args.cmd + '.py')
        ] + ['-v'] + parsed_args.args
    else:
        script = [
            "python3",
            os.path.join(
                'mttl',
                'scripts',
                'mttl_cmds',
                parsed_args.cmd + '.py')
        ] + parsed_args.args
    clear_mongo_database(client)
    before_script()

    if verb:
        print("Opening Script for", parsed_args.cmd)
    script_proc = subprocess.Popen(script)
    script_proc.communicate()

    # export the changes to the ksat files
    try:
        after_script()
        if verb:
            print("success after")
    except Exception as e:
        if verb:
            print("Exception error: ", e)
            print("Exception fault after")
    exit(script_proc.returncode)


if __name__ == "__main__":
    main()
