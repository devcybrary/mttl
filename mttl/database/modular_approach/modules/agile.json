{
  "agile": {
    "uniq_id": "IDF_AGILE",
    "uniq_name": "agile",
    "description": "Understand Agile Development including the four Agile values, Agile development roles, Scrum terms and events, and User Story Development",
    "objective": "Identify how to ask a question the right way;\nIdentify steps to create or refine a product backlog item;\nDescribe Agile Development Concept;\nIdentify the four Agile values.;\nIdentify the Agile development roles;\nDescribe Scrum Terms and Methodology;\nExplain the scrum event distinction and process order;\nExplain the process of product backlog grooming;\nDemonstrate the daily scrum standup;\nSummarize Scrum Advantages to Rapid Capability Development;\nExplain the Size and Complexity of Functional Test Items;\nExplain User Story Development",
    "content": [
      {
        "description": "IDF Agile course content",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/agile/slides/#/"
      },
      {
        "description": "IDF Agile course outline",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview.html"
      },
      {
        "description": "https://www.simform.com/functional-testing-types/",
        "ref": "https://www.simform.com/functional-testing-types/"
      },
      {
        "description": "https://www.mountaingoatsoftware.com/agile/user-stories",
        "ref": "https://www.mountaingoatsoftware.com/agile/user-stories"
      },
      {
        "description": "https://39iosdev.gitlab.io/ccd-iqt/idf/agile/howtoaskquestions/Howtoaskquestions.html",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/agile/howtoaskquestions/Howtoaskquestions.html"
      },
      {
        "description": "https://39iosdev.gitlab.io/ccd-iqt/idf/agile/",
        "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/agile/"
      },
      {
        "description": "https://www.scrum.org/resources/what-is-a-daily-scrum",
        "ref": "https://www.scrum.org/resources/what-is-a-daily-scrum"
      },
      {
        "description": "https://www.romanpichler.com/blog/the-product-backlog-refinement-steps",
        "ref": "https://www.romanpichler.com/blog/the-product-backlog-refinement-steps"
      }
    ],
    "performance": [
      {
        "description": "Review question seeking strategies - describe the strategies used to complete this task",
        "perf": "https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Labs/QuestionScavengerHunt.html"
      },
      {
        "description": "Review core Agile and Scrum terms",
        "perf": "https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview_MC.html"
      },
      {
        "description": "Reflect on core Agile and Scrum terms",
        "perf": "https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Agile/Overview_Checkpoints.html"
      },
      {
        "description": " Practice creating User Stories",
        "perf": "https://39iosdev.gitlab.io/ccd-iqt/idf/agile/slides/#/9/1"
      }
    ],
    "required_tasks": [
      {
        "ksat_id": "T0005",
        "completes": false,
        "description": "(U) Interpret customer requirements and evaluate resource and system constraints to create solution design specifications."
      },
      {
        "ksat_id": "T0006",
        "completes": false,
        "description": "(U) Reference capability repositories and other sources to identify existing capabilities which fully/partially meet customer requirements (with or without modification)."
      },
      {
        "ksat_id": "T0007",
        "completes": false,
        "description": "(U) Collaborate with stakeholders to identify and/or develop appropriate cyberspace solutions."
      },
      {
        "ksat_id": "T0033",
        "completes": false,
        "description": "(U) Implement project management, software engineering philosophies, modern capability development methodologies (Agile, TDD, CI/CD, etc)."
      }
    ],
    "required_abilities": [],
    "required_skills": [],
    "required_knowledge": [
      {
        "ksat_id": "K0891",
        "completes": true,
        "description": "Review how to ask a question the right way"
      },
      {
        "ksat_id": "K0261",
        "completes": true,
        "description": "Describe the Agile Development Concept"
      },
      {
        "ksat_id": "K0889",
        "completes": true,
        "description": "Identify the four Agile values."
      },
      {
        "ksat_id": "K0260",
        "completes": true,
        "description": "Identify steps to create or refine a product backlog item"
      },
      {
        "ksat_id": "K0620",
        "completes": true,
        "description": "Agile Development accountabilities"
      },
      {
        "ksat_id": "K0621",
        "completes": true,
        "description": "Scrum Event Distinction and Process Order"
      },
      {
        "ksat_id": "K0622",
        "completes": true,
        "description": "Understand the goal of the Product Backlog Grooming Meeting."
      },
      {
        "ksat_id": "K0623",
        "completes": true,
        "description": "Understand the composition and purpose of the Daily Scrum Event"
      },
      {
        "ksat_id": "K0624",
        "completes": true,
        "description": "Scrum Advantages to Rapid Capability Development"
      },
      {
        "ksat_id": "K0626",
        "completes": true,
        "description": "Size and Complexity of Functional Test Items"
      },
      {
        "ksat_id": "K0627",
        "completes": true,
        "description": "Explain the proper structure of a User Story"
      },
      {
        "ksat_id": "K0262",
        "completes": true,
        "description": "Describe Scrum Terms and Methodology"
      }
    ],
    "resources": [
      {
        "title": "What is RTFM?",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "http://www.catb.org/jargon/html/R/RTFM.html"
      },
      {
        "title": "What is scrum?",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://digital.ai/resources/agile-101/what-is-scrum"
      },
      {
        "title": "Stack Overflow guide to asking questions",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://stackoverflow.com/help/how-to-ask"
      },
      {
        "title": "Understanding user stories in Agile",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.mountaingoatsoftware.com/agile/user-stories"
      },
      {
        "title": "Be agile vs. do Agile",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://medium.com/@Intersog/how-being-agile-is-different-from-doing-agile-9098e8b679f1"
      },
      {
        "title": "Functional testing",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.simform.com/functional-testing-types/"
      },
      {
        "title": "The Agile Manifesto",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.agilemanifesto.org"
      },
      {
        "title": "What is a daily scrum?",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.scrum.org/resources/what-is-a-daily-scrum"
      },
      {
        "title": "Agile 101 Website",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.agilealliance.org/agile101/"
      },
      {
        "title": "Why asking good questions is important",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://en.wikipedia.org/wiki/Eternal_September"
      },
      {
        "title": "Steps to backlog refinement",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.romanpichler.com/blog/the-product-backlog-refinement-steps"
      },
      {
        "title": "Understanding Agile",
        "type": "reference",
        "description": "Reference from IDF",
        "url": "https://www.mountaingoatsoftware.com/agile"
      }
    ],
    "comments": "generated by rellink to module script",
    "assessment": [
      {
        "description": "IDF Agile final assessment",
        "eval": "https://39iosdev.gitlab.io/ccd-iqt/idf/agile/Labs/AgileLab1.html"
      }
    ]
  }
}
