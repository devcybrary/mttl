{
    "ASM_Control_flow": {
        "uniq_id": "IDF_ASM_control_flow",
        "uniq_name": "ASM_Control_flow",
        "description": "Understand and utilize flags in Assembly to solve relevant problems.;\nUnderstand and utilize flags in Assembly to solve relevant problems.;\nIdentify, differentiate, and leverage string functions in Assembly.;\nDifferentiate and implement conditional and unconditional control flow in Assembly.;\nDifferentiate function call syntaxes and accompanying registers across OSes and architectures",
        "workroles": ["CCD"],
        "group": "assembly",
        "objective": "Utilize common string instructions in Assembly.;\nLeverage conditional branching to solve problems in Assembly.;\nIn Assembly, access predefined external utility functions.;\nIn Assembly, use name mangling to create implement functions.",
        "prerequisites": [
            { "uniq_id": "IDF_ASM_CMP_Basic"},
            { "uniq_id": "ASM_basic_ops"}
        ],
        "content": [
            {
                "description": "Understanding and using flags in Assembly ",
                "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/Flags.html"
            },
            {
                "description": "Understanding and using control flow e.g., call, ret, cmp, jcc, etc. in Assembly",
                "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/index.html"
            },
            {
                "description": "Understanding and using string instructions in Assembly",
                "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/Strings_Calls.html"
            },
            {
                "description": "Calling conventions, name mangling, etc. for Microsoft and Sys V in Assembly",
                "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/ASM_Control_Flow/Calls.html"
            }
        ],
        "performance": [
            {
                "description": "Leverage conditional branching to solve problems in Assembly.",
                "perf": "https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_6/Lab6.nasm"
            },
            {
                "description": "Utilize common string instructions in Assembly.",
                "perf": "https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_7/Lab7.nasm"
            },
            {
                "description": "In Assembly, understand name mangling and access predefined external utility functions.",
                "perf": "https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_9/Lab9.nasm"
            }
        ],
        "required_tasks": [
            {
                "ksat_id": "T0017",
                "proficiency": "1",
                "description": "(U) Analyze, modify, develop, debug and document software and applications using assembly languages."
            }
        ],
        "required_abilities": [],
        "required_skills": [
            {
                "ksat_id": "S0177",
                "proficiency": "2",
                "description": "Write assembly code that accomplishes the same functionality as given in a high level language (C or Python) code snippet."
            },
            {
                "ksat_id": "S0117",
                "proficiency": "2",
                "description": "Utilize labels for control flow."
            },
            {
                "ksat_id": "S0128",
                "proficiency": "2",
                "description": "Utilize common conditional instructions."
            },
            {
                "ksat_id": "S0121",
                "proficiency": "2",
                "description": "Use pre-defined utility functions."
            }
        ],
        "required_knowledge": [
            {
                "ksat_id": "K0231",
                "proficiency": "B",
                "description": "Relationship to functions"
            },
            {
                "ksat_id": "K0776",
                "proficiency": "B",
                "description": "Understand the purpose of the call instruction."
            },
            {
                "ksat_id": "K0777",
                "proficiency": "B",
                "description": "Understand the purpose of the ret instruction."
            },
            {
                "ksat_id": "K0772",
                "proficiency": "B",
                "description": "Understand stack instructions."
            },
            {
                "ksat_id": "K0794",
                "proficiency": "B",
                "description": "Understand the purpose of the cmp instruction."
            },
            {
                "ksat_id": "K0786",
                "proficiency": "B",
                "description": "Understand logical instructions."
            },
            {
                "ksat_id": "K0796",
                "proficiency": "B",
                "description": "Understand the purpose of the jcc and other conditional jump instructions."
            },
            {
                "ksat_id": "K0797",
                "proficiency": "B",
                "description": "Understand the purpose of the loop instruction."
            },
            {
                "ksat_id": "K0811",
                "proficiency": "B",
                "description": "Describe how to utilize labels."
            },
            {
                "ksat_id": "K0812",
                "proficiency": "B",
                "description": "Understand how to implement conditional control flow."
            },
            {
                "ksat_id": "K0237",
                "proficiency": "B",
                "description": "With references and required resources, describe the purpose and use of the various system flag registers."
            },
            {
                "ksat_id": "K0238",
                "proficiency": "B",
                "description": "How assembly programs set flags"
            },
            {
                "ksat_id": "K0239",
                "proficiency": "B",
                "description": "How to manually set the flags"
            },
            {
                "ksat_id": "K0810",
                "proficiency": "B",
                "description": "Understand how to utilize flags."
            },
            {
                "ksat_id": "K0240",
                "proficiency": "B",
                "description": "Relation to conditional branch instructions"
            },
            {
                "ksat_id": "K0798",
                "proficiency": "B",
                "description": "Understand string instructions."
            },
            {
                "ksat_id": "K0806",
                "proficiency": "B",
                "description": "Understand the purpose of the std instruction."
            },
            {
                "ksat_id": "K0807",
                "proficiency": "B",
                "description": "Understand the purpose of the cld instruction."
            },
            {
                "ksat_id": "K0800",
                "proficiency": "B",
                "description": "Understand the purpose of the lods instruction."
            },
            {
                "ksat_id": "K0767",
                "proficiency": "B",
                "description": "Understand move instructions."
            },
            {
                "ksat_id": "K0220",
                "proficiency": "2",
                "description": "Identify the underlying restrictions to low level memory access"
            },
            {
                "ksat_id": "K0233",
                "proficiency": "B",
                "description": "Calling conventions"
            },
            {
                "ksat_id": "K0252",
                "proficiency": "A",
                "description": "With references and required resources, describe the purpose and use of memory segmentation and how a system or process' memory is laid into a section."
            },
            {
                "ksat_id": "K0254",
                "proficiency": "B",
                "description": "With references and required resources, describe the purpose and use of SIMD instructions. Utilize use of registers and instructions that can operate on multiple data items at once."
            },
            {
                "ksat_id": "K0255",
                "proficiency": "B",
                "description": "XMM0-XMM7"
            },
            {
                "ksat_id": "K0232",
                "proficiency": "B",
                "description": "With references and required resources, describe the purpose and use of functions with respect to use in assembly."
            },
            {
                "ksat_id": "K0236",
                "proficiency": "B",
                "description": "Name mangling"
            },
            {
                "ksat_id": "K0813",
                "proficiency": "B",
                "description": "Understand how to create and implement functions."
            },
            {
                "ksat_id": "K0815",
                "proficiency": "B",
                "description": "Understand how to use pre-defined utility functions."
            },
            {
                "ksat_id": "K0253",
                "proficiency": "A",
                "description": "Memory segmentation/process memory"
            }
        ],
        "resources": [
            {
                "title": "https://www.amd.com/system/files/TechDocs/24594.pdf",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://www.amd.com/system/files/TechDocs/24594.pdf"
            },
            {
                "title": "https://eli.thegreenplace.net/2011/09/06/stack-frame-layout-on-x86-64/",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://eli.thegreenplace.net/2011/09/06/stack-frame-layout-on-x86-64/"
            },
            {
                "title": "https://security.stackexchange.com/questions/129499/what-does-eip-stand-for",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://security.stackexchange.com/questions/129499/what-does-eip-stand-for"
            },
            {
                "title": "https://www.intel.com/content/dam/www/public/us/en/documents/manuals/64-ia-32-architectures-software-developer-vol-1-manual.pdf",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://www.intel.com/content/dam/www/public/us/en/documents/manuals/64-ia-32-architectures-software-developer-vol-1-manual.pdf"
            },
            {
                "title": "https://wiki.osdev.org/X86-64_Instruction_Encoding#Legacy_Prefixes",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://wiki.osdev.org/X86-64_Instruction_Encoding#Legacy_Prefixes"
            },
            {
                "title": "https://en.wikipedia.org/wiki/FLAGS_register",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://en.wikipedia.org/wiki/FLAGS_register"
            },
            {
                "title": "https://www.quora.com/What-is-POPF-I-can-understand-PUSHF-cause-it-simply-push-flags-but-what-is-POPF-How-does-computer-know-what-is-flag-to-pop-1",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://www.quora.com/What-is-POPF-I-can-understand-PUSHF-cause-it-simply-push-flags-but-what-is-POPF-How-does-computer-know-what-is-flag-to-pop-1"
            },
            {
                "title": "http://www.c-jump.com/CIS77/ASM/Instructions/I77_0070_eflags_bits.htm",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "http://www.c-jump.com/CIS77/ASM/Instructions/I77_0070_eflags_bits.htm"
            },
            {
                "title": "https://nasm.us/doc/nasmdoc3.html",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://nasm.us/doc/nasmdoc3.html"
            },
            {
                "title": "https://compas.cs.stonybrook.edu/~nhonarmand/courses/sp17/cse506/ref/assembly.html",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://compas.cs.stonybrook.edu/~nhonarmand/courses/sp17/cse506/ref/assembly.html"
            },
            {
                "title": "https://datacadamia.com/computer/cpu/register/eflags",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://datacadamia.com/computer/cpu/register/eflags"
            },
            {
                "title": "https://www.tutorialspoint.com/assembly_programming/assembly_scas_instruction.htm",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://www.tutorialspoint.com/assembly_programming/assembly_scas_instruction.htm"
            },
            {
                "title": "https://www.tutorialspoint.com/assembly_programming/assembly_registers.htm",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://www.tutorialspoint.com/assembly_programming/assembly_registers.htm"
            },
            {
                "title": "https://en.wikipedia.org/wiki/X86_calling_conventions",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://en.wikipedia.org/wiki/X86_calling_conventions"
            },
            {
                "title": "https://en.wikibooks.org/wiki/X86_Assembly/Control_Flow",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://en.wikibooks.org/wiki/X86_Assembly/Control_Flow"
            },
            {
                "title": "https://revers.engineering/applied-re-accelerated-assembly-p1/",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://revers.engineering/applied-re-accelerated-assembly-p1/"
            },
            {
                "title": "https://wiki.skullsecurity.org/index.php?title=Registers#eip",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://wiki.skullsecurity.org/index.php?title=Registers#eip"
            },
            {
                "title": "https://www.felixcloutier.com/x86/scas:scasb:scasw:scasd",
                "type": "reference",
                "description": "Reference from IDF",
                "url": "https://www.felixcloutier.com/x86/scas:scasb:scasw:scasd"
            }
        ],
        "comments": "generated by rellink to module script",
        "assessment": [
            {
                "description": "Leverage conditional branching to solve problems in Assembly.",
                "eval": "https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_6/Lab6.nasm"
            },
            {
                "description": "Utilize common string instructions in Assembly.",
                "eval": "https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_7/Lab7.nasm"
            },
            {
                "description": "In Assembly, understand name mangling and access predefined external utility functions.",
                "eval": "https://gitlab.com/39iosdev/ccd-iqt/idf/assembly/-/blob/master/mdbook/src/labs/Lab_9/Lab9.nasm"
            }
        ]
    }
}
