{
    "Python Flow Control": {
        "uniq_id": "BD_PY_FLCON_BREAK",
        "uniq_name": "flow_control",
        "description": "Comprehend if, elif, else statements in Python;\nComprehend While loops in Python;\nComprehend For loops in Python;\nComprehend Python operators;\nGiven a scenario, select the appropriate relational operator for the solution.;\nGiven a code snippet containing operators, predict the outcome of the code.;\nComprehend the purpose of the Break statement;\nComprehend the purpose of the Continue statement;\nGiven a code snippet, predict the behavior of a loop that contains a break/continue statement.",
        "workrole": "CCD",
        "group": "python",
        "objective": "Control the execution of a program with conditional if, elif, else statements.;\nUse a while loop to repeat code execution a specified number of times;\nIterate through Python object elements with a For loop;\nUtilize assignment operators as part of a Python code solution.;\nUtilize boolean operators as part of a Python code solution.;\nUtilize membership operators as part of a Python code solution.;\nUtilize identity operators as part of a Python code solution.;\nUtilize break or continue statements to control code behavior in loops.",
        "prerequisites": [
            {
                "uniq_id": "BD_PY_DTYPES_TUP"
            }
        ],
        "content": [
            {
                "description": "IDF lessons for flow control - operators",
                "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/"
            },
            {
                "description": "IDF lessons for flow control - if, elif, else",
                "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/if_elif_else.html"
            },
            {
                "description": "IDF lessons for flow control - while",
                "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/while_loops.html"
            },
            {
                "description": "IDF lessons for flow control - for loops",
                "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/for_loops.html"
            },
            {
                "description": "IDF lessons for flow control - break and continue",
                "ref": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/break_continue.html"
            }
        ],
        "performance": [
            
        ],
        "required_tasks": [],
        "required_abilities": [
            {
                "ksat_id": "A0047",
                "proficiency": "3",
                "description": "Implement file management operations."
            }
        ],
        "required_skills": [
            {
                "ksat_id": "S0082",
                "proficiency": "3",
                "description": "Implement conditional control flow constructs."
            },
            {
                "ksat_id": "S0037",
                "proficiency": "3",
                "description": "Open and close an existing file."
            },
            {
                "ksat_id": "S0038",
                "proficiency": "3",
                "description": "Read, parse, write (append, insert, modify) file data."
            },
            {
                "ksat_id": "S0039",
                "proficiency": "3",
                "description": "Create and delete a file."
            },
            {
                "ksat_id": "S0040",
                "proficiency": "3",
                "description": "Determine the size of a file."
            },
            {
                "ksat_id": "S0041",
                "proficiency": "3",
                "description": "Determine location of content within a file."
            },
            {
                "ksat_id": "S0081",
                "proficiency": "3",
                "description": "Implement a looping construct."
            },
            {
                "ksat_id": "S0032",
                "proficiency": "3",
                "description": "Utilize relational operators to formulate boolean expressions."
            },
            {
                "ksat_id": "S0033",
                "proficiency": "3",
                "description": "Utilize assignment operators to update a variable."
            }
        ],
        "required_knowledge": [
            {
                "ksat_id": "K0713",
                "proficiency": "C",
                "description": "Understand how to implement conditional statements."
            },
            {
                "ksat_id": "K0037",
                "proficiency": "A",
                "description": "Understand the purpose and use of the keyword \"Super\""
            },
            {
                "ksat_id": "K0038",
                "proficiency": "A",
                "description": "Recognize the signature and purpose of the initialization function of a constructor."
            },
            {
                "ksat_id": "K0039",
                "proficiency": "A",
                "description": "Describe the purpose of getter and setter functions"
            },
            {
                "ksat_id": "K0040",
                "proficiency": "A",
                "description": "Describe the purpose of a class attribute."
            },
            {
                "ksat_id": "K0041",
                "proficiency": "A",
                "description": "Describe the behavior of factory design pattern."
            },
            {
                "ksat_id": "K0062",
                "proficiency": "B",
                "description": "Describe the libraries commonly used to aid in serialization."
            },
            {
                "ksat_id": "K0741",
                "proficiency": "B",
                "description": "Understand how to open and close an existing file."
            },
            {
                "ksat_id": "K0742",
                "proficiency": "B",
                "description": "Understand how to read, parse write file data."
            },
            {
                "ksat_id": "K0743",
                "proficiency": "B",
                "description": "Understand how to create and delete a new file."
            },
            {
                "ksat_id": "K0744",
                "proficiency": "B",
                "description": "Understand how to determine the size of a file."
            },
            {
                "ksat_id": "K0745",
                "proficiency": "B",
                "description": "Understand how to determine location of content within a file."
            },
            {
                "ksat_id": "K0107",
                "proficiency": "B",
                "description": "Understand how to implement and use looping constructs."
            },
            {
                "ksat_id": "K0715",
                "proficiency": "C",
                "description": "Identify and understand relational operators."
            },
            {
                "ksat_id": "K0735",
                "proficiency": "C",
                "description": "Identify and understand assignment operators."
            }
        ],
        "comments": "generated by rellink to module script",
        "assessment": [
            {
                "description": "Utilize assignment operators as part of a Python code solution.",
                "eval": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3a.html"
            },
            {
                "description": "Control the execution of a program with conditional if, elif, else statements.",
                "eval": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3c.html"
            },
            {
                "description": "Use a while loop to repeat code execution a specified number of times",
                "eval": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3d.html"
            },
            {
                "description": "Iterate through Python elements with a For loop",
                "eval": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3e.html"
            },
            {
                "description": "Utilize boolean operators as part of a Python code solution.",
                "eval": "https://39iosdev.gitlab.io/ccd-iqt/idf/python/Flow_Control/lab3f.html"
            }
        ]
    }
}