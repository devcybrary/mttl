import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoWrMetricsComponent } from './no-wr-metrics.component';

describe('NoWrMetricsComponent', () => {
  let component: NoWrMetricsComponent;
  let fixture: ComponentFixture<NoWrMetricsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoWrMetricsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoWrMetricsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
