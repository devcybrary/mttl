import { Component, OnInit, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ApiService } from '../api.service';
import { MatDialog } from '@angular/material/dialog';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { KsatLinkPromptComponent } from '../ksat-link-prompt/ksat-link-prompt.component';
import { NewKsatPopupComponent } from '../new-ksat-popup/new-ksat-popup.component';
import { CoverageCalculatorPipe } from '../pipes/coverage-calculator.pipe';
import { CookieService } from 'ngx-cookie-service';

import { ExportDataService } from '../export-data.service';

import * as introJs from 'intro.js/intro.js';

//font awesome icon import
import { faBook,
         faStopwatch,
         faUsers,
         faPlus,
         faSearch,
         faChild,
         faSortUp,
         faSortDown,
         faSort,
         faHammer,
         faToolbox,
         faHome,
         faCheckDouble,
         faChalkboardTeacher,
         faMapMarker,
         faThList
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-no-wr-metrics',
  templateUrl: './no-wr-metrics.component.html',
  styleUrls: ['./no-wr-metrics.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers:    [ CoverageCalculatorPipe ]
})
export class NoWrMetricsComponent implements OnInit {
  @ViewChild('filterCol') filterCol;
  @ViewChild('filterInput') filterInput;
  @ViewChild('searchInput') searchInput;

  cookieValue: string;
  introJS = introJs();

  ksats;
  autocomplete;
  extradata;
  specs;
  tmp_array = [];
  currentReport = 'MTTL';
  sortDirection = '';
  sortColumn = 'wr_spec';
  count = 0;
  page = 1;
  pageSize = 2;
  tableSize = 7;
  filterObj = [];
  allWR = ['All'];
  tableSizes = [3, 6, 9, 12];
  sortCombo = [this.sortDirection + this.sortColumn];
  wrObj = this.allWR;

  // FA Icons
  faBook = faBook;
  faStopwatch = faStopwatch;
  faUsers = faUsers;
  faPlus = faPlus;
  faSearch = faSearch;
  faChild = faChild;
  faSortUp = faSortUp;
  faSortDown = faSortDown;
  faSort = faSort;
  bothCovered = faCheckDouble;
  faTrn = faChalkboardTeacher;
  unmapped = faMapMarker;

  // Searchbar settings
  visible = true;
  selectable = false;
  removable = true;
  addOnBlur = false;
  readonly separatorKeysCodes: number[] = [COMMA, ENTER];

  // Expanded Pane Carousel Settings
  itemsPerSlide = 5;
  linksPerSlide = 7;
  singleSlideOffset = false;
  noWrap = true;

  searchFC = new FormControl ();
  filterOptions = ['Tasks', 'Abilities', 'Skills', 'Knowledge'];

  /* eslint-disable */
  // ESLint doesn't like how these properties are named.
  filterMappings = {
      Tasks: 'Tasks',
      Abilities: 'Abilities',
      Skills: 'Skills',
      Knowledge: 'Knowledge'
  };
  constructor(
    private apiService: ApiService,
    private dialog: MatDialog,
    private exportdata:   ExportDataService,
    private cookieService: CookieService,
  ) { }


  getNoWrMetrics(): void{
    this.extradata = this.apiService.getNoWrMetrics();
    this.specs = Object.keys(this.extradata.nowrspec).map((key) => {
      return {
        tasks: this.extradata.nowrspec[key].Tasks,
        abs: this.extradata.nowrspec[key].Abilities,
        skill: this.extradata.nowrspec[key].Skills,
        know: this.extradata.nowrspec[key].Knowledge
      }
    })
  }

  clearChecks(radioName): void {
    // Function to clear radio buttons based on the name
    // This closes an expanded detail pane
    const ele = document.querySelector('input[name="' + radioName + '"]:checked') as HTMLInputElement;
    ele.checked = false;
  }

  addFilter(filter): void {
    let toAdd = true;
    this.filterObj.forEach(element => {
      this.test("Element Column checking in addFilter: ", element.column)
      this.test("Element filterText checking in addFilter: ", element.filterText)
      if (element.column === filter.column && element.filterText === filter.filterText){
        toAdd = false;
      }
    });

    if (toAdd){
      this.filterObj.push(filter);
    }
  }


  filterKSATList(column, text): void {
    let modifiedCol = '';
    if(column !== 'search'){
      modifiedCol=this.filterMappings[column];
    }else{
      modifiedCol = column;
    }
    this.addFilter({column: modifiedCol, filterText: text});
    this.filterInput.nativeElement.value = '';
    this.searchInput.nativeElement.value = '';
    this.page = 1;
  }

  clear(){
    this.searchFC.reset();
    this.filterObj=[];
    this.filterInput.nativeElement.value = '';
    this.searchInput.nativeElement.value = '';
  }

  clickableFilter(filterColumn, text): void {
    this.addFilter({ column: filterColumn, filterText: text });
    this.page = 1;
  }


  searchKSATList(value): void {
    // Adds a special key value pair with the "search" column prepopulated
    // This notifies the filter pipe to do an expensive full text search on every result
    // this.filter_obj.push({ 'column': 'search', 'filter_text': this.search_input.nativeElement.value })
    this.addFilter({ column: 'search', filterText: value });
    this.page = 1;
  }

  
  removeFilter(column, text): void {
    this.filterObj.forEach( (element, index) => {
      if(element.column === column && element.filterText === text){
        this.filterObj.splice(index, 1);
      }
    });
    this.page = 1;
  }


  addKsatDialog(): void {
    const dialogRef = this.dialog.open(NewKsatPopupComponent, {
      height: 'auto',
      width: '410px'
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }


  ksatLinkDialog(ksat, ksatType): void {
    const dialogRef = this.dialog.open(KsatLinkPromptComponent, {
      data: {
        ksatId: ksat.ksat_id,
        type: ksatType
      }
    });

  }


  changeSort(sort): void {
    // Changes sort column and sets variable used by frontend
    if (this.sortColumn !== sort) {
      this.sortColumn = sort;
      this.sortDirection = '';
      this.sortCombo = [this.sortDirection + this.sortColumn];
    } else {
      this.changeSortDirection();
    }
  }


  changeSortDirection(): void {
    // Changes sort direction and handles sorting on same column (inverts results)
    if (this.sortDirection === '') { this.sortDirection = '-'; }
    else { this.sortDirection = ''; }
    this.sortCombo = [this.sortDirection + this.sortColumn];
  }

  changeWR(workRole): void {
    // Setter function for work roles
    // if All is selected, set it to predefined list
    if (workRole !== 'All') { this.wrObj = [workRole]; }
    else { this.wrObj = this.allWR; }
    this.page = 1;
  }

  test (text: string, item: any): void{
    console.log(text, item);
  }


  ngOnInit(): void {
    //pulling data into key:value stored in specs
    this.getNoWrMetrics();
    this.autocomplete = this.apiService.getAutocomplete();
  }


  getFilename(): any {
    const d = new Date();
    const date = d.getDate();
    const month = d.getMonth() + 1;
    const year = d.getFullYear().toString().substr(-2);
    const hour = d.getHours();
    const minutes = d.getMinutes();
    const dateStr = month + '_' + date + '_' + year + '_' + hour + '_' + minutes;
    const fileNameWithDate = 'MTTL' + '_' + dateStr;
    return fileNameWithDate;
  }

  downloadCSV(): void {
    this.exportdata.downloadFile("csv", this.ksats);
  }

  downloadXLSX(): void {
    this.exportdata.downloadFile("xlsx", this.ksats);
  }

  getColor(ksatId: string): string {
    let color = '#000000';
    switch (ksatId[0]){
      case ('K'): {
        color = '#00abd4';
        break;
      }
      case ('S'): {
        color = '#b43000';
        break;
      }
      case ('A'): {
        color = '#b48a00';
        break;
      }
      case ('T'): {
        color = '#5d00b4';
        break;
      }
      default: {
        break;
      }
    }
    return color;
  }


  determineIcon(ksatId: string): any {
    let icon = faBook;
    switch (ksatId[0]){
      case ('K'): {
        icon = faBook;
        break;
      }
      case ('S'): {
        icon = faHammer;
        break;
      }
      case ('A'): {
        icon = faToolbox;
        break;
      }
      case ('T'): {
        icon = faHome;
        break;
      }
      default: {
        break;
      }
    }
    return icon;
  }


  getHref(html: string): string {
    try{
      const re = /href\s*=\s*"([^"]*)"/;
      return re.exec(html)[1];
    } catch {
      return '#';
    }
  }


  getAnchorText(html: string): string {
    try{
      const re = />([^<]*)</;
      const text = (re.exec(html)[1]);
      return text.replace(/[_-]/g, ' ');
    } catch {
      return 'No String';
    }
  }


  add(event: MatChipInputEvent): void {
    const value = event.value;
    const column = this.filterCol.nativeElement.value;
    if (column === 'search'){
      this.searchKSATList(value);
    }else{
      try{
        this.filterKSATList(column.trim(), value.trim());
      }catch{
        console.log('Error: could not add filter');
      }

    }
    // Reset the input value
    this.filterInput.nativeElement.value = '';
  }

  addBySelection(event: MatAutocompleteSelectedEvent): void{
    const value = event.option.viewValue;
    const column = this.filterCol.nativeElement.value;
    this.filterKSATList(column.trim(), value.trim());
    this.filterInput.nativeElement.value = '';
 
  }

  searched(col: any []): any []{
    const output = [];
    col.forEach(element =>{
      if(element.column !== 'search'){
        output.push(element);
      }
    });
    return output
  }

  filtered(col: any []): any []{
    const output = [];
    col.forEach(element => {
      if (element.column !== 'search'){
        output.push(element);
      }
    });
    return output;
  }

  sortSpec(filter: any): any []{
    const output = [];
    const hold = [];
    var str: string;
    const mtp = [];
    this.specs.forEach(element => {
      if(element[filter]!=''){
        hold.push(element[filter]);
      }
    });
    //this.test("hold array: ",hold);
    for (var _i = 0; _i < hold.length; _i++){
      str = hold[_i];
      if(str != undefined){
        this.tmp_array = str.split(',');
        this.tmp_array.forEach(obj=>{
          output.push(obj);
        });
      }
    }
    return output;
  }

  specFilter(list: string[], filt: string): any[]{
    const output = [];
    if (list[filt].length > 5){
      output.push(list[filt].split(','));
    }else{
      output.push(list[filt]);
    }
    return output;
  }
}
