import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyKsatDialogComponent } from './modify-ksat-dialog.component';

describe('ModifyKsatDialogComponent', () => {
  let component: ModifyKsatDialogComponent;
  let fixture: ComponentFixture<ModifyKsatDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifyKsatDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyKsatDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
