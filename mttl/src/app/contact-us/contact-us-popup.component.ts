import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ApiService } from '../api.service';
import { ProfileService } from '../profile.service';
import { faSync } from '@fortawesome/free-solid-svg-icons';

import { environment } from '../../environments/environment';

import * as halfmoon from 'halfmoon';

@Component({
  selector: 'app-contact-us-popup',
  templateUrl: './contact-us-popup.component.html',
  styleUrls: ['./contact-us-popup.component.scss']
})
export class ContactUsComponent implements OnInit {

  public desc = '';
  public topic = '';
  public notes = '';
  error = false;
  thanks = false;
  profileInfo;
  profileName = '';
  profileCookieValue;
  client_id = 'f02714f7797efd485c1d6602abbb2a4ff9e185355750390cf52f8632e3277ff1';
  redirect_uri: string;
  decodedRedirect_URI: string;
  gitLabRedirectURI: string;
  syncIcon = faSync;
  url = '';


  constructor(
    public dialogRef: MatDialogRef<ContactUsComponent>,
    private apiService: ApiService,
    private profileService: ProfileService
  ) {
    this.getProfileInfo();
    this.redirect_uri = environment.REDIRECT_URI;
    this.decodedRedirect_URI = decodeURIComponent(this.redirect_uri);
   }

  ngOnInit() {

  }


  createGitLabRedirectURImessage(): string {
    /* eslint max-len: "off" */
    this.gitLabRedirectURI = 'You must authenticate with Gitlab to access this feature.\
                                                      <br/> Click\
                                                      <a href=\
                                                      \'https://gitlab.com/oauth/authorize?client_id=' +
      this.client_id +
      '&redirect_uri=' +
      this.redirect_uri +
      '&response_type=code&state=' +
      this.profileCookieValue +
      '&scope=openid\' class=\'alert-link\'>\
                                                      here</a> or on the Gitlab icon in the top right.\
                                                      <fa-icon [icon]=\'gitlab\'></fa-icon>';
    return this.gitLabRedirectURI;
    /* eslint max-len: ["error", { "code": 120 }]*/
  }

  async setProfileCookie() {
    await this.profileService.setProfileCookie();
  }

  async getProfileInfo() {
    for (let i = 0; i < 5; i++){
      try {
        this.profileInfo = await this.profileService.getProfileInfo();
        this.profileName = await this.profileService.getUserName();
        if (this.profileName){
          break;
        }
      } catch {
        await this.delay(100);
      }
    }
    try {
      this.profileInfo = await this.profileService.getProfileInfo();
      this.profileName = await this.profileService.getUserName();
    } catch {
      this.dialogRef.close();
      this.setProfileCookie();
      const alertContent = this.createGitLabRedirectURImessage();
      halfmoon.initStickyAlert({
        content: alertContent,
        title: 'Authentication Required',
        alertType: 'alert-danger',
        fillType: 'filled',
        hasDismissButton: true,
        timeShown: 9000
      });
      return;
    }

  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
  async sendData(): Promise<any> {
    this.url = '';
    this.error = false;
    this.thanks = false;
    if (this.desc && this.topic){
      this.thanks = true;
      this.url = await this.apiService.requestContact(this.desc, this.topic, this.notes);
    } else {
      this.error = true;
    }
  }
}
