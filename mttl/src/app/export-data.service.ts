import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

@Injectable({
  providedIn: 'root'
})
export class ExportDataService {
  public downloadFile(fileType, data): void {
    // This is all actually pass by reference
    const fixedData = this.undecorateTrainingAndEval(data);
    const expandedData = this.expandProficiencyData(fixedData);
    const csvData = this.convertToCSV(expandedData, ['_id', 'ksat_id', 'parent', 'description', 'topic',
      'requirement_src', 'requirement_owner',
      'children', 'work-roles', 'child_count', 'parent_count',
      'training_count', 'eval_count',
      'requirement_src_id', 'proficiency_data', 'created_on',
      'comments', 'updated_on',
      'specializations', 'references', 'training_links', 'eval_links']);
    if (fileType === 'csv') {
      const blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
      const dwldLink = document.createElement('a');
      const url = URL.createObjectURL(blob);
      const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
      if (isSafariBrowser) {  // if Safari open in new window to save file with random filename.
        dwldLink.setAttribute('target', '_blank');
      }
      dwldLink.setAttribute('href', url);
      dwldLink.setAttribute('download', this.getFilename() + '.csv');
      dwldLink.style.visibility = 'hidden';
      document.body.appendChild(dwldLink);
      dwldLink.click();
      document.body.removeChild(dwldLink);
    }
    else  if (fileType === 'xlsx') {
      const blob = new Blob(['\ufeff' + csvData],
                            { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8' });
      const dwldLink = document.createElement('a');
      const url = URL.createObjectURL(blob);
      const isSafariBrowser = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
      if (isSafariBrowser) {  // if Safari open in new window to save file with random filename.
        dwldLink.setAttribute('target', '_blank');
      }
      dwldLink.setAttribute('href', url);
      dwldLink.setAttribute('download', this.getFilename() + '.xlsx');
      dwldLink.style.visibility = 'hidden';
      document.body.appendChild(dwldLink);
      dwldLink.click();
      document.body.removeChild(dwldLink);
    }
  }

  private getFilename(): any {
    const d = new Date();
    const date = d.getDate();
    const month = d.getMonth() + 1;
    const year = d.getFullYear().toString().substr(-2);
    const hour = d.getHours();
    const minutes = d.getMinutes();
    const dateStr = month + '_' + date + '_' + year + '_' + hour + '_' + minutes;
    const fileNameWithDate = 'MTTL' + '_' + dateStr;
    return fileNameWithDate;
  }

  private removeKey(obj) {
    delete obj.Comments;
    return obj;
    // once the wrong KSAT is fixed and / or a pipeline check is added this is superfluous
  }

  private expandProficiencyData(jsonData: any[]): any[] {
    jsonData.forEach(element => {
      if (JSON.stringify(element).includes('proficiency_data')) {
        // console.log(JSON.stringify(element))
        const newProficiencyData = JSON.stringify(element.proficiency_data);
        // console.log('old proficiency_data: ', newProficiencyData);
      }
    let newProficiency = JSON.stringify(element.proficiency_data);
    const wr = /"work-role":/gi;
    const prof = /"proficiency":/gi;
    const emptyQuotes = /""/gi;
    const quotes = /"/gi;
    const br = /},{/gi;
    const start = /\[\{/gi;
    const end = /\}\]/gi;
    newProficiency = newProficiency.replace(wr, '');
    newProficiency = newProficiency.replace(prof,'');
    newProficiency = newProficiency.replace(emptyQuotes,'undefined');
    newProficiency = newProficiency.replace(quotes,'');
    newProficiency = newProficiency.replace(br,';');
    newProficiency = newProficiency.replace(start,'');
    newProficiency = newProficiency.replace(end,'');
    newProficiency = newProficiency.replace(',',':');
    newProficiency = newProficiency.replace('\[\]','');
    element.proficiency_data = newProficiency;

    // console.log('new proficiency_data: ', newProficiency);
    }
    );
    return jsonData;
  }

  private undecorateTrainingAndEval(jsonData: any[]): any[] {
    jsonData.forEach(element => {
      if (JSON.stringify(element).includes('Comment')) {
        // Note - 'Comment' not 'comment' - there was one 'Comment', it was empty and broke things
        element = this.removeKey(element);
      }
      let newcomments = element.comments;
      newcomments = newcomments.replace('\"', '\'');
      newcomments = newcomments.replace('\\', '');
      newcomments = newcomments.replace(/"  "/g, ' ');
      newcomments = newcomments.replace(/"  +"/g, ' ');
      newcomments = newcomments.replace(/^\s+|\s+$/g, '');
      element.comments = newcomments;

      let newtopic = element.topic;
      newtopic = newtopic.replace('\"', '\'');
      newtopic = newtopic.replace('\\', '');
      newtopic = newtopic.replace(/"  "/g, ' ');
      newtopic = newtopic.replace(/"  +"/g, ' ');
      newtopic = newtopic.replace(/^\s+|\s+$/g, '');
      element.topic = newtopic;

      if (element.children !== undefined){
      let newchildren = element.children;
      newchildren = newchildren.replace(/\"/g, '\'');
      newchildren = newchildren.replace(/\\/g, '');
      newchildren = newchildren.replace(/"  "/g, ' ');
      newchildren = newchildren.replace(/"  +"/g, ' ');
      newchildren = newchildren.replace(/","/g, ';');
      newchildren = newchildren.replace(/" "/g, '');
      newchildren = newchildren.replace(/^\s+|\s+$/g, '');
      element.children = newchildren;
      }
      //these are arrays

      if (element['work-roles'] !== undefined){
   let newwork_roles = JSON.stringify(element['work-roles']);
      newwork_roles = newwork_roles.replace(/\"/g, '\'');
      newwork_roles = newwork_roles.replace(/\\/g, '');
      newwork_roles = newwork_roles.replace(/"  "/g, ' ');
      newwork_roles = newwork_roles.replace(/"  +"/g, ' ');
      newwork_roles = newwork_roles.replace(/","/g, ';');
      newwork_roles = newwork_roles.replace(/" "/g, '');
      newwork_roles = newwork_roles.replace(/^\s+|\s+$/g, '');
      element.work_roles = newwork_roles;
      }



      if (element.updated_on !== undefined){
        let newupdated_on = JSON.stringify(element.updated_on);
        console.log(typeof(newupdated_on));
        newupdated_on = newupdated_on.replace(/\'/g, '');
        newupdated_on = newupdated_on.replace(/\"/g, '\'');
        newupdated_on = newupdated_on.replace(/\\/g, '');
        newupdated_on = newupdated_on.replace(/"  "/g, ' ');
        newupdated_on = newupdated_on.replace(/"  +"/g, ' ');
        newupdated_on = newupdated_on.replace(/^\s+|\s+$/g, '');
        element.updated_on = newupdated_on;
      }


          // let newparent = element.parent;
      // newparent = newparent.replace(/^\s+|\s+$/g, "");
      // element.parent = newparent


      let description = element.description;
      description = description.replace(/\"/g, '\'');
      description = description.replace(/\\/g, '');
      description = description.replace(/"  +"/g, ' ');
      description = description.replace(/^\s+|\s+$/g, '');
      element.description = description;

      let requirement_owner = element.requirement_owner;
      requirement_owner = requirement_owner.replace(/\"/g, '\'');
      requirement_owner = requirement_owner.replace(/"  +"/g, ' ');
      requirement_owner = requirement_owner.replace(/^\s+|\s+$/g, '');
      element.requirement_owner = requirement_owner;

      let requirement_src = element.requirement_src;
      requirement_src = requirement_src.replace(/\"/g, '\'');
      requirement_src = requirement_src.replace(/"  +"/g, ' ');
      requirement_src = requirement_src.replace(/^\s+|\s+$/g, '');
      element.requirement_src = requirement_src;

      if (element.requirement_src_id !== undefined){
        let requirement_src_id = element.requirement_src_id;
        requirement_src_id = requirement_src_id.replace(/\'/g, '');
        requirement_src_id = requirement_src_id.replace(/\"/g, '');
        requirement_src_id = requirement_src_id.replace(/"  +"/g, ' ');
        requirement_src_id = requirement_src_id.replace(/^\s+|\s+$/g, '');
        element.requirement_src_id = requirement_src_id;
      }


      let neweval = element.eval_links;
      neweval = neweval.replace(/\<a target="_blank" rel="noopener noreferrer" href=/g, '');
      neweval = neweval.replace(/title[^,]*/g, '');
      neweval = neweval.replace(/,/g, '');
      neweval = neweval.replace(/,/g, '');
      neweval = neweval.replace(/"/g, '\'');
      neweval = neweval.replace(/   +/g, ';');
      neweval = neweval.replace(/' '/g, ';');
      neweval = neweval.replace(/\ /g, ';');
      neweval = neweval.replace(/;;/g, ';');
      element.eval_links = neweval;

      let newtraining = element.training_links;
      newtraining = newtraining.replace(/\<a target="_blank" rel="noopener noreferrer" href=/g, '');
      newtraining = newtraining.replace(/title[^,]*/g, '');
      newtraining = newtraining.replace(/,/g, ' ');
      newtraining = newtraining.replace(/"/g, '\'');
      newtraining = newtraining.replace(/   +/g, ';');
      element.training_links = newtraining;
    });
    return jsonData;
  }


  private exportExcel(jsonData: any[]): void {
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(jsonData);
    delete (ws.Comments);
    // eslint-disable-next-line
    const wb: XLSX.WorkBook = { Sheets: { data: ws }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
    this.saveExcelFile(excelBuffer);
  }

  private saveExcelFile(buffer: any): void {
    const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    const fileExtension = '.xlsx';
    const data: Blob = new Blob([buffer], { type: fileType });
    FileSaver.saveAs(data, this.getFilename() + fileExtension);
  }

  private convertToCSV(objArray, headerList): string {
    const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = 'index,';
    headerList.forEach(element => {
      row += element + ',';
    });
    row = row.slice(0, -1);
    str += row + '\r\n'; // header row
    array.forEach((element, idx) => {
      let line = (idx + 1) + '';
      headerList.forEach(index => {
        const head = index;
        let nextfield = ' ';
        if (array[idx][head] !== undefined && array[idx][head] !== '') {
          nextfield = '"' + array[idx][head] + '"';
        }
        if (typeof (nextfield) !== 'string') {
          console.log('next field is : ', typeof (nextfield));
        }
        line += ',' + nextfield;
        // console.log(line)
      });
      str += line + '\r\n';
    });
    return str;
  }
}
