import { Pipe, PipeTransform } from '@angular/core';
import { CoverageCalculatorPipe } from './coverage-calculator.pipe';

@Pipe({
  name: 'filterMTTL',
  pure: false
})
export class FilterMTTLPipe implements PipeTransform {

  constructor(private coverageCalculator: CoverageCalculatorPipe) {}

  transform(items: any[], filterObj: any[]): any[] {
    if (filterObj.length > 0) {
      // filter obj column : search , search string
      // items are all the KSATs
      let itemsFiltered = items;
      filterObj.forEach(elem => {
        // console.log("elem that we are iterating", elem);
        // elem = column search and filterText
        if (elem.column === 'search') {
          const tempArray = [];
          itemsFiltered.forEach(element => {
            if (element.description.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
              tempArray.push(element);
            }
            else if (element.topic.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
              tempArray.push(element);
            }
            else if (element.comments.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
              tempArray.push(element);
            }
            else if (element.ksat_id.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
              tempArray.push(element);
            }
            else if (element._id.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
              tempArray.push(element);
            }
          });
          itemsFiltered = tempArray;
        }

        else if (elem.column === 'wr_spec'){
          itemsFiltered = itemsFiltered.filter( (it) => {
                // console.log(it['wr_spec'])
                if (it.wr_spec.indexOf(elem.filterText) !== -1) { return true; }
            }
          );
        } else if (elem.column === 'parent'){
          itemsFiltered = itemsFiltered.filter( (it) => {
                // console.log(it['wr_spec'])
                if (it.parent.indexOf(elem.filterText) !== -1) { return true; }
            }
          );
        } else if (elem.column === 'description'){
          itemsFiltered = itemsFiltered.filter( (it) => {
                // console.log(it['wr_spec'])
                if (it.description.toLowerCase().indexOf(elem.filterText.toLowerCase()) !== -1) { return true; }
            }
          );
        } else if (elem.column === 'coverage'){
          let coverage = '';
          switch (elem.filterText) {
            case 'Training and Eval': {
              coverage = 't_and_e';
              break;
            }
            case 'Eval Only': {
              coverage = 'e_o';
              break;
            }
            case 'Training Only': {
              coverage = 't_o';
              break;
            }
            default: {
              coverage = 'none';
              break;
            }
          }
          itemsFiltered = this.coverageCalculator.transform(itemsFiltered, coverage);
        }
        else {
          itemsFiltered = itemsFiltered.filter((it) => {
              if (it[elem.column].toLowerCase() === elem.filterText.toLowerCase()) { return true; }
            }
          );
        }
      });
      return itemsFiltered;  // i.e. by Topic
    }
    else {
      return items;
    }
  }
}
