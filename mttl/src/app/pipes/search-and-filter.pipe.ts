import { Pipe, PipeTransform } from '@angular/core';
import { CoverageCalculatorPipe } from './coverage-calculator.pipe';

@Pipe({
  name: 'searchFilter',
  pure: false
})
export class SearchAndFilterPipe implements PipeTransform {

  constructor(private coverageCalculator: CoverageCalculatorPipe) {}

  transform(items: any[], filterObj: any[]): any[] {
    if (filterObj.length > 0) {
      // filter obj column : search , search string
      // items are all the KSATs
      let itemsFiltered = items;
      filterObj.forEach(elem => {
        // console.log("elem that we are iterating", elem);
        // elem = column search and filterText
        if (elem.column === 'search') {
          const tempArray = [];
          itemsFiltered.forEach(element => {
            if (element.know.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
              tempArray.push(element);
            }
            else if (element.skill.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
              tempArray.push(element);
            }
            else if (element.abs.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
              tempArray.push(element);
            }
            else if (element.tasks.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
              tempArray.push(element);
            }
            else if (element.wr.toLowerCase().search(elem.filterText.toLowerCase()) !== -1) {
              tempArray.push(element);
            }
          });
          itemsFiltered = tempArray;
        } else if (elem.column === 'wr'){
          itemsFiltered = itemsFiltered.filter( (it) => {
                // console.log(it['wr_spec'])
                if (it.wr.indexOf(elem.filterText) !== -1) { return true; }
            }
          );
        } else if (elem.column === 'skill'){
          itemsFiltered = itemsFiltered.filter( (it) => {
                // console.log(it['wr_spec'])
                if (it.skill.indexOf(elem.filterText) !== -1) { return true; }
            }
          );
        } else if (elem.column === 'know'){
          itemsFiltered = itemsFiltered.filter( (it) => {
                // console.log(it['wr_spec'])
                if (it.know.toLowerCase().indexOf(elem.filterText.toLowerCase()) !== -1) { return true; }
            }
          );
        } else if (elem.column === 'abs'){
          itemsFiltered = itemsFiltered.filter( (it) => {
                // console.log(it['wr_spec'])
                if (it.abs.toLowerCase().indexOf(elem.filterText.toLowerCase()) !== -1) { return true; }
            }
          );
        } else if (elem.column === 'tasks'){
          itemsFiltered = itemsFiltered.filter( (it) => {
                // console.log(it['wr_spec'])
                if (it.tasks.toLowerCase().indexOf(elem.filterText.toLowerCase()) !== -1) { return true; }
            }
          );
        } else if (elem.column === 'coverage'){
          let coverage = '';
          switch (elem.filterText) {
            case 'Training and Eval': {
              coverage = 't_and_e';
              break;
            }
            case 'Eval Only': {
              coverage = 'e_o';
              break;
            }
            case 'Training Only': {
              coverage = 't_o';
              break;
            }
            default: {
              coverage = 'none';
              break;
            }
          }
          itemsFiltered = this.coverageCalculator.transform(itemsFiltered, coverage);
        }
        else {
          itemsFiltered = itemsFiltered.filter((it) => {
              if (it[elem.column].toLowerCase() === elem.filterText.toLowerCase()) { return true; }
            }
          );
        }
      });
      return itemsFiltered;  // i.e. by Topic
    }
    else {
      return items;
    }
  }
}
