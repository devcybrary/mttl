import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'FAQSearch'
})
export class FAQSearchPipe implements PipeTransform {

  transform(value: any[], text: string): any[] {
    const outputArr = [];
    const key = 'question';
    value.forEach(val => {
      if (val[key].toLowerCase().indexOf(text.toLowerCase()) !== -1){
        outputArr.push(val);
      }
    });
    return outputArr;
  }

}
