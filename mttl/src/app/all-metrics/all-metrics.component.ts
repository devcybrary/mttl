import { AfterViewInit, Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ApiService } from '../api.service';

// Filters
import filterData from './filters.json';
// Pipes
import { FilterMTTLPipe } from '../pipes/filter-mttl.pipe';
import { CoverageCalculatorPipe } from '../pipes/coverage-calculator.pipe';
import { FilterByArrayPipe } from '../pipes/filter-by-array.pipe';

import * as halfmoon from 'halfmoon';
import Chart from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-all-metrics',
  templateUrl: './all-metrics.component.html',
  styleUrls: ['./all-metrics.component.scss'],
  providers:    [ FilterMTTLPipe, CoverageCalculatorPipe, FilterByArrayPipe ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AllMetricsComponent implements OnInit, AfterViewInit {

  // Main ksat list populated from API call
  ksats;

  filters = filterData;

  constructor(
      private apiService: ApiService,
      private coverageCalculator: CoverageCalculatorPipe,
      private mttlFilter: FilterMTTLPipe,
      private arrayFilter: FilterByArrayPipe
  ) { }

  ngOnInit(): void {
    // When DOM is ready, do this:
    this.ksats = this.apiService.getAllKSATs();
    // Default on ready function for halfmoon UI
    halfmoon.onDOMContentLoaded();
  }

  ngAfterViewInit(): void {
    this.generateCharts();
  }

  getData(filter): any {
    const filterType = filter.filter_type;
    const filterText = filter.filter_text;
    let filterObj = [];
    const output: {[key: string]: any} = {};
    if (filterType !== 'mttl') {
      filterObj = [{ column: filterType, filterText: filterText}];
    }
    const filteredKsats = this.mttlFilter.transform(this.ksats, filterObj);
    output.total = filteredKsats.length;
    output.covered = this.coverageCalculator.transform(filteredKsats, 't_and_e').length;
    output.evalCovered = this.coverageCalculator.transform(filteredKsats, 'e_o').length;
    output.trnCovered = this.coverageCalculator.transform(filteredKsats, 't_o').length;
    output.noCoverage = this.coverageCalculator.transform(filteredKsats, 'none').length;

    return output;
  }

  generateCharts(): void {
    this.filters.forEach(filter => {
      const data = this.getData(filter);
      const ctx = document.getElementById(filter.filter_text);
      this.calculateBarChart(data.covered, data.evalCovered, data.trnCovered, data.noCoverage, ctx);
    });
  }

  coverageText(filter: any): string {
    let output;
    if (filter.filter_type === 'mttl')
    {
      output = 'Total MTTL Coverage';
    } else if (filter.filter_type === 'wr_spec')
    {
      output = filter.filter_text + ' Work Role Coverage';
    } else {
      output = filter.filter_text + ' Coverage';
    }

    return output;
  }

  calculateBarChart(covered: number, evalCovered: number, trnCovered: number, noCoverage: number, ctx: any): void {
    ctx.height = 45;
    const total = covered + noCoverage + trnCovered + evalCovered;
    const chart = new Chart(ctx, {
      type: 'horizontalBar',
      plugins: [ChartDataLabels],
      data: {
        labels: ['KSATs Covered'],
        datasets: [
          {
            label: 'Training and Eval (' + covered + '/' + total + ')',
            data: [covered],
            backgroundColor: '#6EC664',
            borderWidth: 3,
            borderSkipped: null
          },
          {
            label: 'Eval (' + (evalCovered + covered) + '/' + total + ')',
            data: [evalCovered],
            backgroundColor: '#F0AB00',
            borderWidth: 3,
            borderSkipped: null
          },
          {
            label: 'Training (' + (trnCovered + covered) + '/' + total + ')',
            data: [trnCovered],
            backgroundColor: '#0066CC',
            borderWidth: 3,
            borderSkipped: null
          },
          {
            label: 'No Coverage (' + noCoverage + '/' + total + ')',
            data: [noCoverage],
            backgroundColor: '#707070',
            borderWidth: 3,
            borderSkipped: null
          }
        ]
      },
      options: {
        responsive: true,
        tooltips: {
          enabled: false
        },
        scales: {
          xAxes: [{
            stacked: true,
            ticks: { max: total },
            display: false,
            gridLines: {
              display: false,
            }
          }],
          yAxes: [{ display: false, stacked: true }]
        },
        legend: {
          display: false,
          labels: {
            fontSize: 20,
          }
        },
        plugins: {
          // Change options for ALL labels of THIS CHART
          datalabels: {
            color: '#ffffff',
            formatter: (value, contxt) => {
              const percentage = Math.round(( value/total ) * 100);
              if (percentage){
                return percentage + '%';
              }
              return '';
            },
            font: {
              size: 20
            }
          }
        }
      }
    });
  }

}
