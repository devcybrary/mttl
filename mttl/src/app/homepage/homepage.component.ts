import { Component, OnInit } from '@angular/core';
import * as halfmoon from 'halfmoon';
import { ContactUsComponent } from '../contact-us/contact-us-popup.component';
import { MatDialog } from '@angular/material/dialog';



@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})

export class HomepageComponent implements OnInit {
  contactEmail = '90IOS.DOT.INBOX@us.af.mil';
  constructor(    private dialog: MatDialog ) {   }

  toggleDarkMode(): void {
    halfmoon.toggleDarkMode();
  }

  contactUs(): void {
    const dialogRef = this.dialog.open(ContactUsComponent, {
      height: '400px',
      width: '390px',
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  ngOnInit(): void {

    halfmoon.onDOMContentLoaded();

  }


}
