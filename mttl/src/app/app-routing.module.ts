import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {AppComponent} from './app.component';
import {HomepageComponent} from './homepage/homepage.component';
import { MetricsComponent } from './metrics/metrics.component';
import { WrMetricsComponent } from './wr-metrics/wr-metrics.component';
import { AllMetricsComponent } from './all-metrics/all-metrics.component';
import {MttlComponent} from './mttl/mttl.component';
import {RoadmapComponent} from './roadmap/roadmap.component';
import {ModuleViewerComponent} from './module-viewer/module-viewer.component';
import {ContactUsComponent} from './contact-us/contact-us-popup.component';
import {FAQComponent} from './faq/faq.component';
import {ModuleMakerComponent} from './module-maker/module-maker.component';
import {WorkroleRoadmapComponent} from './workrole-roadmap/workrole-roadmap.component';
import {IdfKsatsComponent} from './idf-ksats/idf-ksats.component';
import { MiscTablesComponent } from './misc-tables/misc-tables.component';
import { BugReportComponent } from './bug-report/bug-report.component';

// MUST ADD COMPONENT TO SWITCH TO IT!

import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';

const routes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'mttl', component: MttlComponent},
  {path: 'mttl/:workrole', component: MttlComponent},
  {path: 'misc-tables', component: MiscTablesComponent },
  {path: 'metrics', component: MetricsComponent},
  {path: 'wr-metrics', component: WrMetricsComponent},
  {path: 'all_metrics', component: AllMetricsComponent},
  {path: 'roadmap', component: RoadmapComponent },
  {path: 'module-viewer', component: ModuleViewerComponent },
  {path: 'module-viewer/:module_id', component: ModuleViewerComponent },
  {path: 'module-maker', component: ModuleMakerComponent },
  {path: 'contact_us', component: ContactUsComponent},
  {path: 'faq', component: FAQComponent},
  {path: 'bug-report', component: BugReportComponent},
  {path: 'disclaimer', component: DisclaimerComponent},
  {path: '**', component: PageNotFoundComponent}
  // Variables can be passed to components through the url structure
  // The variable name is what you will end up importing in the component
  // {path: 'URL/:IMPORTVARIABLE', component: ComponentName}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
