import { Component, Inject} from '@angular/core';
import { ApiService } from '../api.service';
import { ProfileService } from '../profile.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CookieService } from 'ngx-cookie-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { faSync } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-new-ksat-popup',
  templateUrl: './new-ksat-popup.component.html',
  styleUrls: ['./new-ksat-popup.component.scss'],
  providers: [ProfileService],
})
export class NewKsatPopupComponent{
  syncIcon = faSync;

  public desc = '';
  public topic = '';
  public notes = '';
  public email = '';
  public name = '';
  public workrole = '';
  public children = '';
  public parents = '';
  public reqOwner = '';
  public reqSrc = '';
  public trainingLink = '';
  public trainingRef = '';
  public evalLink = '';
  public gitLabusername ='';
  error = false;
  thanks = false;
  public res;
  public url: string;
  public spam: string[];
  public spamFlag: boolean;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<NewKsatPopupComponent>,
    private apiService: ApiService,
    private cookieService: CookieService,
    private http: HttpClient,
    private profileService: ProfileService) {
      this.name = data.name;
  }

  requestNewKsat(name, topic, workrole, reqOwner,
      reqSrc, parents, children, trainingLink, trainingRef, evals, desc): string {
      const token = this.cookieService.get('ProfileforMTTL');
      console.log('api service identified this token for new ksat -> ', token);

      if (!name) {
        name = 'None Provided';
      }
      if (!reqOwner) {
        reqOwner = 'None Provided';
      }
      if (!reqSrc) {
        reqSrc = 'None Provided';
      }
      if (!parents) {
        parents = 'None Provided';
      }
      if (!children) {
        children = 'None Provided';
      }
      if (!evals) {
        evals = 'None Provided';
      }
      if (!trainingLink) {
        trainingLink = 'None Provided';
      }
      if (!trainingRef) {
        trainingRef = 'None Provided';
      }

      console.log('From component newKsat: ', encodeURIComponent(name)
      + '/' + encodeURIComponent(topic) + '/' + encodeURIComponent(workrole) + '/' + encodeURIComponent(reqOwner)
      + '/' + encodeURIComponent(reqSrc) + '/' + encodeURIComponent(parents) + '/' + encodeURIComponent(children)
      + '/' + encodeURIComponent(trainingLink) + '/' + encodeURIComponent(evals) + '/' + encodeURIComponent(desc)
      + '/' + token );

      this.http.get('newKsat/' + encodeURIComponent(name)
        + '/' + encodeURIComponent(topic) + '/' + encodeURIComponent(workrole) + '/' + encodeURIComponent(reqOwner)
        + '/' + encodeURIComponent(reqSrc) + '/' + encodeURIComponent(parents) + '/' + encodeURIComponent(children)
        + '/' + encodeURIComponent(trainingLink) + '/' + encodeURIComponent(trainingRef) + '/'
        + encodeURIComponent(evals) + '/' + encodeURIComponent(desc) + '/'
        + token, { responseType: 'text' })
        .subscribe( data => {
          console.log('URL after data pull: ', data);
          this.url = data;
          this.spamCheck(this.url);
        });
      return this.url;
    }

  sendData(): any {
    if (this.thanks === true){
      this.reset();
    }
    this.error = false;
    this.thanks = false;

    if (this.desc && this.topic) {
      console.log('popup ts sending \n name: ', this.name, '\n topic:', this.topic,
      '\n Work role:' , this.workrole, '\n owner: ', this.reqOwner, '\n src:', this.reqSrc, '\n parents: ',
      this.parents, '\n children: ', this.children,
      '\n training link: ', this.trainingLink, '\n eval link:' , this.evalLink, '\n description: ', this.desc, '\n \n');

     const tmp = this.requestNewKsat(this.name, this.topic,
        this.workrole, this.reqOwner, this.reqSrc, this.parents, this.children,
        this.trainingLink, this.trainingRef, this.evalLink, this.desc);

      this.thanks = true;
      const scrollContents = document.getElementById('contents');
      scrollContents.scrollTop = scrollContents.scrollHeight;
    }
    else {
      this.error = true;
    }
  }

  reset(): void{
    this.error = false;
    this.thanks = false;
    this.spam = [''];
    this.spamFlag = false;
    this.url = '';
    return;
  }

  //For link management
  spamCheck(val: string): boolean{
    console.log('in spamCheck with: ', val);
    if(val){
      this.spam = val.match(/spam/gi);
      if(this.spam && this.spam.length > 0){
        this.spamFlag=true;
        console.log('Spam response');
      }else{
        this.spamFlag=false;
      }
    }else{
      //if nothing in val send spam
      this.spamFlag=true;
      console.log('Spam response');
    }
   return this.spamFlag;
  }

  click(): void{
    if(!this.spamFlag){
      window.open(this.url, '_blank');
    }
  }
}
