import {
  Component,
  OnInit,
  OnChanges,
  ViewChild,
  ChangeDetectionStrategy,
  AfterViewInit,
  ViewEncapsulation,
  Injector,
  ApplicationRef,
  ChangeDetectorRef
} from '@angular/core';
import { COMMA, ENTER, H } from '@angular/cdk/keycodes';
import { ApiService } from '../api.service';
import { MatDialog, MAT_DIALOG_DATA, throwMatDialogContentAlreadyAttachedError } from '@angular/material/dialog';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { FormControl, Validators } from '@angular/forms';
import { KsatLinkPromptComponent } from '../ksat-link-prompt/ksat-link-prompt.component';
import { NewKsatPopupComponent } from '../new-ksat-popup/new-ksat-popup.component';
import { ModifyKsatDialogComponent } from '../modify-ksat-dialog/modify-ksat-dialog.component';

import { CoverageCalculatorPipe } from '../pipes/coverage-calculator.pipe';

import * as halfmoon from 'halfmoon';

import { CookieService } from 'ngx-cookie-service';
import * as introJs from 'intro.js/intro.js';

import { ExportDataService } from '../export-data.service';


//Need this to get oauth info
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

// get oauth from parent i.e. app component
import { SimpleChanges, Input } from '@angular/core';

// this is for redirect URI - maybe use express?
import { environment } from '../../environments/environment';

//profile service
import { ProfileService } from '../profile.service';

//refresh
// import { ApplicationRef} from '@angular/core';

//observables
import { Observable, of, from } from 'rxjs';

import { BugReportComponent } from '../bug-report/bug-report.component';
import { faGitlab, faThemeisle } from '@fortawesome/free-brands-svg-icons';
import {
  faBook,
  faStopwatch,
  faUsers,
  faPlus,
  faMinus,
  faSearch,
  faChild,
  faSortUp,
  faSortDown,
  faSort,
  faHammer,
  faToolbox,
  faHome,
  faCheckDouble,
  faChalkboardTeacher,
  faMapMarker,
  faEdit,
  faBug,
  faFileExport,
  faPlusSquare,
  faCode,
  faList
} from '@fortawesome/free-solid-svg-icons';
import { SelectColumnsComponent } from '../select-columns/select-columns.component';
// materials
import { MatTableDataSource } from '@angular/material/table';
import { animate, state, style, transition, trigger } from '@angular/animations';

import introData from './introjs.json';

interface ColSelection{
  displayName: string;
  id: string;
  checked: boolean;
  subColumns: ColSelection[];
}

@Component({
  selector: 'app-mttl',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './mttl.component.html',
  styleUrls: ['./mttl.component.scss'],
  providers: [CoverageCalculatorPipe, ProfileService],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none', visibility: 'hidden' })),
      state('expanded', style({ height: '*' }))
    ]),
  ],
})
// transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),

export class MttlComponent implements OnInit, OnChanges, AfterViewInit {
  // Definitions for temporary variables
  @ViewChild('filterCol') filterCol;
  @ViewChild('filterInput') filterInput;
  @ViewChild('searchInput') searchInput;

  expandedElement: any;

  // introJS
  cookieValue: string;
  introJS = introJs();

  //profile
  code;
  cookieURL;
  accessToken;
  redirected = false;
  profileName;
  profileInfo;
  isLoggedIn;
  profileCookieValue;
  mttlcolprefs;
  viewcolscookie;
  profileLoaded = false;
  hasProfileCookie;
  isOK;
  username;
  client_id = 'f02714f7797efd485c1d6602abbb2a4ff9e185355750390cf52f8632e3277ff1';
  redirect_uri: string;
  decodedRedirect_URI: string;
  gitLabRedirectURI: string;
  // Main ksat list populated from API call
  ksats;

  // Pagination count
  pageSize = 100;

  // Pagination options
  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];

  // Sorting defaults
  sortDirection = '';
  sortColumn = 'ksat_id';
  sortCombo = [this.sortDirection + this.sortColumn];

  // Filtering placeholders
  filterObj = []; // Format expected: {"column": "filter_text"}
  // Defining "All" work roles
  allWR = ['All'];
  wrObj = this.allWR;  // FA icons
  // This looks dumb but I'm following the instructions here:
  // https://www.npmjs.com/package/@fortawesome/angular-fontawesome
  codeIcon = faCode;
  faBook = faBook;
  faStopwatch = faStopwatch;
  faUsers = faUsers;
  faPlus = faPlus;
  faMinus = faMinus;
  faSearch = faSearch;
  faChild = faChild;
  faSortUp = faSortUp;
  faSortDown = faSortDown;
  faSort = faSort;
  bothCovered = faCheckDouble;
  faTrn = faChalkboardTeacher;
  faEdit = faEdit;
  faBug = faBug;
  unmapped = faMapMarker;
  gitlab = faGitlab;
  export = faFileExport;
  plusSquare = faPlusSquare;
  faHammer = faHammer;
  faList = faList;
  // Search bar settings
  visible = true;
  selectable = false;
  removable = true;
  addOnBlur = false;
  readonly separatorKeysCodes: number[] = [COMMA, ENTER];

  // Expanded Pane Carousel Settings
  itemsPerSlide = 5;
  linksPerSlide = 5;
  // linksPerSlide = 7;
  singleSlideOffset = false;
  noWrap = true;

  autocomplete;
  searchFC = new FormControl();
  // filterOptions = ['Work Role', 'Topic', 'Owner', 'Source', 'Parent', 'KSAT ID'];
  filterOptions = ['Work Role', 'Topic', 'Owner', 'Source', 'Parent', 'KSAT ID', 'OID', 'Comments'];

  /* eslint-disable */
  // ESLint doesn't like how these properties are named.
  filterMappings = {
    Owner: 'requirement_owner',
    Source: 'requirement_src',
    Parent: 'parent',
    'KSAT ID': 'ksat_id',
    Topic: 'topic',
    'Work Role': 'wr_spec',
    OID: 'ksat_oid',
    Comments: 'ksat_comm'
  };

  // initially displayed columns
  displayedColumns: string[] = ['coverage', 'ksat_id', 'description', 'topic', 'requirement_owner', 'requirement_src', 'wr_spec',
    'child_count', 'parent_count', 'training_count', 'add_training', 'eval_count', 'add_training']

  dataSource;
  columnDefs: any[] = ['ksat_id', 'description', 'topic', 'requirement_owner'];

  viewCols: ColSelection [] = [
    {displayName: 'ID', id:'ksat_id', checked: true, subColumns: [] },
    {displayName: 'Description', id:'description', checked: true, subColumns: [] },
    {displayName: 'Topic', id:'topic', checked: true, subColumns: [] },
    {displayName: 'Owner', id: 'requirement_owner', checked: true, subColumns: [] },
    {displayName: 'Source', id: 'requirement_src', checked: true, subColumns: [] },
    {displayName: 'Work Role', id: 'wr_spec', checked: true, subColumns: [
    {displayName: 'Work Role: Proficiency', id: 'proficiency_data', checked: false, subColumns: []},
    ]},
    {displayName: 'Children', id: 'child_count', checked: true, subColumns: []},
    {displayName: 'Parents', id: 'parent_count', checked: true, subColumns: []},
    {displayName: 'Training', id: 'training_count', checked: true, subColumns: []},
    {displayName: 'Evaluation', id: 'eval_count', checked: true, subColumns: []},
    {displayName: 'OID', id: 'OID', checked: false, subColumns: []},
    {displayName: 'Comments', id: 'comments', checked: false, subColumns: []},
  ];;
  cols_prev;
  darkModeOn;
  /* eslint-enable */
  constructor(
    private apiService: ApiService,
    private exportdata: ExportDataService,
    public dialog: MatDialog,
    private cookieService: CookieService,
    private http: HttpClient,
    private route: ActivatedRoute,
    private router: Router,
    private profileService: ProfileService,
    private appRef: ApplicationRef,
    private changeDetect: ChangeDetectorRef
  ) {
    this.redirect_uri = environment.REDIRECT_URI;
    this.decodedRedirect_URI = decodeURIComponent(this.redirect_uri);
    // some sources recommend adding the variables below etc.
    // this.profileName = this.profileService.getProfileName();
    //  but that didn't seem to help
  }

  async getHasProfileCookie() {
    this.hasProfileCookie = await this.profileService.getHasProfileCookie();
  }

  async getUserName() {
    this.username = await this.profileService.getUserName();
    return this.username;
  }

  async loadProfile() {
    await this.profileService.getProfileWithCookie();
    this.getProfileInfoFromService();
    this.getProfileNamefromService();
    this.getUserNamefromService();
    if (this.profileName !== undefined && this.profileName !== '') {
      this.profileLoaded = true;
      this.isLoggedIn = true;
    }
    return;
  }

  async getProfileInfoFromService() {
    this.profileInfo = await this.profileService.getProfileInfo();
    if (this.profileName !== undefined) {
      this.profileLoaded = true;
    }
    return;
  }

  async getProfileNamefromService() {
    this.profileName = await this.profileService.getProfileName();
    return;
  }

  async getUserNamefromService() {
    this.username = await this.profileService.getUserName();
    return;
  }

  addFilter(filter): void {
    let toAdd = true;
    this.filterObj.forEach(element => {
      if (element.column === filter.column && element.filterText === filter.filterText) {
        toAdd = false;
      }
    });

    if (toAdd) {
      this.filterObj.push(filter);
    }
  }

  getAllKSATs(): void {
    // Main function to retrieve all ksats from API service
    this.ksats = this.apiService.getAllKSATs();
  }


  toggleDarkMode(): void {
    halfmoon.toggleDarkMode();

  }


  clearChecks(radioName): void {
    // Function to clear radio buttons based on the name
    // This closes an expanded detail pane
    const ele = document.querySelector('input[name="' + radioName + '"]:checked') as HTMLInputElement;
    ele.checked = false;
  }

  filterKSATList(column, text): void {
    let modifiedCol = '';
    if (column !== 'search') {
      modifiedCol = this.filterMappings[column];
    } else {
      modifiedCol = column;
    }
    // Adds a key value pair to the filter_obj array to be filtered on
    this.addFilter({ column: modifiedCol, filterText: text });
    this.filterInput.nativeElement.value = '';
    this.searchInput.nativeElement.value = '';
    this.page = 1;
  }

  clear() {
    this.searchFC.reset();
    this.filterObj = [];
    this.searchInput.nativeElement.value = '';
    this.filterInput.nativeElement.value = '';
  }

  clickableFilter(filterColumn, text): void {
    this.addFilter({ column: filterColumn, filterText: text });
    this.page = 1;
  }

  searchKSATList(value): void {
    // Adds a special key value pair with the "search" column prepopulated
    // This notifies the filter pipe to do an expensive full text search on every result
    // this.filter_obj.push({ 'column': 'search', 'filter_text': this.search_input.nativeElement.value })
    this.addFilter({ column: 'search', filterText: value });
    this.page = 1;
    // console.log('search value:', this.search_input.nativeElement.value);
  }

  removeFilter(column, text): void {
    // Removes specified key value pair from filter_obj
    this.filterObj.forEach((element, index) => {
      if (element.column === column && element.filterText === text) {
        this.filterObj.splice(index, 1);
      }
    });
    this.page = 1;
  }

  createGitLabRedirectURImessage(): string {
    /* eslint max-len: "off" */
    this.gitLabRedirectURI = '<div style="z-index:200">\
                                                      You must authenticate with Gitlab to access this feature.\
                                                      <br/> Click\
                                                      <a href=\
                                                      \'https://gitlab.com/oauth/authorize?client_id=' +
      this.client_id +
      '&redirect_uri=' +
      this.redirect_uri +
      '&response_type=code&state=' +
      this.profileCookieValue +
      '&scope=openid\' class=\'alert-link\'>\
                                                      here</a> or on the Gitlab icon in the top right.\
                                                      <fa-icon [icon]=\'gitlab\'></fa-icon> </div>' ;
    return this.gitLabRedirectURI;
    /* eslint max-len: ["error", { "code": 120 }]*/
  }


  setProfileCookie(): void {
    this.profileService.setProfileCookie();
  }

  redirectToAuth() {
    this.setProfileCookie();
    const alertContent = this.createGitLabRedirectURImessage();
    halfmoon.initStickyAlert({
      content: alertContent,
      title: 'Authentication Required',
      alertType: 'alert-danger',
      fillType: 'filled',
      hasDismissButton: true,
      timeShown: 9000
    });
    return;
  }

  async addKsatDialog(): Promise<void> {
    if (this.isLoggedIn) {
      const userName = await this.profileService.getProfileName();
      console.log('mttl user\'s name being sent', userName);
      const dialogRef = this.dialog.open(NewKsatPopupComponent, {
        height: 'auto',
        width: 'auto',
        data: {
          name: userName
        }
      });
      dialogRef.afterClosed().subscribe(result => {
      });
    } else {
      this.redirectToAuth();
    }
  }

  modifyKsatDialog(ksat): void {
    if (this.isLoggedIn) {
      const dialogRef = this.dialog.open(ModifyKsatDialogComponent, {
        data: {
          ksatId: ksat.ksat_id
        }
      });
      dialogRef.afterClosed().subscribe(result => {
      });
    } else {
      this.redirectToAuth();
    }
  }


  ksatLinkDialog(ksat, ksatType): void {
    if (this.isLoggedIn) {
      const dialogRef = this.dialog.open(KsatLinkPromptComponent, {
        data: {
          ksatId: ksat.ksat_id,
          type: ksatType
        }
      });
    } else {
      this.redirectToAuth();
    }
  }

  countChecked(): number {
    let count = 0;
    this.viewCols.forEach(val => {
      if (val.checked) {
        count += 1;
      }
    });

    return count;
  }

  adjustTextCentering(): void {
    const table = document.getElementById('mttl'); //should be the table selected
    const text = document.getElementsByName('ksat_description');
    let i = 0;
    while (text[i]) {
      // document.getElementsByName("kssat_description")[i].style.backgroundColor="red";
      const element = text[i];
      const newStyle = 'color: red; background-color: pink';
      element.setAttribute('style', newStyle);
      console.log('set style for ', text[i]);
      // this.test('Is this valid? ', element.style);
      i++;
    }


  }

  setStyle(): void {
    const main = document.getElementById('mttl_body'); //should be the table selected
    const table = main.getElementsByTagName('tr');
    const selected = this.countChecked();
    const sending = '2% 1% repeat(' + selected + ', 10%)';
    this.test('Style that is being sent:', sending);
    let i = 0;
    while (table[i]) {
      const element = table[i];
      //for compounding style:
      //let oldStyle = element.getAttribute('style');
      const newStyle = 'color: red; grid-template-columns:' + sending;
      element.setAttribute('style', newStyle);
      // this.test('Is this valid? ', element.style);
      i++;
    }
  }

  cardBasedonlength(length): Record<string, unknown> {
    if (length > 30) {
      return {
        'vertical-align': 'top',
        'font-size': '95%', '-moz-transform': 'scale(.95)'
      };
    }
    else if (length > 24) {
      return { 'font-size': '95%', '-moz-transform': 'scale(.95)' };
    }
    else if (length > 16) {
      return { 'font-size': '95%' };
    }
    return {};
  }


  // if (length > 30) {
  //   return { 'vertical-align': 'top', position: 'relative', top: '-15px', 'margin-top' : '-10px',
  //     'font-size': '90%',   '-moz-transform': 'scale(.90)', 'margin-bottom': '0', 'padding-bottom' : '0'};
  // }

  showElement(element) {
    console.log(element);
    // let bob = (this.ksats | filterMTTL:filterObj | filterByArray: 'wr_spec' :wrObj)?.length
    console.log('filterObj', JSON.stringify(element));
    const x = document.getElementById('mttl');
    console.log('x', x);
  }

  setEvalCardLength() {
    this.itemsPerSlide = 3;
    this.linksPerSlide = 3;
  }

  setOtherCardLength() {
    this.itemsPerSlide = 5;
    this.linksPerSlide = 5;
  }

  topicPlaceBasedOnLength(length): Record<string, unknown> {
    if (length > 64) {
      return {
        'vertical-align': 'top',
        'font-size': '90%', '-moz-transform': 'scale(.90)'
      };
    }
    else if (length > 42) {
      return { 'font-size': '95%', '-moz-transform': 'scale(.90)' };
    }
    else if (length > 36) {
      return { 'font-size': '90%', '-moz-transform': 'scale(.95)' };
    }
    return {};
  }

  styleBasedOnLength(length): Record<string, unknown> {
    if (length > 180) {
      return {
        'vertical-align': 'top', 'text-align': 'top', '-transform': 'scaleY(.90)',
        'padding-top': '5px ', 'padding-right': '10px'
      };
    }
    else if (length > 116) {
      return {
        'vertical-align': 'top', 'text-align': 'top', '-transform': 'translateY(10px)',
        'padding-top': '5px ', 'margin-top': '5px', 'margin-right': '10px',
      };
    }
    return {};
  }

  selectColumnsDialog(): void {
    this.cols_prev = this.viewCols;
    const dialogRef = this.dialog.open(SelectColumnsComponent,
      { data: this.viewCols }
    );
    dialogRef.disableClose = true;
    dialogRef.afterClosed().subscribe((data) => {
      if (data){
        this.viewCols = data;
        this.updateColumns();
      }
    });
  }

  // displayedColumns: string[] = ['ksat_id','description', 'topic', 'requirement_owner', 'requirement_src', 'wr_spec',
  // 'child_count', 'parent_count', 'training_count', 'eval_count', 'OID', 'comments']
  updateColumns(): void {
    this.displayedColumns = ['coverage'];
    this.viewCols.forEach(element => {
      if (element.checked){
        if (element.id === 'training_count'){
          this.displayedColumns.push(element.id);
          this.displayedColumns.push('add_training');
        }
        else if (element.id === 'eval_count'){
          this.displayedColumns.push(element.id);
          this.displayedColumns.push('add_eval');
        }
        else if (element.id === 'wr_spec' && element.subColumns.find(elem => elem.id === 'proficiency_data'
        && elem.checked)) {
          this.displayedColumns.push('proficiency_data');
        }
        else {
          this.displayedColumns.push(element.id);
        }
      }
    });
    this.displayedColumns.push('theEnd');
    // this.cookieService.set('viewcolscookie', JSON.stringify(this.viewCols));
    this.changeDetect.detectChanges();
  }
  changeSort(sort): void {
    // Changes sort column and sets variable used by frontend
    if (this.sortColumn !== sort) {
      this.sortColumn = sort;
      this.sortDirection = '';
      this.sortCombo = [this.sortDirection + this.sortColumn];
    } else {
      this.changeSortDirection();
    }
  }


  changeSortDirection(): void {
    // Changes sort direction and handles sorting on same column (inverts results)
    if (this.sortDirection === '') { this.sortDirection = '-'; }
    else { this.sortDirection = ''; }
    this.sortCombo = [this.sortDirection + this.sortColumn];
  }


  changeWR(workRole): void {
    // Setter function for work roles
    // if All is selected, set it to predefined list
    if (workRole !== 'All') { this.wrObj = [workRole]; }
    else { this.wrObj = this.allWR; }
    this.page = 1;
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.darkModeOn = halfmoon.darkModeOn;
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        const chng = changes[propName];
        const cur: any = chng.currentValue;
        const prev: any = chng.previousValue;
      }
    }
  }

  goToPage(pageName: string): void {
    const navDetails: string[] = ['../'];
    if (pageName.length) {
      navDetails.push(pageName);
    }
    this.router.navigate(navDetails);
  }

  bug_dialog(): void {
    if (this.isLoggedIn) {
      const dialogRef = this.dialog.open(BugReportComponent,
        {
          data: {
            bug_short: [],
            bug_desc: [],
          }
        });
      dialogRef.afterClosed().subscribe(result => { });
    } else {
      this.redirectToAuth();
    }
  }


  ngAfterViewInit(): void {
    this.loadProfile();
    this.darkModeOn = halfmoon.darkModeOn;
  }


  ngOnInit(): void {
    // When DOM is ready, do this:
    this.getAllKSATs();
    // Default on ready function for halfmoon UI
    halfmoon.onDOMContentLoaded();
    this.darkModeOn = halfmoon.darkModeOn;

    if (this.cookieService.check('mttlcolprefs')) {
      const cookieData = JSON.parse(this.cookieService.get('mttlcolprefs'));
      if (cookieData[0].subColumns){ //Delete old cookies
        this.viewCols = cookieData;
      } else {
        this.cookieService.delete('mttlcolprefs');
      }
    }

    //pulls the autocomplete data into a json.
    this.autocomplete = this.apiService.getAutocomplete();

    if (this.cookieService.get('mttl-pagination')) {
      this.pageSize = +this.cookieService.get('mttl-pagination');
    }

    this.introJS.setOptions(introData);

    this.introJS.onafterchange((targetElement: any) => {
      switch (targetElement.id) {
        case 'coverage_legend':
          const element = document.getElementsByClassName('introjs-tooltip');
          const boxArrow = document.getElementsByClassName('introjs-arrow top');
          const numberLayer = document.getElementsByClassName('introjs-helperNumberLayer');
          element.item(0).setAttribute('style', 'top:21px; right:-200');
          element.item(1).setAttribute('style', 'top:21px; right:-200');
          // boxArrow.item(0).setAttribute("style", "display: block");
          numberLayer.item(0).setAttribute('style', 'left: 0; top:0;');
          break;
        case 'm4b':
          introJs().setOption('tooltipPosition', 'top');
          break;
      }
    });

    this.cookieValue = this.cookieService.get('mttl');

    if (this.cookieValue !== '2') {
      if (halfmoon.getPreferredMode() === 'dark-mode') {
        halfmoon.toggleDarkMode();
      }
      this.introJS.start();
      this.cookieService.set('mttl', '2', 365);
    }

    //for testing only
    //this.cookieService.delete('ProfileforMTTL', '/');



    if (this.cookieService.check('ProfileforMTTL')) {
      this.profileCookieValue = this.cookieService.get('ProfileforMTTL');
    }

    if (this.cookieService.check('viewcolscookie')) {
      this.viewCols = JSON.parse(this.cookieService.get('viewcolscookie'));
    }






    this.code = this.route.snapshot.queryParamMap.get('code');
    if (this.code !== '' && this.code !== undefined && this.code !== null) {
      this.profileService.getTokenWithCode(this.code);
      //this.updateApplication();
      if (!localStorage.getItem('refreshed')) {
        localStorage.setItem('refreshed', 'no reload');
        location.reload();
      }
    }

    // These subscriptions are where the component gets updated values from profile service
    // We should give them an end condition


    this.profileService.profileNameChange.subscribe(value => {
      this.profileName = value;
      if (this.profileName !== undefined && this.profileName !== '') {
        this.isLoggedIn = true;
        // this.updateApplication();
        // this is not necessary except for Firefox
        if (!localStorage.getItem('FF-refresh')) {
          localStorage.setItem('FF-refresh', 'no reload');
          setTimeout(window.location.reload.bind(window.location), 90);
        }
      }
    }
    );


    this.profileService.profileInfoChange.subscribe(value => {
      this.profileInfo = value;
    }
    );

    this.profileService.profileUserNameChange.subscribe(value => {
      this.username = value;
    }
    );



    const urlWR = this.route.snapshot.paramMap.get('workrole');
    if (urlWR) {
      this.clickableFilter('wr_spec', urlWR.toUpperCase());
    }

    this.updateColumns();
  }

  downloadCSV(): void {
      this.exportdata.downloadFile('csv', JSON.parse(JSON.stringify(this.ksats)));
  }

  downloadXLSX(): void {
    this.exportdata.downloadFile('xlsx', JSON.parse(JSON.stringify(this.ksats)));
  }

  paginationChange(num): void {
    this.cookieService.set('mttl-pagination', num, 365);
    this.pageSize = num;
  }


  checkboxSelect(i, name): void {
    const tabs = document.getElementsByName('tabs-' + i.toString());
    const valueKey = 'value';
    const checkedKey = 'checked';
    tabs.forEach(tab => {
      if (tab[valueKey] !== name) {
        tab[checkedKey] = false;
      }
    });
  }


  descFromKsatId(ksatId): string {
    let output = '';
    this.ksats.forEach(ksat => {
      if (ksat.ksat_id === ksatId) {
        output = ksat.description;
      }
    });
    return output;
  }


  getColor(ksatId: string): string {
    let color = '#000000';
    switch (ksatId[0]) {
      case ('K'): {
        color = '#00abd4';
        break;
      }
      case ('S'): {
        color = '#b43000';
        break;
      }
      case ('A'): {
        color = '#b48a00';
        break;
      }
      case ('T'): {
        color = '#5d00b4';
        break;
      }
      default: {
        break;
      }
    }
    return color;
  }


  determineIcon(ksatId: string): any {
    let icon = faBook;
    switch (ksatId[0]) {
      case ('K'): {
        icon = faBook;
        break;
      }
      case ('S'): {
        icon = faHammer;
        break;
      }
      case ('A'): {
        icon = faToolbox;
        break;
      }
      case ('T'): {
        icon = faHome;
        break;
      }
      default: {
        break;
      }
    }
    return icon;
  }


  getHref(html: string): string {
    try {
      const re = /href\s*=\s*"([^"]*)"/;
      return re.exec(html)[1];
    } catch {
      return '#';
    }
  }


  getAnchorText(html: string): string {
    try {
      const re = />([^<]*)</;
      const text = (re.exec(html)[1]);
      return text.replace(/[_-]/g, ' ');
    } catch {
      return 'No String';
    }
  }


  add(event: MatChipInputEvent): void {
    const value = event.value;
    const column = this.filterCol.value;
    if (column === 'search') {
      this.searchKSATList(value);
    }
    else {
      try {
        this.filterKSATList(column.trim(), value.trim());
      } catch {
        console.log('Error: could not add filter');
      }

    }

    // Reset the input value
    this.filterInput.nativeElement.value = '';
  }

  addBySelection(event: MatAutocompleteSelectedEvent): void {
    const value = event.option.viewValue;
    const column = this.filterCol.value;
    this.filterKSATList(column.trim(), value.trim());
    this.filterInput.nativeElement.value = '';
  }

  searched(col: any[]): any[] {
    const output = [];
    col.forEach(element => {
      if (element.column === 'search') {
        output.push(element);
      }
    });
    return output;
  }

  filtered(col: any[]): any[] {
    const output = [];
    col.forEach(element => {
      if (element.column !== 'search') {
        output.push(element);
      }
    });
    return output;
  }
  test(str: string, element: any): void {
    console.log(str, element);
  }
}
