import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { ApiService } from '../api.service';
import { MatDialog } from '@angular/material/dialog';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { KsatLinkPromptComponent } from '../ksat-link-prompt/ksat-link-prompt.component';
import { NewKsatPopupComponent } from '../new-ksat-popup/new-ksat-popup.component';

import { CookieService } from 'ngx-cookie-service';
import * as introJs from 'intro.js/intro.js';


import { faBook,
         faStopwatch,
         faUsers,
         faPlus,
         faSearch,
         faChild,
         faSortUp,
         faSortDown,
         faSort,
         faHammer,
         faToolbox,
         faHome,
         faCheckDouble,
         faChalkboardTeacher,
         faMapMarker
} from '@fortawesome/free-solid-svg-icons';

import { CoverageCalculatorPipe } from '../pipes/coverage-calculator.pipe';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-idf-ksats',
  templateUrl: './idf-ksats.component.html',
  styleUrls: ['./idf-ksats.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ CoverageCalculatorPipe ]
})
export class IdfKsatsComponent implements OnInit {

  // Definitions for temporary variablesl
  @ViewChild('filterCol') filterCol;
  @ViewChild('filterInput') filterInput;
  @ViewChild('searchInput') searchInput;

  // introjs
  cookieValue: string;
  introJS = introJs();

  // Main ksat list populated from API call
  ksats;

  // Pagination count
  pageSize = 10;

  // Pagination options
  page = 1;
  count = 0;
  tableSize = 7;
  tableSizes = [3, 6, 9, 12];

  // Sorting defaults
  sortDirection = '';
  sortColumn = 'ksat_id';
  sortCombo = [this.sortDirection + this.sortColumn];

  // Filtering placeholders
  filterObj = []; // Format expected: {"column": "filter_text"}
  // Defining "All" work roles
  allWR = ['All'];
  wrObj = this.allWR;


  // FA icons
  // This looks dumb but I'm following the instructions here:
  // https://www.npmjs.com/package/@fortawesome/angular-fontawesome
  faBook = faBook;
  faStopwatch = faStopwatch;
  faUsers = faUsers;
  faPlus = faPlus;
  faSearch = faSearch;
  faChild = faChild;
  faSortUp = faSortUp;
  faSortDown = faSortDown;
  faSort = faSort;
  bothCovered = faCheckDouble;
  faTrn = faChalkboardTeacher;
  unmapped = faMapMarker;
  // Searchbar settings
  visible = true;
  selectable = false;
  removable = true;
  addOnBlur = false;
  readonly separatorKeysCodes: number[] = [COMMA, ENTER];

  // Expanded Pane Carousel Settings
  itemsPerSlide = 5;
  linksPerSlide = 7;
  singleSlideOffset = false;
  noWrap = true;

  autocomplete;
  searchFC = new FormControl ();
  filterOptions = ['Work Role', 'Topic', 'Owner', 'Source', 'Parent', 'KSAT ID'];

  /* eslint-disable */
  // ESLint doesn't like how these properties are named.
  filterMappings = {
      Owner: 'requirement_owner',
      Source: 'requirement_src',
      Parent: 'parent',
      'KSAT ID': 'ksat_id',
      Topic: 'topic',
      'Work Role': 'wr_spec'
  };

  /* eslint-enable */
  constructor(
    private apiService: ApiService,
    private dialog: MatDialog,
    private cookieService: CookieService
  ) { }


  addFilter(filter): void {
    let toAdd = true;
    this.filterObj.forEach(element => {
      if (element.column === filter.column && element.filterText === filter.filterText){
        toAdd = false;
      }
    });

    if (toAdd){
      this.filterObj.push(filter);
    }
  }

  getAllIDFKSATs(): void {
    // Main function to retrieve all ksats from API service
    this.ksats = this.apiService.getAllIDFKSATs();
  }


  clearChecks(radioName): void {
    // Function to clear radio buttons based on the name
    // This closes an expanded detail pane
    const ele = document.querySelector('input[name="' + radioName + '"]:checked') as HTMLInputElement;
    ele.checked = false;
  }


  filterKSATList(column, text): void {
    let modifiedCol = '';
    if (column !== 'search'){
      modifiedCol = this.filterMappings[column];
    } else {
      modifiedCol = column;
    }

    // Adds a key value pair to the filter_obj array to be filtered on
    this.addFilter({ column: modifiedCol, filterText: text });
    this.filterInput.nativeElement.value = '';
    this.searchInput.nativeElement.value = '';
    this.page = 1;
  }

  clear(){
    this.searchFC.reset();
    this.filterObj = [];
    this.searchInput.nativeElement.value = '';
    this.filterInput.nativeElement.value = '';
  }

  clickableFilter(filterColumn, text): void {
    this.addFilter({ column: filterColumn, filterText: text });
    this.page = 1;
  }


  searchKSATList(value): void {
    // Adds a special key value pair with the "search" column prepopulated
    // This notifies the filter pipe to do an expensive full text search on every result
    // this.filter_obj.push({ 'column': 'search', 'filter_text': this.search_input.nativeElement.value })
    this.addFilter({ column: 'search', filterText: value });
    this.page = 1;
    // console.log('search value:', this.search_input.nativeElement.value);
  }


  removeFilter(column, text): void {
    // Removes specified key value pair from filter_obj
    this.filterObj.forEach( (element, index) => {
      if (element.column === column && element.filterText === text) {
        this.filterObj.splice(index, 1);
      }
    });
    this.page = 1;
  }


  addKsatDialog(): void {
    const dialogRef = this.dialog.open(NewKsatPopupComponent, {
      height: 'auto',
      width: '410px'
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }


  ksatLinkDialog(ksat, ksatType): void {
    const dialogRef = this.dialog.open(KsatLinkPromptComponent, {
      data: {
        ksatId: ksat.ksat_id,
        type: ksatType
      }
    });

  }


  changeSort(sort): void {
    // Changes sort column and sets variable used by frontend
    if (this.sortColumn !== sort) {
      this.sortColumn = sort;
      this.sortDirection = '';
      this.sortCombo = [this.sortDirection + this.sortColumn];
    } else {
      this.changeSortDirection();
    }
  }


  changeSortDirection(): void {
    // Changes sort direction and handles sorting on same column (inverts results)
    if (this.sortDirection === '') { this.sortDirection = '-'; }
    else { this.sortDirection = ''; }
    this.sortCombo = [this.sortDirection + this.sortColumn];
  }


  changeWR(workRole): void {
    // Setter function for work roles
    // if All is selected, set it to predefined list
    if (workRole !== 'All') { this.wrObj = [workRole]; }
    else { this.wrObj = this.allWR; }
    this.page = 1;
  }


  ngOnInit(): void {
    // When DOM is ready, do this:
    // this.getAllKSATs();

    //

    this.getAllIDFKSATs();
    // Default on ready function for halfmoon UI
    //pulls the autocomplete data into a json.
    this.autocomplete = this.apiService.getAutocomplete();
  }


  getFilename(): any {
    const d = new Date();
    const date = d.getDate();
    const month = d.getMonth() + 1;
    const year = d.getFullYear().toString().substr(-2);
    const hour = d.getHours();
    const minutes = d.getMinutes();
    const dateStr = month + '_' + date + '_' + year + '_' + hour + '_' + minutes;
    const fileNameWithDate = 'MTTL' + '_' + dateStr;
    return fileNameWithDate;
  }


  checkboxSelect(i, name): void {
    const tabs = document.getElementsByName('tabs-' + i.toString());
    const valueKey = 'value';
    const checkedKey = 'checked';
    tabs.forEach(tab => {
      if (tab[valueKey] !== name){
        tab[checkedKey] = false;
      }
    });
  }


  descFromKsatId(ksatId): string {
    let output = '';
    this.ksats.forEach(ksat => {
      if (ksat.ksat_id === ksatId){
        output = ksat.description;
      }
    });
    return output;
  }


  getColor(ksatId: string): string {
    let color = '#000000';
    switch (ksatId[0]){
      case ('K'): {
        color = '#00abd4';
        break;
      }
      case ('S'): {
        color = '#b43000';
        break;
      }
      case ('A'): {
        color = '#b48a00';
        break;
      }
      case ('T'): {
        color = '#5d00b4';
        break;
      }
      default: {
        break;
      }
    }
    return color;
  }


  determineIcon(ksatId: string): any {
    let icon = faBook;
    switch (ksatId[0]){
      case ('K'): {
        icon = faBook;
        break;
      }
      case ('S'): {
        icon = faHammer;
        break;
      }
      case ('A'): {
        icon = faToolbox;
        break;
      }
      case ('T'): {
        icon = faHome;
        break;
      }
      default: {
        break;
      }
    }
    return icon;
  }


  getHref(html: string): string {
    try{
      const re = /href\s*=\s*"([^"]*)"/;
      return re.exec(html)[1];
    } catch {
      return '#';
    }
  }


  getAnchorText(html: string): string {
    try{
      const re = />([^<]*)</;
      const text = (re.exec(html)[1]);
      return text.replace(/[_-]/g, ' ');
    } catch {
      return 'No String';
    }
  }


  add(event: MatChipInputEvent): void {
    const value = event.value;
    const column = this.filterCol.nativeElement.value;
    if (column === 'search'){
      this.searchKSATList(value);
    }
    else
    {
      try{
        this.filterKSATList(column.trim(), value.trim());
      } catch {
        console.log('Error: could not add filter');
      }

    }

    // Reset the input value
    this.filterInput.nativeElement.value = '';
  }

  addBySelection(event: MatAutocompleteSelectedEvent): void{
    const value = event.option.viewValue;
    const column = this.filterCol.value;
    this.filterKSATList(column.trim(), value.trim());
    this.filterInput.nativeElement.value = '';
  }

  searched(col: any []): any []{
    const output = [];
    col.forEach(element => {
      if (element.column === 'search'){
        output.push(element);
      }
    });
    return output;
  }

  filtered(col: any []): any []{
    const output = [];
    col.forEach(element => {
      if (element.column !== 'search'){
        output.push(element);
      }
    });
    return output;
  }

  uniqueValue(col: any[]): any[]{
    const output = [];
    for(const val of col){
      if(!output.includes(val)){
        output.push(val);
      }
    }
    return output;
  }
}
