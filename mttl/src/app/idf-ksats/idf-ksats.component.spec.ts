import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdfKsatsComponent } from './idf-ksats.component';

describe('IdfKsatsComponent', () => {
  let component: IdfKsatsComponent;
  let fixture: ComponentFixture<IdfKsatsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdfKsatsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdfKsatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
