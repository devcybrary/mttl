# Introduction-to-git :

## Linked KSATs: 
A0694, A0695, K0001, K0645, K0680, K0720, K0721, K0722, K0723, K0724, K0725, K0726, K0728, K0729, K0731, K0732, K0890, S0018, S0019, S0020, S0021, S0022, T0013

## Measurement: 
+ Written
+ Performance

## Lecture Time: 
+ 45 Minutes

## Demo/Performance Time:
+ 1 Hour

## Instructional Methods:
+ Informal Lecture & Demonstration/Performance

## Multiple Instructor Requirements: 
+ 1:8 for Labs

## Classification: UNCLASSIFIED
------
## Lesson Objectives:
**LO 1** Identify purpose for and types of version control including git (Proficiency Level: B)

**LO 2** Articulate the basic process for using git (Proficiency Level: B)

**LO 3** Identify how gitlab integrates to the CI/CD pipeline (Proficiency Level: B)

**LO 4** Identify work-role / environment specific git processes (Proficiency Level: B)

**LO 5** Identify more advanced git and gitlab functions (Proficiency Level: B)

--------
## Performance Objectives (Proficiency Level: 3c)

**Conditions:**

+ Given access to (references, tools, etc.):
+ Access to specified remote virtual environment
+ Student Guide and Lab Guide
+ Student Notes

**Performance/Behavior Tasks:**

+ Demonstrate proficiency with fundamental git commands
+ Implement more advanced git commands and functionality

**Standard(s):**

+ Criteria: Demonstration: Correctable to 100% in class
+ Evaluation: Students will have 4 hours to complete the timed evaluation consisting of both cognitive and performance components.
+ Minimum passing score is 80%

-----------
# References
+ http://gitforwindows.org/
+ http://rogerdudler.github.io/git-guide/
+ https://docs.gitlab.com/ee/README.html
+ https://docs.gitlab.com/ee/ci/introduction/index.html
+ https://docs.gitlab.com/ee/ci/pipelines/index.html
+ https://docs.gitlab.com/ee/ci/yaml/README.html
+ https://docs.gitlab.com/ee/ci/yaml/README.html#stages
+ https://git-scm.com/book/en/v2/Appendix-C%3A-Git-Commands-Basic-Snapshotting
+ https://git-scm.com/docs
+ https://git-scm.com/docs/gittutorial
+ https://learngitbranching.js.org/
+ https://medium.com/@lulu.ilmaknun.q/kompilasi-meme-git-e2fe49c6e33e
+ https://oer.gitlab.io/oer-courses/vm-oer/03-Git-Introduction.html#/slide-vcs-review
+ https://tom.preston-werner.com/2009/05/19/the-git-parable.html
+ https://try.github.io/levels/1/challenges/1
+ https://www.atlassian.com/git/tutorials
+ https://www.atlassian.com/git/tutorials/what-is-git
+ https://www.tutorialspoint.com/git/index.htm
