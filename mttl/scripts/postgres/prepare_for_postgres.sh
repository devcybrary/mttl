#! /bin/bash
echo "Preparing for postgres!!!"
pip3 install psycopg2-binary
pip3 install sqlalchemy
pip3 install pandas
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt focal-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update -y 
# If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':
sudo apt-get -y install postgresql-client
#ls 
#pwd
#find -iname "MTTL.min.json"
cp ./mttl/src/data/MTTL.min.json ./mttl/scripts/postgres/
cp ./mttl/src/data/autocomplete.min.json ./mttl/scripts/postgres/ 
echo "Ready to postgres."
exit 0