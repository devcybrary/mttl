#! /bin/bash 

echo "Making local copy of MTTL.min.json"
#we could just import each of the JSONs
cp ../../src/data/MTTL.min.json .
echo "removing dashes and slashes for Postgres"
sed -i 's/work-roles/work_roles/g' MTTL.min.json 
sed -i 's/work-roles/work_roles/g' MTTL.min.json 
sed -i 's1roles/spec1roles_spec1g' MTTL.min.json 
sed -i 's/references/refs/g' MTTL.min.json 
sed -i 's/Comments/comments/g' MTTL.min.json 