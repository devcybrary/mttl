import pymongo
import os
import json

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links
wrs = db.work_roles

output_file = "mttl/src/data/autocomplete.min.json"
fields = ["ksat_id", "topic", "requirement_src", "requirement_owner"]


def main():
    data = {}
    for field in fields:
        data[field] = reqs.distinct(field)
        if "" in data[field]:
            data[field].remove("")

    data["work_role"] = wrs.distinct("work-role")

    with open(output_file, "w+") as of:
        json.dump(data, of)


if __name__ == "__main__":
    main()
