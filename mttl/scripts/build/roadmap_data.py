#!/usr/bin/env python3

import argparse
import json
from os import listdir
from os.path import isfile, join

directory = './mttl/database/work-roles'
output_file = 'roadmap.json'
root_wr = '90COS'


def build_data(dir: str):
    output_data = []
    files = [f for f in listdir(dir) if isfile(join(dir, f))]
    for file in files:
        if("json" in file):
            with open(f'{dir}/{file}') as file:
                data = json.load(file)

            try:
                for node in data['roadmap_nodes']:
                    node['name'] = data['name']
                    node['fullName'] = data['fullName']
                    node['description'] = data['description']
                    node['paths'] = data['paths']
                    node['has_ttl'] = len(data['TTL']) > 0
                    output_data.append(node)
            except KeyError:
                print(f"ERROR: roadmap_nodes does not exist in {file}")
                exit(1)
    if output_data:
        return output_data
    else:
        print("Error: no workroles with node info in directory")
        exit(2)


def main():
    parser = argparse.ArgumentParser(
        description='Compile workrole TTLs into roadmapDataStructure')
    parser.add_argument(
        '-o',
        '--output',
        type=str,
        help='name of output file')
    parser.add_argument(
        '-d',
        '--dir',
        type=str,
        help='directory of workrole jsons')
    parsed_args = parser.parse_args()

    of = output_file
    if (parsed_args.output):
        of = parsed_args.output
    input_dir = directory
    if (parsed_args.dir):
        input_dir = parsed_args.dir

    full_data = build_data(input_dir)
    with open(of, "w+") as output:
        json.dump(full_data, output, indent=4)


if __name__ == "__main__":
    main()
