#!/usr/bin/python3

import json
import argparse
import os
from pathlib import Path
from ordered_set import OrderedSet


DATA_FNAME = "data.json"
MTTL_POSTREQ_KEY = "follow-ons"
MTTL_TRN_KEY = "trn-rel-link-urls"
PHASE_KEY = "phases"
COURSES_KEY = "courses"


def open_json(fname):
    try:
        with open(fname) as spec:
            data = json.load(spec)
    except FileNotFoundError:
        print("file", fname, "could not be found")
    except json.JSONDecodeError:
        print("file", fname, "is not valid JSON")
    return data


# find the roles that have no prerequisites and
# combine all jsons into a master dictionary
def find_progenitors(file_data):
    role_dict = {}  # a master dictionary of all roles
    check_set = OrderedSet()  # a set to track roles that have no prerequisite
    # add each of the dictionary's contnets to a master dictionary
    for dat in file_data:
        for key in dat:
            role_dict[key] = dat[key]
            check_set.add(key)
    # iterate through each work role and remove post requisites from the set
    for key, value in role_dict.items():
        try:
            # remove each postreq of this role from the se
            for post in value[MTTL_POSTREQ_KEY]:
                check_set.remove(post)
        except KeyError:
            pass

    # return the set of roles with no prerequisites and
    # the dictionary of all roles
    return check_set, role_dict


def build_train_obj(role, url, incr):
    name = url.split("/")[-1]
    trnid = role + str(incr)
    return {"trngId": trnid, "name": name, "url": url}


# add a role and all post reqs
def register_role(role, role_dict, added_set, phases, phase_counter):
    try:
        if role not in added_set:
            # add this role to a set that tracks the roles already added
            added_set.add(role)

            # create a new role object
            new_role = {}
            new_role["id"] = role
            try:
                new_role["linksTo"] = role_dict[role][MTTL_POSTREQ_KEY]
            except KeyError:
                new_role["linksTo"] = []
            # add the courses
            try:
                new_role["courses"] = role_dict[role][COURSES_KEY]
            except KeyError:
                new_role["courses"] = []
            phases[phase_counter].append(new_role)
        # if there are postrequisites, register them as well
        if role_dict[role][MTTL_POSTREQ_KEY]:
            # each postreq should be one phase higher, so increase the counter
            phase_counter += 1
            # if this will create a new phase, append it to the phases array
            if phase_counter >= len(phases):
                phases.append([])

            for follow_on in role_dict[role][MTTL_POSTREQ_KEY]:
                register_role(
                    follow_on,
                    role_dict,
                    added_set,
                    phases,
                    phase_counter
                )

    except KeyError:
        return


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("inputs", nargs="+", type=str)
    parser.add_argument("-o", "--output", type=Path, default=Path("."))
    pargs = parser.parse_args()

    # If output dir does not exist, create it
    if not os.path.isdir(pargs.output):
        os.mkdir(pargs.output)

    roadmap_data = {}

    file_data = []
    for input_file in pargs.inputs:
        file_data.append(open_json(input_file))

    initial_roles, role_dict = find_progenitors(file_data)

    added_set = set()
    # for each initial work role
    for initial in initial_roles:
        phases = [[]]
        phase_counter = 0
        # register the initial role and any postreqs
        register_role(initial, role_dict, added_set, phases, phase_counter)
        roadmap_data[initial] = {PHASE_KEY: phases}

    with open(str(Path.joinpath(pargs.output, DATA_FNAME)), "w") as data_f:
        json.dump(roadmap_data, data_f, indent=4)


if __name__ == "__main__":
    main()
