#!/usr/bin/env python3

import json
import os
import argparse


def main():
    parser = argparse.ArgumentParser(
        description='Compile workrole TTLs into one TTL '
        'for the Mongo Database')
    parser.add_argument(
        'input_file',
        type=str,
        help='path to the rel-link directory')
    parser.add_argument(
        'output_file',
        type=str,
        help='specify the template to validate rel-link files')
    parser.add_argument(
        '-a',
        '--append',
        action="store_true",
        help="append to existing file"
    )
    parsed_args = parser.parse_args()

    open_mode = 'w+'

    with open(parsed_args.input_file, 'r') as inf:
        data = json.load(inf)

    # Grab TTL Data
    data = data['TTL']

    if (
        os.path.isfile(parsed_args.output_file) and
        parsed_args.append
    ):
        try:
            with open(parsed_args.output_file) as of:
                old_data = json.load(of)

            for datum in old_data:
                data.append(datum)
        except json.decoder.JSONDecodeError:
            print(
                "ERROR: Output file exists, but could not be loaded. "
                f"Is it in JSON format?\n{parsed_args.output_file}"
            )
            exit(1)

    with open(parsed_args.output_file, open_mode) as of:
        json.dump(data, of)


if __name__ == "__main__":
    main()
