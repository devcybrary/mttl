#!/bin/bash

if [ -z ${MONGO_HOST} ]; then
    MONGO_HOST=localhost;
fi

mongo --host $MONGO_HOST --eval 'db.dropDatabase()' mttl

# import all requirement files into mongodb requirements colleciton
if [ -d mttl/database/requirements ]; then
    mongo --host $MONGO_HOST --eval 'db.requirements.createIndex({"ksat_id": 1}, {unique: true})' mttl
    for f in mttl/database/requirements/*.json
    do
        until mongoimport --host $MONGO_HOST --db mttl --collection requirements --file $f --jsonArray; do sleep 1; done
    done
else
    echo "requirements failed!"
fi


# import all supplementary files into mongodb supplementary collection
if [ -d mttl/database/supplementary ]; then
    for f in mttl/database/supplementary/*.json
    do
        echo "importing $f from supplementary"
        mongoimport --host $MONGO_HOST --db mttl --collection supplementary --file $f --jsonArray
    done
else
    echo "before_script.sh: importing supplementary training to mongo failed!"
fi

# import all rel-link files into mongodb rel_links collection
if [ -d mttl/database/rel-links/training ]; then
    for f in mttl/database/rel-links/training/*.rel-links.json
    do
        mongoimport --host $MONGO_HOST --db mttl --collection rel_links --file $f --jsonArray
    done
else
    echo "rel-links/training failed!"
fi

if [ -d mttl/database/rel-links/eval ]; then
    for f in mttl/database/rel-links/eval/*.rel-links.json
    do
        mongoimport --host $MONGO_HOST --db mttl --collection rel_links --file $f --jsonArray
    done
else
    echo "rel-links/eval failed!"
fi

# import all work-role files into mongodb work_roles colleciton
if [ -d mttl/database/work-roles ]; then
    for f in mttl/database/work-roles/*.json
    do
        python3 mttl/scripts/build/compile-TTLs.py $f TTLs.json -a
    done
    mongoimport --host $MONGO_HOST --db mttl --collection work_roles --file TTLs.json --jsonArray;
else
    echo "Work-roles failed!"
fi