#! /usr/bin/python3

import json
import report_ccd_change
import get_old_work_role_status
import get_current_workrole_status
from urllib.parse import quote

time_ago = '1 week ago'
old_ccd_work_role_file = 'oldCCD.json'
ccd_file = 'newCCD.json'
old_tasks_file = "oldTasks.json"
old_abilities_file = "oldAbilities.json"
old_skills_file = "oldSkills.json"
old_knowledge_file = "oldKnowledge.json"

new_ccd_work_role_file = "newCCD.json"

new_tasks_file = "newTasks.json"
new_abilities_file = "newAbilities.json"
new_skills_file = "newSkills.json"
new_knowledge_file = "newKnowledge.json"


def compare(string1, string2, no_match_c=' ', match_c='|'):
    if len(string2) < len(string1):
        string1, string2 = string2, string1
    result = ''
    n_diff = 0
    for c1, c2 in zip(string1, string2):
        if c1 == c2:
            result += match_c
        else:
            result += no_match_c
            n_diff += 1
    delta = len(string2) - len(string1)
    result += delta * no_match_c
    n_diff += delta
    return (result, n_diff)


def get_change_message(changed_ksats, worth_mentioning_changes):
    changed_ksat_num = 0
    change_notification = "\
            The following KSATs\
            have changed: "
    for changed_ksat in changed_ksats:
        if changed_ksat_num != 1:
            changed_ksat_num = 1
            change_notification = change_notification + changed_ksat
        else:
            change_notification = change_notification + ", " + changed_ksat
    for wm in worth_mentioning_changes:
        change_notification = change_notification + wm
    return change_notification


def add_added_ksats(added_ksats, new_ccd_ksats):
    change_notification = "\n\nThe following KSATs\
            have been added: \n\n"
    for added_ksat in added_ksats:
        ksat_desc = ""
        for line in new_ccd_ksats:
            if line['ksat_id'] == added_ksat:
                ksat_desc = line['description']
        change_notification = change_notification + \
            "{} -> {}\n\n\n".format(added_ksat, ksat_desc)
    return change_notification


def del_deleted_ksats(del_ksats):
    change_notification = "\n\nThe following KSATs\
            have been deleted from the CCD work role: \n\n"
    del_ksat_num = 0
    for del_ksat in del_ksats:
        if del_ksat_num != 1:
            del_ksat_num = 1
            change_notification = change_notification + del_ksat
        else:
            change_notification = change_notification + ", " + del_ksat
    return change_notification


def find_ksat_changes(new_ccd_ksats, old_ccd_ksats):
    change_result = ""
    changed_ksats = []
    worth_mentioning_changes = []
    new_ksats = []
    old_ksats = []
    added_ksats = []
    deleted_ksats = []
    for old_line in old_ccd_ksats:
        old_ksat = old_line['ksat_id']
        old_ksats.append(old_ksat)
    for new_line in new_ccd_ksats:
        new_ccd_ksat = new_line['ksat_id']
        new_ksats.append(new_ccd_ksat)
        for old_line in old_ccd_ksats:
            old_ccd_ksat = old_line['ksat_id']
            if new_ccd_ksat == old_ccd_ksat:
                new_value_desc = new_line['description']
                old_value_desc = old_line['description']
                cmp_char, n_diffs = compare(
                    new_value_desc, old_value_desc, no_match_c='_')
                line1 = "{}  difference(s)\n\n".format(n_diffs)
                line2 = "    new: {}\n\n".format(new_value_desc)
                # line3 = "    ____ {}\n\n\n".format(cmpChar)
                line4 = "    old: {}\n\n\n\n".format(old_value_desc)
                line5 = "************************************\n\n"
                temp_change_result = line1 + line2 + line4 + line5
                if n_diffs > 5:
                    changed_ksats.append(new_ccd_ksat)
                    worth_mentioning_changes.append(temp_change_result)

    added_ksats = [i for i in new_ksats if i not in old_ksats]
    deleted_ksats = [i for i in old_ksats if i not in new_ksats]
    print("new not old", added_ksats)
    print("old not new", deleted_ksats)
    add_message = ""
    if len(added_ksats) > 0:
        add_message = add_added_ksats(added_ksats, new_ccd_ksats)
    del_message = ""
    if len(deleted_ksats) > 0:
        del_message = del_deleted_ksats(deleted_ksats)
    if len(changed_ksats) > 0:
        change_message = get_change_message(
            changed_ksats,
            worth_mentioning_changes
        ) + ' '
    change_result = change_message + add_message + del_message
    return change_result


def find_work_role_expectation_changes():
    work_role_expectation_changes = ""
    with open(old_ccd_work_role_file) as old_ccd_check:
        old_ccd_data = json.load(old_ccd_check)
        old_ttl = (old_ccd_data['TTL'])

    with open(new_ccd_work_role_file) as new_ccd_check:
        new_ccd_data = json.load(new_ccd_check)
        new_ttl = (new_ccd_data['TTL'])

    old_proficiencies = []
    for ksat in old_ttl:
        line = '"{}": "{}"'.format(ksat["ksat_id"], ksat["proficiency"])
        try:
            newline = "{" + line + "}"
            res = json.loads(newline)
            old_proficiencies.append(res)
        except Exception as uhoh:
            print(uhoh, "\n old proficiency add failed on ", line)

    new_proficiencies = []
    for ksat in new_ttl:
        line = '"{}": "{}"'.format(ksat["ksat_id"], ksat["proficiency"])
        try:
            newline = "{" + line + "}"
            res = json.loads(newline)
            new_proficiencies.append(res)
            # print("new prof: ".format(res))
        except Exception as uhoh:
            print(uhoh, "\n new proficiency failed on ", line)

    newnotold = [i for i in new_proficiencies if i not in old_proficiencies]
    oldnotnew = [i for i in old_proficiencies if i not in new_proficiencies]
    # print(newnotold)
    # print(oldnotnew)
    added_ksats = []
    deleted_ksats = []
    changed_ksats = []
    changes_to_report = ""
    if len(newnotold) > 0 or len(oldnotnew) > 0:
        changes_to_report = "\n\n***\n\n\
The following changes have occurred\
with CCD work role expectations: \n\n"
    for new in newnotold:
        for key in new:
            if any(key in old for old in oldnotnew):
                changed_ksats.append(key)
            else:
                added_ksats.append(new)
    for old in oldnotnew:
        for key in old:
            if not any(key in new for new in newnotold):
                deleted_ksats.append(old)

    if len(added_ksats) > 0:
        changes_to_report = changes_to_report + \
            "The following KSATs and \
corresponding proficiencies have\
been added to the CCD work role:\n\n"
        for ksat in added_ksats:
            changes_to_report = changes_to_report + "   - {}\n\n".format(ksat)

    if len(deleted_ksats) > 0:
        changes_to_report = changes_to_report + \
            "\n\n***\n\nThe following KSATs \
have been deleted from the CCD work role:\n\n"
        already_deleted = ""
        for ksat in deleted_ksats:
            for key in ksat:
                if len(already_deleted) < 2:
                    changes_to_report = changes_to_report + "{}".format(key)
                    already_deleted = "{}".format(key)
                else:
                    changes_to_report = changes_to_report + ", {}".format(key)

    if len(changed_ksats) > 0:
        changes_to_report = changes_to_report + \
            "***\n\nThe following CCD related \
             KSATs have changed proficiencies:\n\n"
        for ksat in changed_ksats:
            changes_to_report = changes_to_report + "\n    {}\n\n".format(ksat)
            changes_to_report = changes_to_report + \
                "     - old value: {}\n\n".format(
                    next(item for item in old_proficiencies if item[ksat]))
            changes_to_report = changes_to_report + \
                "     - new value: {}\n\n".format(
                    next(item for item in new_proficiencies if item[ksat]))
    work_role_expectation_changes = changes_to_report
    return work_role_expectation_changes


def main():
    new_ccd_ksats = get_current_workrole_status.main("CCD")
    old_ccd_ksats = get_old_work_role_status.main("CCD", time_ago)
    print("Identifying changes in CCD expectations and related KSATs if any.\
            Updating CCD KSAT status.")
    ksat_changes = str(find_ksat_changes(new_ccd_ksats, old_ccd_ksats))
    expectation_changes = find_work_role_expectation_changes()
    all_changes = ksat_changes + expectation_changes
    if len(all_changes) > 5:
        print(all_changes)
        report_ccd_change.main(quote(all_changes))
    else:
        print("No changes to report")


if __name__ == "__main__":
    main()
