#! /usr/bin/python3
import sys
import subprocess

# old KSATs
old_tasks_file = "oldTasks.json"
old_abilities_file = "oldAbilities.json"
old_skills_file = "oldSkills.json"
old_knowledge_file = "oldKnowledge.json"
# old work roles
old_90cos_work_role_file = "old90COS.json"
old_ac_work_role_file = "oldAC.json"
old_ccd_work_role_file = "oldCCD.json"
old_cst_work_role_file = "oldCST.json"
old_instructor_work_role_file = "oldInstructor.json"
old_jsre_work_role_file = "oldJSRE.json"
old_mccd_work_role_file = "oldMCCD.json"
old_po_work_role_file = "oldPO.json"
old_sad_work_role_file = "oldSAD.json"
old_sccdl_work_role_file = "oldSCCD-L.json"
old_sccdw_work_role_file = "oldSCCD-W.json"
old_see_work_role_file = "oldSEE.json"
old_sm_work_role_file = "oldSM.json"
old_spo_work_role_file = "oldSPO.json"
old_ssm_work_role_file = "oldSSM.json"
old_ssre_work_role_file = "oldSSRE.json"
old_sysadmin_work_role_file = "oldSysadmin.json"
old_tae_work_role_file = "oldTAE.json"
# current work role files
ninety_cos_file = "90COS.json"
ac_file = "AC.json"
ccd_file = "CCD.json"
cst_file = "CST.json"
instructor_file = "Instructor.json"
jsre_file = "JSRE.json"
mccd_file = "MCCD.json"
po_file = "PO.json"
sad_file = "SAD.json"
sccdl_file = "SCCD-L.json"
sccdw_file = "SCCD-W.json"
see_file = "SEE.json"
sm_file = "SM.json"
spo_file = "SPO.json"
ssm_file = "SSM.json"
ssre_file = "SSRE.json"
sysadmin_file = "Sysadmin.json"
tae_file = "TAE.json"


new_ksats = []
old_ksats = []
new_expectations = []
old_expectations = []


def write_old_file(rev_file, old_work_role_file):
    with open(old_work_role_file, 'w') as write_here:
        p = subprocess.Popen(['git', 'show', rev_file], stdout=write_here)
        p.wait()


def get_old_ccd_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + ccd_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    print("rev file", rev_file)
    write_old_file(rev_file, old_ccd_work_role_file)


def get_old_sccdl_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + sccdl_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_sccdl_work_role_file)


def get_old_sccdw_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + sccdw_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_sccdw_work_role_file)


def get_old_mccd_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + mccd_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_mccd_work_role_file)


def get_old_po_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + po_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_po_work_role_file)


def get_old_spo_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + spo_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_spo_work_role_file)


def get_old_sm_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + sm_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_sm_work_role_file)


def get_old_ssm_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + ssm_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_ssm_work_role_file)


def get_old_ac_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + ac_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_ac_work_role_file)


def get_old_90cos_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + ninety_cos_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_90cos_work_role_file)


def get_old_tae_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + tae_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_tae_work_role_file)


def get_old_see_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + see_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_see_work_role_file)


def get_old_jsre_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + jsre_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_jsre_work_role_file)


def get_old_sad_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + sad_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_sad_work_role_file)


def get_old_sysadmin_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + sysadmin_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_sysadmin_work_role_file)


def get_old_instructor_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + instructor_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_instructor_work_role_file)


def get_old_ssre_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + ssre_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_ssre_work_role_file)


def get_old_cst_work_role_file(rev1):
    old_file_name = ":mttl/database/work-roles/" + cst_file
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_ssre_work_role_file)


def get_old_work_role_file(file_to_get, rev1):
    if file_to_get == "CCD":
        print("getting old CCD file")
        get_old_ccd_work_role_file(rev1)
    elif file_to_get == "SCCDL":
        get_old_sccdl_work_role_file(rev1)
    elif file_to_get == "SCCDW":
        get_old_sccdw_work_role_file(rev1)
    elif file_to_get == "MCCD":
        get_old_mccd_work_role_file(rev1)
    elif file_to_get == "SM":
        get_old_sm_work_role_file(rev1)
    elif file_to_get == "SSM":
        get_old_ssm_work_role_file(rev1)
    elif file_to_get == "AC":
        get_old_ac_work_role_file(rev1)
    elif file_to_get == "CST":
        get_old_cst_work_role_file(rev1)
    elif file_to_get == "TAE":
        get_old_tae_work_role_file(rev1)
    elif file_to_get == "SEE":
        get_old_see_work_role_file(rev1)
    elif file_to_get == "INSTRUCTOR":
        get_old_instructor_work_role_file(rev1)
    elif file_to_get == "SYSADMIN":
        get_old_sysadmin_work_role_file(rev1)
    elif file_to_get == "SAD":
        get_old_sad_work_role_file(rev1)
    elif file_to_get == "JSRE":
        get_old_jsre_work_role_file(rev1)
    elif file_to_get == "90COS":
        get_old_90cos_work_role_file(rev1)
    elif file_to_get == "AC":
        get_old_ac_work_role_file(rev1)
    else:
        print("Invalid File selection: {}.".format(file_to_get))
        print("Please choose from a Work Role file:  90COS,AC,CCD,CST,Instructor,\
JSRE,MCCD,PO,SAD,SCCD-L,SCCD-W,SEE,SM,SPO,SSM,SSRE,Sysadmin,TAE")
        print("or ")
        print("Choose a requirements file:\
ABILITIES, KNOWLEDGE, SKILLS, TASKS")
        sys.exit()


def get_old_task_file(rev1):
    old_file_name = ":mttl/database/requirements/TASKS.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_tasks_file)


def get_old_abilities_file(rev1):
    old_file_name = ":mttl/database/requirements/ABILITIES.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_abilities_file)


def get_old_skills_file(rev1):
    old_file_name = ":mttl/database/requirements/SKILLS.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_skills_file)


def get_old_knowledge_file(rev1):
    old_file_name = ":mttl/database/requirements/KNOWLEDGE.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, old_knowledge_file)


def get_old_ksat_file(file_to_get, rev1):
    if (file_to_get[0]) == 'T':
        get_old_task_file(rev1)
    elif (file_to_get[0]) == 'A':
        get_old_abilities_file(rev1)
    elif (file_to_get[0]) == 'S':
        get_old_skills_file(rev1)
    elif (file_to_get[0]) == 'K':
        get_old_knowledge_file(rev1)
    else:
        print("Error retrieving invalid old KSAT filename for: ", file_to_get)

# training rel link files
# i.e. mttl/database/rel-links/training/


def get_old_actp_linux_file_rl(rev1):
    old_file_name = ":mttl/database/rel-links/training/\
ACTP_Linux.rel-links.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, "oldACTP_Linux.rel-links.json")


def get_old_all_personnel_rl(rev1):
    old_file_name = \
        ":mttl/database/rel-links/training/All_Personnel.rel-links.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, "oldAll_Personnel.rel-links.json")


def get_old_ghidra_rl(rev1):
    old_file_name = ":mttl/database/rel-links/training/Ghidra.rel-links.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, "oldGhidra.rel-links.json")


def get_old_idf_rl(rev1):
    old_file_name = ":mttl/database/rel-links/training/IDF.rel-links.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, "oldIDF.rel-links.json")


def get_old_professional_scrum_product_owner_rl(rev1):
    old_file_name = \
        ":mttl/database/rel-links/training/\
Professional_Scrum_Product_Owner_I.rel-links.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(
        rev_file, "oldProfessional_Scrum_Product_Owner_I.rel-links.json")


def get_old_see_trl(rev1):
    old_file_name = ":mttl/database/rel-links/training/SEE.rel-links.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, "oldSEE.training.rel-links.json")


def get_old_tae_rl(rev1):
    old_file_name = ":mttl/database/rel-links/training/TAE.rel-links.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, "oldTAE.rel-links.json")


def get_old_training_file(file_to_get, rev1):
    print("getting Training rel link", file_to_get)
    first_two = file_to_get[0:2]
    if first_two == "AC":
        get_old_actp_linux_file_rl(rev1)
    elif first_two == "AL":
        get_old_all_personnel_rl(rev1)
    elif first_two == "GH":
        get_old_ghidra_rl(rev1)
    elif first_two == "ID":
        get_old_idf_rl(rev1)
    elif first_two == "PR":
        get_old_professional_scrum_product_owner_rl(rev1)
    elif first_two == "SE":
        get_old_see_trl(rev1)
    else:
        print("Invalid Training Rel-Link Specified: ", file_to_get)
        sys.exit()

# eval rel-links


def get_old_ccd_rl(rev1):
    old_file_name = ":mttl/database/rel-links/eval/CCD.rel-links.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, "oldCCD.rel-links.json")


def get_old_po_rl(rev1):
    old_file_name = ":mttl/database/rel-links/eval/PO.rel-links.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, "oldPO.rel-links.json")


def get_old_sccdl_rl(rev1):
    old_file_name = ":mttl/database/rel-links/eval/SCCD-L.rel-links.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, "oldSCCD-L.rel-links.json")


def get_old_sccdw_rl(rev1):
    old_file_name = ":mttl/database/rel-links/eval/SCCD-W.rel-links.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, "oldSCCD-W.rel-links.json")


def get_old_see_erl(rev1):
    old_file_name = ":mttl/database/rel-links/eval/SEE.rel-links.json"
    rev_file = "{}{}".format(str(rev1), old_file_name)
    write_old_file(rev_file, "oldSEE.eval.rel-links.json")


def get_old_eval_file(file_to_get, rev1):
    print("getting old Eval rel link file ", file_to_get)
    first_two = file_to_get[0:2]
    if first_two == "CC":
        get_old_ccd_rl(rev1)
    elif first_two == "PO":
        get_old_po_rl(rev1)
    elif first_two == "SC":
        if file_to_get == "SCCD-L":
            get_old_sccdl_rl(rev1)
        elif file_to_get == "SCCD-W":
            get_old_sccdw_rl(rev1)
    elif first_two == "SE":
        get_old_see_erl(rev1)
    else:
        print("Invalid Eval  Rel-Link Specified: ", file_to_get)
        sys.exit()


def get_old_rel_links_file(file_to_get, rev1):
    orig = file_to_get
    file_to_get = file_to_get.split(".")[0].upper()
    if file_to_get in ["ACTP_LINUX", "ALL_PERSONNEL", "GHIDRA",
                       "IDF", "PROFESSIONAL_SCRUM_PRODUCT_OWNER_I",
                       "SEET", "TAE", "ACTP", "ALL", "PRO"]:
        get_old_training_file(file_to_get, rev1)
    elif file_to_get in ["CCD", "PO", "SCCD-L", "SCCD-W", "SEE"]:
        get_old_eval_file(file_to_get, rev1)
    else:
        print("Invalid rel-link file specified: ", orig)


def get_old_file(file_to_get, rev1):
    if file_to_get in ["90COS", "AC", "CCD", "CST", "INSTRUCTOR", "JSRE",
                       "MCCD", "PO", "SAD", "SCCDL", "SCCDW", "SEE",
                       "SM", "SPO", "SSM", "SSRE", "SYSADMIN", "TAE"]:
        get_old_work_role_file(file_to_get, rev1)
    elif file_to_get in ["ACTP_Linux.rel-links",
                         "All_Personnel.rel-links", "Ghidra.rel-links",
                         "IDF.rel-links",
                         "Professional_Scrum_Product_Owner_I.rel-links",
                         "SEE.rel-links", "TAE.rel-links",
                         "CCD.rel-links", "PO.rel-links",
                         "SCCD-L.rel-links", "SCCD-W.rel-links",
                         "SEE.rel-links",
                         "ACTP_LINUX.REL-LINKS", "ALL_PERSONNEL.REL-LINKS",
                         "GHIDRA.REL-LINKS", "IDF.REL-LINKS",
                         "PROFESSIONAL_SCRUM_PRODUCT_OWNER_I.REL-LINKS",
                         "SEE.REL-LINKS", "TAE.REL-LINKS",
                         "ACTP_LINUX.REL", "ALL_PERSONNEL.REL",
                         "GHIDRA.REL", "IDF.REL",
                         "PROFESSIONAL_SCRUM_PRODUCT_OWNER_I.REL",
                         "SEE.REL", "TAE.REL", "SEET.REL",
                         "SCCD-L.REL", "SCCD-W.REL"]:
        get_old_rel_links_file(file_to_get, rev1)
    elif file_to_get in ["K", "S", "A", "T",
                         "ABILITIES", "KNOWLEDGE", "SKILLS", "TASKS"]:
        get_old_ksat_file(file_to_get, rev1)
    else:
        print("Invalid File selection: {}.".format(file_to_get))
        print("Please choose from a Work Role file:\
90COS,AC,CCD,CST,Instructor,JSRE,MCCD,PO,SAD,\
SCCD-L,SCCD-W,SEE,SM,SPO,SSM,SSRE,Sysadmin,TAE")
        print("or ")
        print("Choose a requirements file:\
ABILITIES, KNOWLEDGE, SKILLS, TASKS")
        print("or")
        print("Choose a training or eval rel-link file i.e.")
        print(" Training: ACTP_LINUX, ALL_PERSONNEL,GHIDRA,\
IDF,PROFESSIONAL_SCRUM_PRODUCT_OWNER_I,\
SEET,TAE, ACTP, ALL, PRO (use 'SEET' for SEE Training)")
        print(" Eval: CCD,PO,SCCD-L,SCCD-W,SEE")
        print("     \
    - followed by .rel or .rel-links to specify the rel-link file")
        sys.exit()


def main(file_to_get, time_ago):
    time_string = "--before=\"{}\"".format(time_ago)
    rev1 = subprocess.check_output(
        ['git', 'rev-list', '-n1',
         time_string, 'master']).decode("utf-8").strip()
    file_to_get = file_to_get.upper()
    if file_to_get.find("REL"):
        get_old_file(file_to_get, rev1)
    else:
        file_to_get = ''.join(e for e in file_to_get if e.isalnum())
        get_old_file(file_to_get, rev1)


if __name__ == "__main__":
    file_to_get = sys.argv[1]
    time_ago = sys.argv[2]

    main(file_to_get, time_ago)
