'''
Validates that every ksat with a requirement source has a requirement owner
'''

import pymongo
import os
import json

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links
wrs = db.work_roles

file = "missing_req_owners.json"
error_msg = "Requirement source without requirement owner"


def main():
    missing_data = {}
    for req in reqs.find():
        if (req['requirement_src'] and not req['requirement_owner']):
            missing_data[str(req['_id'])] = error_msg

    if (missing_data):
        with open(file, 'w') as log_file:
            json.dump(missing_data, log_file, indent=4)
            exit(1)  # throw error


if __name__ == "__main__":
    main()
