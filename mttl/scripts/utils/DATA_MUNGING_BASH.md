
## Data munging in bash - reminders

sudo apt install -y jq

jq cat IDF.rel-links.json |jq ' .[] | [.KSATs | .[].ksat_id | .[]], .module '

cat IDF.rel-links.json |jq ' .[] | select(.module == "pseudocode")| [.KSATs | .[].ksat_id | .[]]| join(", ")'

rm ksats; for oid in $(cat IDF.rel-links.json |jq ' .[] | select(.module == "python")| select(.topic=="functions") | [.KSATs | .[].ksat_id | .[]]| join(", ")' | sed -e 's/,/\n/g' -e 's/"//g' | grep -oi "[0-9a-z]*" ); do ./oid_to_ksat.py "$oid" >> ksats; done

wc ksats; for ksat in $(cat ksats |sort); do echo "CCD,"Introduction to Python", $ksat" >> oam_csv.csv; done 

