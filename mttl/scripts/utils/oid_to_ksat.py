#! /usr/bin/python3
import csv
import sys


mttl_file = sys.argv[1]
oid = sys.argv[2]

with open(mttl_file, 'rt') as csvfile:
    reader = csv.reader(csvfile, quotechar='"')
    for row in reader:
        if (row[1] == oid):
            print(row[2])
