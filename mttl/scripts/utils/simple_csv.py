#! /usr/bin/python3
import csv
import sys


if len(sys.argv) < 2:
    print("You should specify a location for the exported MTTL.csv")
    sys.exit()
else:
    file = sys.argv[1]

with open(file, 'rt') as csvfile:
    reader = csv.reader(csvfile, quotechar='"')
    for row in reader:
        # The last two rows are training and eval
        # print( '{}, {}, {}, {}, {}, {}'.format(row[1],\
        # row[2].strip(), row[4].strip(), row[5].strip(), row[-2], row[-1]))
        print('{}, {}, {}, {}'.format(
            row[1], row[2].strip(), row[4].strip(), row[5].strip()))
