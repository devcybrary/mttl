#! /usr/bin/python3
import argparse
import json


try:
    with open(
        '../../database/rel-links/training/IDF.rel-links.json'
    ) as data_file:
        idf = json.load(data_file)
except Exception:
    print('The IDF.rel-links.json file should be located at\
           mttl/database/rel-links/training/IDF.rel-links.json')

try:
    with open('../../src/data/MTTL.min.json') as data_file:
        mttl = json.load(data_file)
except Exception:
    print('!!! The MTTL.min.jsonfile is created by running:\
          ./build_frontend.sh\
          at the top level i.e. mttl-dev')

blank_ref = {
    "title": "",
    "type": "",
    "description": "",
    "url": ""
}

# add module name on export "module_name":""
blank_module = {
    "uniq_id": "",
    "uniq_name": "",
    "description": "",
    "workrole": "",
    "group": "",
    "objective": "",
    "prerequisites": [],
    "content": [],
    "performance": [],
    "required_tasks": [],
    "required_abilities": [],
    "required_skills": [],
    "required_knowledge": [],
    "resources": [],
    "comments": ""
}


def get_desc_from_ksat_id(ksat_id):
    for obj in mttl:
        if (obj['ksat_id'] == ksat_id):
            return obj['description']
    return ''


def get_desc_from_lesson_objectives(rel_link):
    lesson_objectives = rel_link['lesson_objectives']
    objective_descriptions = []
    for lesson in lesson_objectives:
        objective_descriptions.append(lesson['description'])

    description = ";\n".join(objective_descriptions)
    return(description)


def get_performance_objectives(rel_link):
    performance_objectives = rel_link['performance_objectives']
    objective_descriptions = []
    for perf in performance_objectives:
        objective_descriptions.append(perf['description'])

    description = ";\n".join(objective_descriptions)
    if description:
        return(description)
    else:
        return(get_desc_from_lesson_objectives(rel_link))


def get_workrole(rel_link):
    wr = rel_link['work-roles']
    return(wr[0])


def get_content_from_ksats(rel_link):
    content = []
    content_for_module = []
    ksats = rel_link['KSATs']
    for ksat in ksats:
        for key, value in ksat.items():
            if key == "url":
                content.append(value)
    uniq = list(set(content))
    for resource in uniq:
        content_for_module.append({"description": resource, "ref": resource})
    return content_for_module


def get_labs_from_performance_objs(rel_link):
    performance_objs = rel_link['performance_objectives']
    objective_descriptions = []
    labs_for_module = []
    for perf in performance_objs:
        objective_descriptions.append(perf['description'])
    for resource in objective_descriptions:
        labs_for_module.append({"description": resource, "perf": "TBD"})
    return labs_for_module


def get_evals_from_performance_objs(rel_link):
    performance_objectives = rel_link['performance_objectives']
    objective_descriptions = []
    tests_for_module = []
    for perf in performance_objectives:
        objective_descriptions.append(perf['description'])
    for resource in objective_descriptions:
        tests_for_module.append({"description": resource, "eval": "TBD"})
    return tests_for_module


def disaggregate_ksats(rel_link):
    ksats = rel_link['KSATs']
    tasks = []
    abilities = []
    skills = []
    knowledge = []
    for ksat in ksats:
        for key, ksat_id in ksat.items():
            desc = get_desc_from_ksat_id(ksat_id)
            if key == "ksat_id":
                if "T" in ksat_id:
                    tasks.append(
                        {
                            "ksat_id": ksat_id,
                            "item_proficiency": ksat['item_proficiency'],
                            "description": desc
                        })
                elif "A" in ksat_id:
                    abilities.append({"ksat_id": ksat_id,
                                      "item_proficiency": ksat[
                                          'item_proficiency'
                                        ],
                                      "description": desc})
                elif "S" in ksat_id:
                    skills.append(
                        {
                            "ksat_id": ksat_id,
                            "item_proficiency": ksat['item_proficiency'],
                            "description": desc
                        })
                elif "K" in ksat_id:
                    knowledge.append(
                        {
                            "ksat_id": ksat_id,
                            "item_proficiency": ksat['item_proficiency'],
                            "description": desc
                        })
                else:
                    print("Nothing found for ", ksat_id)
    return {"tasks": tasks, "abilities": abilities,
            "skills": skills, "knowledge": knowledge}


def references_to_supplemental_resources(rel_link):
    refs = rel_link['references']
    refs_to_resources = []
    for ref in refs:
        refs_to_resources.append({"title": ref, "type": "reference",
                                 "description": "Reference from IDF",
                                  "url": ref})
    return refs_to_resources


def parse_rel_link(rel_link):
    new_module = blank_module
    new_module['uniq_id'] = rel_link['module_id']
    new_module['uniq_name'] = rel_link['topic']
    new_module['description'] = get_desc_from_lesson_objectives(rel_link)
    new_module['workrole'] = get_workrole(rel_link)
    new_module['group'] = rel_link['module']
    new_module['objective'] = get_performance_objectives(rel_link)
    new_module['prerequisites'] = [{"uniq_id": "none"}]
    new_module['content'] = get_content_from_ksats(rel_link)
    new_module['performance'] = get_labs_from_performance_objs(rel_link)
    # until we can manually disaggregate practice labs from qualifying content
    new_module['assessment'] = get_evals_from_performance_objs(rel_link)
    # separate and parse ksats
    # the description is added to facilitate manual determination
    parsed_ksats = disaggregate_ksats(rel_link)
    new_module['required_tasks'] = parsed_ksats['tasks']
    new_module['required_abilities'] = parsed_ksats['abilities']
    new_module['required_skills'] = parsed_ksats['skills']
    new_module['required_knowledge'] = parsed_ksats['knowledge']
    new_module['resources'] = references_to_supplemental_resources(rel_link)
    new_module['comments'] = "generated by rellink to module script"
    return new_module


def get_rel_link_to_parse(oid):
    for obj in idf:
        for key, value in obj.items():
            if key == "_id":
                if(value.get('$oid') == oid):
                    return obj
    else:
        print("oid not found")
        return()


def main():
    # put oid of IDF topic to be parsed to json
    parser = argparse.ArgumentParser(
        description='creating module from\
                                     IDF.rel-links.json and\
                                     MTTL.min.json')
    parser.add_argument('oid', type=str,
                        help='The oid of the IDF lesson must be specified\
                              The oid is found in\
                              ../../database/rel-links/training/IDF.rel-links.json\
                              i.e.\
                              "_id": {\
                              "$oid": THIS VALUE }')
    parsed_args = parser.parse_args()
    oid = parsed_args.oid
    rel_link = get_rel_link_to_parse(oid)
    json_info = parse_rel_link(rel_link)
    json_to_export = json.dumps({json_info["uniq_name"]: json_info}, indent=4)
    print(json_to_export)
    json_file = json_info["uniq_name"] + ".json"
    with open(json_file, "w") as export_json:
        export_json.write(json_to_export)


main()
