#!/usr/bin/python3

import os
import re
import pymongo
import argparse
from datetime import date
from bson.objectid import ObjectId

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links


def regex_type_validation(arg_value, pat=re.compile(r"^[0-9a-f]{24}$")):
    if not pat.match(arg_value):
        raise argparse.ArgumentTypeError
    return arg_value


def delete_ksat_data(id: str):
    id = ObjectId(id)
    ret = reqs.delete_one({'_id': id})
    if ret.deleted_count < 1:
        print(f'No KSAT associated with {str(id)}')
        return

    # remove all parents with the deleted ksat_id
    reqs.update_many(
        {'parent': id},
        {
            '$pull': {'parent': id},
            '$set': {'updated_on': f'{date.today()}'}
        }
    )

    # remove ksat_id from all rel_links that maps to it
    rls.update_many(
        {'KSATs.ksat_id': id},
        {
            '$pull': {'KSATs': {'ksat_id': id}},
            '$set': {'updated_on': f'{date.today()}'}
        }
    )


def delete_rel_link_data(id: str):
    rel_link = rls.find_one({'_id': ObjectId(id)})
    if rel_link is not None:
        # remove the rel-link
        rls.delete_one({'_id': ObjectId(id)})
    else:
        print(f'No rel-link associated with {id}')


def main():
    parser = argparse.ArgumentParser(
        description='remove ksat/rel-link objects from datasets')
    parser.add_argument(
        'type',
        choices=['ksat', 'rel-link'],
        help='select what type of item you want to remove'
    )
    parser.add_argument(
        'id_list',
        metavar='_id',
        nargs='+',
        type=regex_type_validation,
        help='list of either ksat or rel-link _id\'s'
    )
    parser.add_argument(
        '-v',
        "--verbose",
        action="store_true",
        help="increase output verbosity"
    )
    parsed_args = parser.parse_args()

    for item in parsed_args.id_list:
        if parsed_args.type == 'ksat':
            if parsed_args.verbose:
                print("Deleting KSAT: ", item)
            delete_ksat_data(item)
        else:
            if parsed_args.verbose:
                print("Deleting REL Link: ", item)
            delete_rel_link_data(item)


if __name__ == "__main__":
    main()
