#!/usr/bin/python3

import os
import sys
import pymongo
import argparse
import jsonschema
import json
from datetime import date
from bson.objectid import ObjectId
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from json_templates import rel_link_mapping_item_template
from mongo_helpers import get_new_ksat_key

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

ksat_avoid_field = ['training', 'eval']


def validate_insert_mapping_data(data: dict, schema_func):
    '''
    validate all insertion data
    '''
    try:
        jsonschema.validate(instance=data, schema=schema_func())
    except jsonschema.exceptions.ValidationError as err:
        print(err)
        exit(1)


def update_ksat_query(reqs: object, update_query: list):
    find_query = update_query[0]
    if '_id' in find_query:
        find_query['_id'] = ObjectId(find_query['_id'])

    found = list(reqs.find(find_query, {"_id": 1, "map_for": 1}))
    update_list = ', '.join([str(item['_id']) for item in found])
    print(f'Will be Updated: {update_list}')
    ret = reqs.update_many(find_query, update_query[1])

    if ret.modified_count == 0:
        print('No KSATs were modified')
        exit(1)


def update_rel_link_query(rls: object, update_query: dict):
    find_query = update_query[0]
    if '_id' in find_query:
        find_query['_id'] = ObjectId(find_query['_id'])

    found = list(rls.find(find_query))
    update_list = ', '.join([str(item['_id']) for item in found])
    print(f'Will be Updated: {update_list}')
    ret = rls.update_many(find_query, update_query[1])

    if ret.modified_count == 0:
        print('No Rel-Links were modified')
        exit(1)


def insert_rel_link_mapping(reqs, rls, mapping: list):
    new_map = {
        'ksat_id': reqs.find_one({'ksat_id': mapping[1]})['_id'],
        'item_proficiency': mapping[2],
        'url': mapping[3]
    }
    validate_insert_mapping_data(new_map, rel_link_mapping_item_template)

    find_query = {
        '_id': ObjectId(mapping[0])
    }

    found = list(rls.find(find_query, {"_id": 1, "map_for": 1}))
    update_list = ', '.join([str(item['_id']) for item in found])
    print(f'Insert KSAT mappings into: {update_list}')
    # will insert KSATs into all rel-links found with the find-query
    rls.update_many(
        find_query,
        {
            '$push': {
                "KSATs": new_map
            }
        }
    )


def update_rel_link_mapping(rls, find_query: dict, mappings: list, attr: str):
    if '_id' in find_query:
        find_query['_id'] = ObjectId(find_query['_id'])

    update_list = ', '.join(
        [str(item['_id']) for item in list(
            rls.find(find_query, {"_id": 1})
        )]
    )
    print(f'Update KSAT mapping in: {update_list}')

    for mapping in mappings:
        tmp_map = mapping.split(':')
        ksat = tmp_map.pop(0)
        val = ':'.join(tmp_map)
        tmp_find_query = {'KSATs.ksat_id': ksat}
        tmp_find_query.update(find_query)

        rls.update_one(
            tmp_find_query,
            {
                '$set': {
                    f'KSATs.$.{attr}': val,
                    'updated_on': f'{date.today()}'
                }
            }
        )


def delete_rel_link_mapping(reqs, rls, find_query: dict, ksats: list):
    if '_id' in find_query:
        find_query['_id'] = ObjectId(find_query['_id'])

    found = list(rls.find(find_query, {"_id": 1, "map_for": 1}))
    update_list = ', '.join([str(item['_id']) for item in found])
    print(f'Delete KSAT mappings from: {update_list}')
    if len(update_list) == 0:
        print('Did not find any matching rel-links from the find-query')
        exit(1)

    # will delete KSATs from all rel-links within find-query
    ksats = [ksat['_id'] for ksat in reqs.find({'ksat_id': {'$in': ksats}})]
    rls.update(
        find_query,
        {
            '$pull': {
                "KSATs": {
                    'ksat_id': {'$in': ksats}
                }
            }
        }
    )


def update_ksat_id(reqs: object, rls: object, mapping: str):
    mapping = mapping.split(':')  # mapping[0] = old, mapping[1] = new
    find_query = {'ksat_id': mapping[0]}
    update_item = reqs.find_one(find_query)
    new_id = get_new_ksat_key(reqs, mapping[1])
    if update_item is not None:
        print(f'Update KSAT ksat_id in {update_item["ksat_id"]} to {new_id}')
    else:
        print(f'No KSAT with ksat_id: {mapping[0]}')
        return

    reqs.delete_one(find_query)
    update_item['ksat_id'] = new_id
    reqs.insert_one(update_item)


def convert_to_json(item: str) -> dict:
    ret = None
    try:
        ret = json.loads(item)
    except Exception as err:
        print(err)
        exit(1)
    return ret


def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements
    rls = db.rel_links

    parser = argparse.ArgumentParser(
        description='modify script to update ksat/rel-link objects in datasets'
    )

    update_group = parser.add_mutually_exclusive_group(required=True)
    update_group.add_argument(
        '-u',
        '--update-query',
        metavar=('FIND_QUERY', 'UPDATE_QUERY'),
        nargs=2,
        type=json.loads,
        help="mongodb query to update specified documents found by find-query "
             "'{...}' (wrap KEYs and VALs in double quotes)")
    update_group.add_argument(
        '-k',
        '--modify-ksat-id',
        type=str,
        nargs='+',
        metavar=('OLD_ID:{knowledge, skills, abilities, tasks}'),
        help="list (space delimited) of KEY:VAL pairs of OLD_ID:"
             "{knowledge, skills, abilities, tasks} to update "
             "mappings of documents found by find-query")
    update_group.add_argument(
        '-i',
        '--insert-mapping',
        nargs=4,
        type=str,
        metavar=('REL_LINK_ID', 'KSAT_ID', 'PROFICIENCY', 'URL'),
        help="insert a new mapping into a rel-link. takes four values; "
             "the REL_LINK_ID, the ksat_id (5 chars), item_proficiency, "
             "and the url"
    )
    update_group.add_argument(
        '-d',
        '--delete-mapping',
        type=str,
        nargs='+',
        metavar=('FIND_QUERY', 'KSAT_ID'),
        help="deletes a mapping from rel-links. takes two or more values;"
             " the FIND_QUERY string '{...}' to find the rel-links to update, "
             "and the ksat_id of the KSAT (5 chars) to delete"
    )
    parser.add_argument('-v', "--verbose", action="store_true",
                        help="increase output verbosity")
    parser.add_argument('type', choices=['ksat', 'rel-link'],
                        help='select the type of item you want to modify')
    parsed_args = parser.parse_args()

    if parsed_args.type == 'ksat':
        if parsed_args.update_query:
            if parsed_args.verbose:
                print("Updating KSAT with Query: ", parsed_args.update_query)
            update_ksat_query(reqs, parsed_args.update_query)
        elif parsed_args.modify_ksat_id:
            # iterate every space delimited string
            for mapping in parsed_args.modify_ksat_id:
                if parsed_args.verbose:
                    print("Iterating Every space delimited string...")
                update_ksat_id(reqs, rls, mapping)
    else:
        if parsed_args.update_query:
            if parsed_args.verbose:
                print("Updating REL Link...")
            update_rel_link_query(rls, parsed_args.update_query)
        elif parsed_args.insert_mapping:
            if parsed_args.verbose:
                print("Inserting REL Link...")
            insert_rel_link_mapping(reqs, rls, parsed_args.insert_mapping)
        elif parsed_args.delete_mapping:
            if parsed_args.verbose:
                print("Deleting REL Link...")
            find_query = convert_to_json(parsed_args.delete_mapping.pop(0))
            delete_rel_link_mapping(
                reqs,
                rls,
                find_query,
                parsed_args.delete_mapping
            )


if __name__ == "__main__":
    main()
