#!/usr/bin/python3

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from urllib.parse import unquote
import create_gitlab_db
import get_profile
import os
import requests
import sys
# just comment out import json if not in use
import json


gl = requests.Session()
retries = Retry(
    total=5,
    backoff_factor=1,
    status_forcelist=[401, 502, 503, 504])
gl.mount('https://', HTTPAdapter(max_retries=retries))


def refresh_token(access_token, state) -> str:
    cid = "f02714f7797efd485c1d6602abbb2a4ff9e185355750390cf52f8632e3277ff1"
    # because linting
    client_id = cid
    try:
        part_two = os.getenv('PART_TWO')
    except Exception:
        sys.exit("PART_TWO does not exist in environment. Exiting.")
    try:
        redirect_uri = unquote(os.getenv('REDIRECT_URI'))
    except Exception:
        sys.exit("REDIRECT_URI does not exist in environment. Exiting.")
    print('refreshtoken - accesstoken ', access_token)
    res = gl.post("https://gitlab.com/oauth/token", data={
        'client_id': client_id,
        'client_secret': part_two,
        'redirect_uri': redirect_uri,
        'grant_type': 'refresh_token',
        'state': state,
        'refresh_token': access_token[3],
        'scope': 'openid'
    })
    response = res.json()
    print("refresh response", json.dumps(response, indent=4, sort_keys=True))
    parse_values_to_db(state, response)
    return(response)


def get_token(code, state) -> str:
    cid = "f02714f7797efd485c1d6602abbb2a4ff9e185355750390cf52f8632e3277ff1"
    client_id = cid
    try:
        part_two = os.getenv('PART_TWO')
    except Exception:
        sys.exit("PART_TWO does not exist in environment. Exiting.")
    try:
        redirect_uri = unquote(os.getenv('REDIRECT_URI'))
    except Exception:
        sys.exit("REDIRECT_URI does not exist in environment. Exiting.")

    # print("Python getToken")
    if create_gitlab_db.check_for_existing_user(state):
        response = create_gitlab_db.get_existing_user(state)
        print("User exists in GitlabDB")
        print(json.dumps(response, indent=4, sort_keys=True))
        return(response)
    else:
        res = gl.post("https://gitlab.com/oauth/token", data={
            'client_id': client_id,
            'client_secret': part_two,
            'redirect_uri': redirect_uri,
            'grant_type': 'authorization_code',
            'state': state,
            'code': code,
            'scope': 'openid'
        })
        response = res.json()
        print("Response from Python getToken")
        print(json.dumps(response, indent=4, sort_keys=True))
        parse_values_to_db(state, response)
        return(response)


def get_access_token(response) -> str:
    return(response['access_token'])


def parse_values_to_db(cookie, access_token_info):
    # print("The access token includes - cookie_state: {}, \
    #         access_token: {} \
    #         created_at: {} \
    #         refresh_token: {} \
    #         scope: {} \
    #         token_type: {}".format(cookie,
    #                                accessTokenInfo['access_token'],
    #                                accessTokenInfo['created_at'],
    #                                accessTokenInfo['refresh_token'],
    #                                accessTokenInfo['scope'],
    #                                accessTokenInfo['token_type']))
    if (access_token_info['access_token']):
        refresh_token = access_token_info['refresh_token']
        create_gitlab_db.insert_token_values_gitlab(
            cookie,
            access_token_info['access_token'],
            access_token_info['created_at'],
            refresh_token,
            access_token_info['scope'],
            access_token_info['token_type']
        )
    else:
        print("The access token info provided \
               to be parsed was not recognized: \
              ", access_token_info)


def main():
    print("Getting token from Python")
    if not sys.argv[1]:
        print("code was not given. This is an error.")
    else:
        code = unquote(sys.argv[1])

    if not sys.argv[2]:
        print("state was not provided. This is an error")
    else:
        state = unquote(sys.argv[2])

    if not create_gitlab_db.check_for_gitlab_db():
        create_gitlab_db.create_gitlab_db()
        create_gitlab_db.create_gitlab_table()
        create_gitlab_db.create_profile_table()
        access_token_info = get_token(code, state)

    if not create_gitlab_db.check_for_existing_user(state):
        access_token_info = get_token(code, state)
    else:
        print("refreshing token")
        access_token_info = get_token(code, state)
        access_token_info = refresh_token(access_token_info, state)

    print(json.dumps(access_token_info, indent=4, sort_keys=True))
    access_token = get_access_token(access_token_info)

    if create_gitlab_db.check_for_existing_profile(state):
        print("profile exists for ", state)
        profile = create_gitlab_db.get_existing_profile(state)
    else:
        print("Retrieving profile for oauth_validate from Gitlab")
        profile = get_profile.get_profile_from_gitlab(access_token)
        print("profile", profile)
        create_gitlab_db.insert_profile_gitlab(state, profile)


# In future, we'll want to just return profile info to the page
main()
