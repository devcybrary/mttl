#!/usr/bin/python3
import requests
import os
import sys
from urllib.parse import unquote
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import json

gl = requests.Session()
retries = Retry(
    total=5,
    backoff_factor=1,
    status_forcelist=[401, 502, 503, 504])
gl.mount('https://', HTTPAdapter(max_retries=retries))


def refresh_token(access_token, state) -> str:
    try:
        part_two = os.getenv('PART_TWO')
    except Exception:
        sys.exit("PART_TWO does not exist in environment. Exiting.")
    res = gl.post("https://gitlab.com/oauth/token", data={
        'client_id':
        'f02714f7797efd485c1d6602abbb2a4ff9e185355750390cf52f8632e3277ff1',
        'client_secret': part_two,
        'redirect_uri': 'http://localhost:5000/redirect',
        'grant_type': 'refresh_token',
        'state': state,
        'refresh_token': access_token['refresh_token'],
        'scope': 'openid'
    })
    response = res.json()
    print("refresh response", json.dumps(response, indent=4, sort_keys=True))
    parse_values_to_db(state, response)
    return(response)


def get_token(code) -> str:
    cid = 'f02714f7797efd485c1d6602abbb2a4ff9e185355750390cf52f8632e3277ff1'
    client_id = cid
    try:
        part_two = os.getenv('PART_TWO')
    except Exception:
        sys.exit("PART_TWO does not exist in environment. Exiting.")
    try:
        redirect_uri = unquote(os.getenv('REDIRECT_URI'))
    except Exception:
        sys.exit("REDIRECT_URI does not exist in environment. Exiting.")

    res = gl.post("https://gitlab.com/oauth/token", data={
        'client_id': client_id,
        'client_secret': part_two,
        'redirect_uri': redirect_uri,
        'grant_type': 'authorization_code',
        'state': code,
        'code': code,
        'scope': 'openid'
    })
    response = res.json()
    print(json.dumps(response, indent=4, sort_keys=True))
    # parseValuestoDB(state, response)
    return(response)


def get_access_token(response) -> str:
    return(response['access_token'])


def parse_values_to_db(cookie, access_token_info):
    # a pared down version i.e. no DB access
    print("The access token includes - cookie_state: {}, \
            access_token: {} \
            created_at: {} \
            refresh_token: {} \
            scope: {} \
            token_type: {}".format(
                cookie,
                access_token_info['access_token'],
                access_token_info['created_at'],
                access_token_info['refresh_token'],
                access_token_info['scope'],
                access_token_info['token_type']
            ))


def main():
    print("This is for manually testing token retrieval.")
    if not sys.argv[1]:
        print("Enter code then state.\n\
               Currently, Code must be retrieved from Gitlab.")
    else:
        code = unquote(sys.argv[1])

    # if not sys.argv[2]:
    #     print("state was not provided. This is an error")
    # else:
    #     state = unquote(sys.argv[2])

    access_token_info = get_token(code)
    print(json.dumps(access_token_info, indent=4, sort_keys=True))


# we may want to just return profile info to the page
main()
