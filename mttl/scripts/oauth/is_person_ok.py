#!/usr/bin/python3

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
import json
import requests
import sys
import os


gl = requests.Session()
retries = Retry(
    total=5,
    backoff_factor=1,
    status_forcelist=[401, 502, 503, 504])
gl.mount('https://', HTTPAdapter(max_retries=retries))


def get_groups(access_token):
    gitlab_api = 'https://gitlab.com/api/v4/groups/'

    res = gl.get(
        gitlab_api,
        headers={'PRIVATE-TOKEN': access_token})

    # print(type(res))
    response = res.json()
    # print(res.text)
    # print(json.dumps(response, indent=4, sort_keys=True))
    return response


def get_group_members(access_token, group_number):
    # gitlabApi = 'https://gitlab.com/api/v4/groups/3573341/members'
    gitlab_api = "https://gitlab.com/api/v4/groups/"\
        + str(group_number) + "/members"

    # memberURL =  gitlabApi + groupNumber + '/members',
    res = gl.get(
        gitlab_api,
        headers={'PRIVATE-TOKEN': access_token})

    response = res.json()
    # print(res.text)
    # print(json.dumps(response, indent=4, sort_keys=True))
    return response


def get_ok_group_members():
    try:
        access_token = os.getenv('ROBOTOKEN')
    except Exception:
        sys.exit("ROBOTOKEN does not exist in environment. Exiting.")

    # curl --header "PRIVATE-TOKEN: ,special token>
    # " https://gitlab.com/api/v4/groups/ |jq '.[] | .name, .id'
    ok_groups = ["3573341", "10162946", "8729179"]
    ok_people = []
    for group in ok_groups:
        group_stats = get_group_members(access_token, group)
        for index in range(len(group_stats)):
            ok_person = (group_stats[index].get('username'))
            ok_people.append(ok_person)
    return ok_people
    """
    "90COS" 3573341
    "90COS Virtual Training Records" 9535396
    "CMN" 9539340
    "CYA" 9538825
    "CYB" 9538884
    "CYC" 9539169
    "CYD" 9539247
    "CYK" 9539508
    "CYT" 8729179
    "CYT" 9535897
    "Eval" 10162946
    "Example Records" 10097680
    "IDF" 9915071
    "StuCrew" 9609137
    "Training Systems" 8467017
    """


def is_user_ok(username):
    ok_people = set(get_ok_group_members())
    for ok_person in ok_people:
        if ok_person == username:
            return True
    return False


def get_profile_from_gitlab(access_token):
    profile_url = "https://gitlab.com/oauth/userinfo?access_token="\
        + access_token
    res = gl.get(profile_url)
    response = res.json()
    # print("Profile received: ")
    # print(json.dumps(response, indent=4, sort_keys=True))
    return(json.dumps(response, indent=4, sort_keys=True))
    """{"sub":"",
        "sub_legacy":"",
        "name":"",
        "nickname":"",
        "profile":"",
        "picture":"",
        "groups":[]
        }"
"""


def main():
    try:
        username = (sys.argv[1])
    except Exception:
        sys.exit("provide username to compare")

    ok_user = is_user_ok(username)
    print(ok_user)
    return ok_user


if __name__ == '__main__':
    main()
