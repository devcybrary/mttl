#!/usr/bin/python3
import create_gitlab_db


def main():
    if not create_gitlab_db.check_for_gitlab_db():
        create_gitlab_db.create_gitlab_db()
    create_gitlab_db.create_gitlab_table()
    create_gitlab_db.create_profile_table()


# this is simply to ensure that the necessary tables exist
main()
