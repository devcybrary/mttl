#!/usr/bin/env python3

import psycopg2
import os
import sys
from urllib.parse import urlparse


def get_connection_pg() -> psycopg2.extensions.connection:
    # print("getting connection")
    try:
        pgconn = os.getenv('PGCONN')
        result = urlparse(pgconn)
        username = result.username
        password = result.password
        database = result.path[1:]
        hostname = result.hostname
        conn = psycopg2.connect(database=database,
                                user=username,
                                host=hostname,
                                password=password,
                                port=5432
                                )
        if conn is not None:
            return(conn)
        else:
            sys.exit("Postgres connection failed")
    except Exception as e:
        print(e)


def check_problematic_people_db() -> bool:
    conn = None
    conn = get_connection_pg()
    if conn is not None:
        conn.autocommit = True
        cur = conn.cursor()
        cur.execute("SELECT datname FROM pg_database;")
        list_database = cur.fetchall()
        if ("problematic",) in list_database:
            conn.close()
            return(True)
        else:
            conn.close()
            return(False)


def create_problematic_db():
    conn = None
    conn = get_connection_pg()
    if conn is not None:
        conn.autocommit = True
        cursor = conn.cursor()
        sql = '''CREATE database problematic'''
        cursor.execute(sql)
        conn.close()
    else:
        print("Error creating Problematic DB")


def get_connection_pl() -> psycopg2.extensions.connection:
    # print("getting connection")
    try:
        pgconn = os.getenv('PGCONN')
        result = urlparse(pgconn)
        username = result.username
        password = result.password
        database = "problematic"
        hostname = result.hostname
        conn = psycopg2.connect(database=database,
                                user=username,
                                host=hostname,
                                password=password,
                                port=5432
                                )
        if conn is not None:
            return(conn)
        else:
            sys.exit("Postgres connection failed")
    except Exception as e:
        print(e)


def create_problem_table():
    conn = get_connection_pl()
    cur = conn.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS problematic ( \
                 username TEXT, \
                 link TEXT, \
                 warning TEXT, \
                 posting_date DATE NOT NULL DEFAULT CURRENT_DATE \
                 )")
    conn.commit()
    conn.close()


def insert_values_problematic(username, link, warning):
    conn = None
    conn = get_connection_pl()
    if conn is not None:
        curs = conn.cursor()
        insert_query = "INSERT into problematic (username, link, warning \
                       ) VALUES \
                       (%s,%s,%s);"
        insert_data = (username, link, warning)
        curs.execute(insert_query, insert_data)
        conn.commit()
        conn.close()


def check_user_for_problem_history(username):
    conn = None
    conn = get_connection_pl()
    if conn is not None:
        curs = conn.cursor()
        search_query = "SELECT * FROM \
problematic WHERE username = '{}';".format(username)
        curs.execute(search_query)
        response = curs.fetchall()
        conn.commit()
        conn.close()
        return response


def main():
    if not check_problematic_people_db():
        create_problematic_db()
        create_problem_table()


if __name__ == '__main__':
    main()
