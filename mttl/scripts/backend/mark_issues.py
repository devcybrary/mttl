#!/usr/bin/env python3
import os
import requests
import sys
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

gl = requests.Session()
retries = Retry(
    total=5,
    backoff_factor=1,
    status_forcelist=[401, 502, 503, 504]
    )
gl.mount('https://', HTTPAdapter(max_retries=retries))

gitlab_api = 'https://gitlab.com/api/v4/projects'
private_token = os.getenv("TOKEN")

project_id = sys.argv[1]

print("Getting issues for", project_id)
issues = []
page = 1
res = gl.get(
    gitlab_api+'/'+str(project_id)+'/issues',
    data={
        'state': 'opened'
    },
    headers={'PRIVATE-TOKEN': private_token}
)
while ('next' in res.links):
    page += 1
    issues.extend(res.json())
    res = gl.get(
        gitlab_api+'/'+str(project_id)+'/issues',
        data={
            'state': 'opened',
            'page': page
        },
        headers={'PRIVATE-TOKEN': private_token}
    )
issues.extend(res.json())

for issue in issues:
    print("Editing issue #" + str(issue['iid']))
    res = gl.put(
        gitlab_api+'/'+str(project_id)+'/issues/'
        + str(issue['iid']),
        data={
            'confidential': True
        },
        headers={'PRIVATE-TOKEN': private_token}
    )
