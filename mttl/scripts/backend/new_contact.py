#!/usr/bin/env python3
import requests
import os
import sys
from urllib.parse import unquote
import datetime
import gitlab_email

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

gl = requests.Session()
retries = Retry(total=5,
                backoff_factor=1,
                status_forcelist=[401, 502, 503, 504])
gl.mount('https://', HTTPAdapter(max_retries=retries))

gitlab_api = 'https://gitlab.com/api/v4/projects'
private_token = os.getenv("TOKEN")
desc = unquote(sys.argv[1])
topic = unquote(sys.argv[2])
username = unquote(sys.argv[3])
notes = unquote(sys.argv[4])
project_id = 18738190
email_blurb = gitlab_email.get_email_blurb(username)
today = datetime.datetime.now()

data = {
    'title': 'User Contact from Form - '
    + today.strftime('%m')
    + '/'
    + today.strftime('%d')
    + '/'
    + today.strftime('%y'),
    'description': "@" + username + " Submitted Contact Form: " + desc + """

Reason: """ + topic + """

Notes: """ + notes + """

""" + email_blurb + """
/label ~customer ~"office::CYT" ~"backlog::idea" ~USER_SUBMISSIONS ~"CYT::MTTL"
/milestone %"CYT Backlog"
"""
}

res = gl.post(
    gitlab_api + '/' + str(project_id) + '/issues',
    data=data,
    headers={'PRIVATE-TOKEN': private_token})

try:
    print(res.json()['web_url'])
except KeyError:
    print("none")
