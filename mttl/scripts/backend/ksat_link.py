#!/usr/bin/env python3
import os
import requests
import sys
import gitlab_email
import json
import validators
import make_problem_people

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from urllib.parse import unquote


gl = requests.Session()
retries = Retry(total=5,
                backoff_factor=1,
                status_forcelist=[401, 502, 503, 504])
gl.mount('https://', HTTPAdapter(max_retries=retries))

gitlab_api = 'https://gitlab.com/api/v4/projects'
private_token = os.getenv("TOKEN")
ksat_id = sys.argv[1]
link_type = sys.argv[2]
username = sys.argv[4]
safety = sys.argv[5]
url = sys.argv[3]

# check for valid input
if (url.find('http://') != 0 and url.find('https://') != 0):
    formatted_url = "http://" + url
else:
    formatted_url = url

check = validators.url(formatted_url)
if check is False:
    print("%s %s %s" % ("**flag** Base URL response: ", check, formatted_url))
    sys.exit()


project_id = 18738190
url = unquote(url)
email_blurb = gitlab_email.get_email_blurb(username)
safety = unquote(safety)

safety_blurb = ''
loaded_json = False


# Strange behavior - the format that data is passed into this script
# Must be loaded twice
try:
    safety = json.loads(json.loads(safety))
    loaded_json = True
except json.decoder.JSONDecodeError:
    pass

# create db for maliciously posting peoples
make_problem_people.main()

# getting an error here where the value should be checked
try:
    if safety['threat'] != "none identified":
        make_problem_people.insert_values_problematic(
            username, formatted_url, safety['threat'])
except KeyError:
    print('%s: %s' % ("**flag**", "Key Error found"))
    sys.exit()
except TypeError:
    print('%s: %s' % ("**flag**", "Type Error found"))
    sys.exit()


user_result = make_problem_people.check_user_for_problem_history(username)


if len(user_result) > 5:
    if len(user_result) < 7:
        print("**flag** Error: reporting malicious user")
        data = {
            'title': "Malicious User Identified",
            'description': "@" + username + " has submitted more than 5 malicious links and \
is now being banned for posting too many malicious links.   \nMost recently:  \n\n" + formatted_url + "  " + """
""" + safety['threat'] + """
/label ~customer ~"office::CYT" ~"backlog::idea" ~USER_SUBMISSIONS ~"CYT::MTTL"
/milestone %"CYT Backlog" """
        }
        res = gl.post(
            gitlab_api + '/' + str(project_id) + '/issues',
            data=data,
            headers={'PRIVATE-TOKEN': private_token},
            timeout=(3, 10))
    json_response = res.json()
    try:
        json_response["web_url"]
    except KeyError:
        json_response = None
    if json_response is not None:
        print(json_response["web_url"])
    else:
        print("**flag** Error: Spam response")
    sys.exit()

if (loaded_json):
    safety_blurb += """
Content Type: """ + safety['content-type'] + """

"""
    if (safety['threat'] == 'MALWARE'):
        safety_blurb += """### Suspected Threat - Malware

    **Warning — Visiting this web site may harm your computer.** This page \
    appears to contain malicious code that could be downloaded to your \
    computer without your consent. You can learn more about harmful web \
    content including viruses and other malicious code and how to protect \
    your computer at www.StopBadware.org.

    """

    elif (safety['threat'] == "SOCIAL_ENGINEERING"):
        safety_blurb += """### Suspected Threat - Social Engineering

    **Warning — Deceptive site ahead.** Attackers on this site may trick you \
    into doing something dangerous like installing software or revealing your \
    personal information (for example, passwords, phone numbers, or credit \
    cards). You can find out more about social engineering (phishing) \
    [here](https://support.google.com/webmasters/answer/6350487?authuser=3) \
    or from www.antiphishing.org.

    """

    elif (safety['threat'] == "UNWANTED_SOFTWARE"):
        safety_blurb += """### Suspected Threat - Unwanted Software

    **Warning — The site ahead may contain harmful programs.** \
    Attackers might attempt to trick you into installing \
    programs that harm your browsing \
    experience (for example, by changing your homepage or showing \
    extra ads on sites you visit). You can learn more about unwanted \
    software at [Unwanted Software \
    Policy](https://www.google.com/about/unwanted-software-policy.html).

    """

    # PHA warnings are specific to Android
    elif (safety['threat'] == "POTENTIALLY_HARMFUL_APPLICATION"):
        safety_blurb += """### Suspected Threat - Potentially Harmful Application

    **Warning — The site ahead may contain malware.** Attackers might attempt \
    to install dangerous apps on your device that steal or delete your \
    information (for example, photos, passwords, messages, and credit cards).
    """

    elif (safety['threat'] != 'none identified'):
        safety_blurb += """### Suspected Threat - Other

    **Warning — Something could be wrong with this site.** Google Safe \
    Browsing flagged this URL as harmful with an unspecified threat \
    type. Proceed with caution.
    """

    if (safety['threat'] != "none identified"):
        safety_blurb += """
    Advisory provided by [Google Safe Browsing](https://developers.google.com/\
    safe-browsing/v4/advisory?authuser=3)"""

    if (
        'text/html' not in safety['content-type']
        and 'application/pdf' not in safety['content-type']
    ):
        safety_blurb += """### The content type for this URL is neither \
html nor pdf. Proceed with caution!

    """

    if (safety['threat'] == "none identified"):
        safety_blurb += "No threat detected"

    safety_blurb += """

Note: Google works to provide the most accurate and up-to-date \
information about unsafe web resources. However, Google cannot \
guarantee that its information is comprehensive and error-free: \
some risky sites may not be identified, and some safe sites may be \
identified in error.

"""

else:
    safety_blurb = """Unable to scan resource - threat detection \
skipped.

"""
    if validators.url(formatted_url):
        safety_blurb = "We were unable to scan this resource. Proceed with \
        caution"
    else:
        safety_blurb = "This reference does not appear to be a URL."

data = {
    'title': ksat_id + ': Add ' + link_type + ' Link',
    'description': "@" + username + " suggested: " + formatted_url + "  " + """

""" + safety_blurb + """

""" + email_blurb + """
/label ~customer ~"office::CYT" ~"backlog::idea" ~USER_SUBMISSIONS ~"CYT::MTTL"
/milestone %"CYT Backlog" """
}

res = gl.post(
    gitlab_api + '/' + str(project_id) + '/issues',
    data=data,
    headers={'PRIVATE-TOKEN': private_token})

json_response = res.json()
try:
    json_response["web_url"]
except KeyError:
    json_response = None

if json_response is not None:
    print(json_response["web_url"])
    sys.exit()
else:
    print("**flag** Error: Spam response")
    sys.exit()

# print(json.dumps(return_res, indent=4, sort_keys=True))
# print("Issue URL = ", return_res['web_url'])
# print("return_res -> ",  return_res['description'])
# ^^ for looking to give feedback to submitter
