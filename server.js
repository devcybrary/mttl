const html = __dirname + '/mttl/dist/';
const port = 5000;
const confluenceURL = "confluence.90cos.cdl.af.mil"

// Express
const compression = require('compression');
const express = require('express');
//const urlExist = require("url-exist");

var helmet = require('helmet')
var noCache = require('nocache')
var app = express();
var cors = require('cors');
var childProcess = require('child_process');
const fetch = require("node-fetch");

var spawnOptions = {
  stdio: 'inherit'
}

//Postgres Additions 
const { Pool, Client } = require('pg')

//const path = require('path')


// app.use(cors()); //maybe don't enable cors for everything
app.use(compression())
app.use(express.json())
app.use(helmet())
app.use(noCache()) // Disable user-side caching
// Static content
app.use(express.static(html))

const gitlabconn = process.env.GLCONN
const pgconn = process.env.PGCONN

// Functions for use with endpoints
function urlExists(url) {
  if (url.includes(confluenceURL)){
    return true
  }
  return fetch(url, {mode: "no-cors"})
    .then(res => true)
    .catch(err => false)
}

async function checkUser(cookie, username){
  let client = new Client(gitlabconn)
  let returnedName = ''
  const query = "SELECT * FROM profile WHERE cookie= $1"
  const data = [cookie];
  await client.connect()
  await client
    .query(query, data)
    .then(result => {
      returnedName = result['rows'][0]['nickname'];
    })
    .catch(e => 
      {console.error(e.stack)
       console.log("serverJS - Profile not found ")
       console.log(e)
      }
      )
    .then(() => client.end())
    
  return (returnedName != '' && username == returnedName)

}

async function usernameFromCookie(cookie){
  let client = new Client(gitlabconn)
  let returnedName = ''
  const query = "SELECT * FROM profile WHERE cookie= $1"
  const data = [cookie];
  await client.connect()
  await client
    .query(query, data)
    .then(result => {
      returnedName = result['rows'][0]['nickname'];
    })
    .catch(e => 
      {console.error(e.stack)
       console.log("serverJS - Profile not found ")
      }
      )
    .then(() => client.end())

  return returnedName;

}

//#                    _                    _ _            _    
//#  _ __   ___   ___ | | __   _____    ___| (_) ___ _ __ | |_  
//# | '_ \ / _ \ / _ \| | \ \ / / __|  / __| | |/ _ \ '_ \| __| 
//# | |_) | (_) | (_) | |  \ V /\__ \ | (__| | |  __/ | | | |_  
//# | .__/ \___/ \___/|_|   \_/ |___/  \___|_|_|\___|_| |_|\__| 
//# |_|                                                         
// at some point we will need to see if client calls are sufficient 
// or if we need to use pool 

//
// Postgres Connection 
// Pool is to provide availability of multiple concurrent client connections
// const pool = new Pool({
//   user: 'mttl_user',
//   host: 'postgres',
//   database: 'mttl',
//   password: '********',
//   port: 5432
// })


// //const pool = new Pool(process.env.DATABASE_URL)
// pool.connect()



// client is for singular i.e. non-concurrent, client connections 
// const client = new Client({
//   user: 'mttl_user',
//   host: 'postgres',
//   database: 'mttl',
//   password: '*******',
//   port: 5432
// })

// const pgconn = {
//   user: 'mttl_user',
//   host: 'postgres',
//   database: 'mttl',
//   password: '******',
//   port: 5432
// }

// const gitlabconn = {
//   user: 'mttl_user',
//   host: 'postgres',
//   database: 'gitlab',
//   port: 5432
// }


//const pgconn = process.env.DATABASE_URL
//console.log("serverjs pgconn db_url: ", pgconn)
//client.connect()

// Postgres functions 
// In future, for readability, and management, it will be better to use SQL files rather than text strings
//cf. utility functions [here](https://stackoverflow.com/questions/48044541/how-to-write-multi-line-sql-query-nodejs)

const get_now = {
  text: 'SELECT NOW() as now; '
};

const get_cols_ac = {

  text: "SELECT table_name, column_name, data_type FROM information_schema.columns WHERE table_name = 'autocomplete'; "
}

const list_tables = {
  text: "SELECT * FROM information_schema.tables WHERE table_schema='public';"
}

const get_ksats_mttl = {

  text: 'SELECT DISTINCT ksat_id FROM mttl;'

}

// Well, we can prevent an error from killing the whole server at least

try {

// POSTGRES Queries
app.get('/simplestpostgres', function (req, res) {
  console.log("simplest postgres test");
  //pool seems slow compare with client below - also pool doesn't get closed normally
  pool.query(get_now)
    .then(r => {
      console.log(r);
      res.json(r);
      return;
    })
    .catch(function (err) {
      console.log('\nError executing query', err.stack);
    });
})

app.get('/pgtime', function (req, res) {
  console.log("simplest postgres time test");
  //time with pool cf. client 
  pool.query(get_now)
    .then(r => {
      console.log(r);
      res.json(r)
      return; 
    }
    )
    .catch(e => console.log(e))
})

app.get('/pgctt', function (req, res) {
  console.log("postgres client time test");
  let client = new Client(pgconn)
  client.connect()
  client
    .query('SELECT NOW()')
    .then(result => {
      console.log(result),
        res.json(result)
        return
    })
    .catch(e => console.error(e.stack))
    .then(() => client.end())
})


app.get('/gcac', function (req, res) {
  console.log("column names autocomplete");
  let client = new Client(pgconn)
  client.connect()
  client
    .query(get_cols_ac)
    .then(result => {
      console.log(result),
        res.json(result)
        return
    })
    .catch(e => console.error(e.stack))
    .then(() => client.end())
})

app.get('/gkmttl', function (req, res) {
  console.log("unique ksats mttl");
  let client = new Client(pgconn)
  client.connect()
  client
    .query(get_ksats_mttl)
    .then(result => {
      console.log(result),
        res.json(result)
        return
    })
    .catch(e => console.error(e.stack))
    .then(() => client.end())
})

app.get('/list_tables', function (req, res) {
  console.log("list tables in mttl DB ");
  let client = new Client(pgconn)
  client.connect()
  client
    .query(list_tables)
    .then(result => {
      console.log(result),
        res.json(result)
        return
    })
    .catch(e => console.error(e.stack))
    .then(() => client.end())
})
//#                    _   _     
//#   ___   __ _ _   _| |_| |__  
//#  / _ \ / _` | | | | __| '_ \ 
//# | (_) | (_| | |_| | |_| | | |
//#  \___/ \__,_|\__,_|\__|_| |_|
//#                              

app.get("/oauth-submit/:code/:state", function (req, res) {
  if (req.params.state === undefined || req.params.state === ''){
    return res.status(400).json({
      status: 'error',
      error: 'cowardly refusing to request token for empty state',
    });
  } else {
    if (req.params.code !== undefined & req.params.code !== ''){
      const spawn = require("child_process").spawn;
      console.log("oauth submit retrieving code ->  ", req.params.code)
      const pyFetch = spawn('python3', ["mttl/scripts/oauth/oauth_validate.py", req.params.code, req.params.state]);
      let response = 'error'; 
      try{
      pyFetch.stdout.on('data', function(data) {
        console.log("Standard out received from Python - oauth submit:", data.toString());
          response = data.toString(); 
      });
      res.json(response);
      return 
    } catch {
      console.log ("Prevented fatal error on oauth-submit");
      return res.status(400).json({
        status: 'error',
        error: 'Prevented fatal error on oauth-submit',
      });
    }
    } else {
      return res.status(400).json({
        status: 'error',
        error: 'cowardly refusing to request token for empty code',
      });
    }
  }
})



// FOR TESTING PURPOSES ONLY !
// app.get("/bad-oauth-submit/:code/:state", function (req, res) {
//   const spawn = require("child_process").spawn;
//   console.log("retrieving ", req.params.code)
//   const pyFetch = spawn('python3', ["mttl/scripts/oauth/oauth_validate.py", req.params.code, req.params.state]);
//   pyFetch.stdout.on('data', function(data) {
//     console.log("Standard out received from Python - oauth submit:", data.toString());
//     res.json(data.toString());
//     res.json(data.toString());
//     res.json(data.toString());
//     res.json(data.toString());
//     res.status(400).json({
//       status: 'error',
//       error: 'python oauth failed - this may happen with duplicate responses',
//     });   
// });
//   pyFetch.stderr.on('data', function(data) {
//     console.log("Error received from Python:", data.toString());
//     //res.json(data.toString());
//     return res.status(400).json({
//       status: 'error',
//       error: 'python oauth failed - this may happen with duplicate responses',
//     });
// });
// })


app.get('/profileFromCookie/:cookie', function (req, res) {
  if (req.params.cookie === undefined || req.params.cookie === ''){
    return res.status(400).json({
      status: 'error',
      error: 'cowardly refusing to request token for empty cookie',
    });
  } else {
  const spawn = require("child_process").spawn;
  console.log("profile from cookie retrieving  ", req.params.cookie)
  const pyFetch = spawn('python3', ["mttl/scripts/oauth/get_profile_from_cookie.py", req.params.cookie]);
  pyFetch.stdout.on('data', function (data) {
    console.log("Standard out received from Python - profile from cookie:", data.toString());
    //res.json(data.toString());
    response = JSON.parse(JSON.stringify(data.toString()));
   return  res.json(response)
  });
  // pyFetch.stderr.on('data', function (data) {
  //   console.log("Error received from Python:", data.toString());
  //   return res.status(400).json({
  //     status: 'error',
  //     error: 'python oauth failed - this may happen with duplicate responses',
  //   });
  // });
}
})


app.get('/username/:cookie', async function (req, res) {
  let cookie = req.params.cookie
  console.log("serverJS - profile info from DB ");
  console.log("cookie ID ", cookie)
  let client = new Client(gitlabconn)
  const query = "SELECT nickname FROM profile WHERE cookie= $1"
  const data = [cookie];
  await client.connect()
  await client
    .query(query, data)
    .then(result => {
      console.log("serverJS - profile queried for ", cookie),
      res.json(result)
      return
    })
    .catch(e => 
      {console.error(e.stack)
       console.log("serverJS - Profile not found ")
       console.log(e)
       return
      }
      )
    .then(() => client.end())
    return
})


app.get('/profile/:cookie', function (req, res) {
  let cookie = req.params.cookie
  console.log("serverJS - profile info from DB ");
  console.log("cookie ID ", cookie)
  let client = new Client(gitlabconn)
  const query = "SELECT name FROM profile WHERE cookie= $1"
  const data = [cookie];
  client.connect()
  client
    .query(query, data)
    .then(result => {
      console.log("serverJS - profile queried for ", cookie),
      res.json(result)
      return
    })
    .catch(e => 
      {console.error(e.stack)
       console.log("serverJS - Profile not found ")
       console.log(e)
       return
      }
      )
    .then(() => client.end())
})

app.get('/okstatus/:cookie', function (req, res) {
  const spawn = require("child_process").spawn;
  console.log("retrieving okness", req.params.code)
  const pyFetch = spawn('python3', ["mttl/scripts/oauth/get_ok_from_cookie.py", req.params.cookie]);
  pyFetch.stdout.on('data', function (data) {
    console.log("Standard out received from Python - ok from cookie:", data.toString());
    //res.json(data.toString());
    response = JSON.parse(JSON.stringify(data.toString()));
   return res.json(response)
  });
  pyFetch.stderr.on('data', function (data) {
    console.log("Error received from Python:", data.toString());
    return res.status(400).json({
      status: 'error',
      error: 'python oauth failed - this may happen with duplicate responses',
    });
  });
})


app.get('/goodbye/:cookie', function (req, res) {
  const spawn = require("child_process").spawn;
  console.log("retrieving goodbye ", req.params.cookie)
  const pyFetch = spawn('python3', ["mttl/scripts/oauth/remove_based_on_cookie.py", req.params.cookie]);
  pyFetch.stdout.on('data', function (data) {
    console.log("Standard out received from Python - goodbye:", data.toString());
    //res.json(data.toString());
    response = JSON.parse(JSON.stringify(data.toString()));
    return res.json(response)
  });
  // pyFetch.stderr.on('data', function (data) {
  //   console.log("Error received from Python goodbye:", data.toString());
  //   return res.status(400).json({
  //     status: 'error',
  //     error: 'python oauth failed - this may happen with duplicate responses',
  //   });
  // });
}); 


app.get('/farewell/:cookie', function (req, res) {
  if (req.params.cookie === undefined || req.params.cookie === ''){
    return res.status(400).json({
      status: 'error',
      error: 'cowardly refusing to say farewell to empty cookie',
    });
  } else {
  const spawn = require("child_process").spawn;
  console.log("retrieving farewell ", req.params.cookie)
  const pyFetch = spawn('python3', ["mttl/scripts/oauth/logout_user_from_db.py", req.params.cookie]);
  pyFetch.stdout.on('data', function (data) {
    console.log("Standard out received from Python - farewell:", data.toString());
    //res.json(data.toString());
    response = JSON.parse(JSON.stringify(data.toString()));
    return res.json(response)
  });
  // pyFetch.stderr.on('data', function (data) {
  //   console.log("Error received from Python farewell:", data.toString());
  //   return res.status(400).json({
  //     status: 'error',
  //     error: 'python oauth failed - this may happen with duplicate responses',
  //   });
  // });
}
}); 
//  Gitlab postgres
//#        _ _   _       _     
//#   __ _(_) |_| | __ _| |__  
//#  / _` | | __| |/ _` | '_ \ 
//# | (_| | | |_| | (_| | |_) |
//#  \__, |_|\__|_|\__,_|_.__/ 
//#  |___/                     

// switching databases requires a different connection

app.get('/glpgctt', function (req, res) {
  console.log("postgres client time test - gitlab");
  let client = new Client(gitlabconn)
  client.connect()
  client
    .query('SELECT NOW()')
    .then(result => {
      console.log(result),
        res.json(result)
        return
    })
    .catch(e => console.error(e.stack))
    .then(() => client.end())
})

app.get('/getgit', function (req, res) {
  console.log("Retrieving gitlab info");
  let client = new Client(gitlabconn)
  client.connect()
  client
    .query(get_gitlab_tokens)
    .then(result => {
      console.log(result),
        res.json(result)
        return
    })
    .catch(e => console.error(e.stack))
    .then(() => client.end())
})

app.get('/glcols', function (req, res) {
  console.log("column names gitlab");
  let client = new Client(gitlabconn)
  client.connect()
  client
    .query(get_cols_ac)
    .then(result => {
      console.log(result),
        res.json(result)
        return
    })
    .catch(e => console.error(e.stack))
    .then(() => client.end())
})

app.get('/profileExists/:cookie', function (req, res) {
  console.log("serverJS checking if Profile exists -> ", req.params.cookie )
  let client = new Client(gitlabconn)
  let sql  = "SELECT name FROM profile WHERE cookie = $1"
  let exists = false; 
  client.connect()
  client
    .query(sql, [req.params.cookie], )
    .then(result => {
        console.log("server js exists gitprofile -> ", result)
        console.log("rowcount ", result.rows[0]['rowCount'])
        if (result.length < 1){
          console.log("name not found")
          exists = false; 
        } else {
          console.log("name found")
          exists = true; 
        }
        // res.setHeader('content-type', 'text/plain'),
        // res.send(result.rows[0]['access_token'])
        res.send(exists)
        return
    })
    .catch(e => 
      {console.error(e.stack)
       console.log("serverJS - Profile not found ")
       console.log(e)
      }
      )
    .then(() => client.end())
    return
})


app.get('/gitprofile/:cookie', function (req, res) {
  console.log("serverJS getting profile -> ", req.params.cookie )
  console.log("git profile");
  let client = new Client(gitlabconn)
  let sql  = "SELECT name FROM profile WHERE cookie = $1"
  client.connect()
  client
    .query(sql, [req.params.cookie], )
    .then(result => {
        console.log("server js gitprofile -> ", result)
        console.log(result.rows[0]['name']),
        // res.setHeader('content-type', 'text/plain'),
        // res.send(result.rows[0]['access_token'])
        res.send(result)
        return
    })
    .catch(e => 
      {console.error(e.stack)
       console.log("serverJS - Profile not found ")
       console.log(e)
      }
      )
    .then(() => client.end())
    return
})

app.get('/gittoken/:cookie', function (req, res) {
  console.log("parsing state -> ", req.params.cookie )
  console.log("git access token");
  let client = new Client(gitlabconn)
  let sql  = "SELECT access_token FROM gitlab WHERE cookie = $1"
  client.connect()
  client
    .query(sql, [req.params.cookie], )
    .then(result => {
        console.log("result from server js gittoken ", result)
        console.log(result.rows[0]['access_token']),
        // res.setHeader('content-type', 'text/plain'),
        // res.send(result.rows[0]['access_token'])
        res.send(result)
        return
    })
    .catch(e => console.error(e.stack))
    .then(() => client.end())
    return
})


// Server JS Functions 
//#                                  _ ____  
//#  ___  ___ _ ____   _____ _ __   | / ___| 
//# / __|/ _ \ '__\ \ / / _ \ '__|  | \___ \ 
//# \__ \  __/ |   \ V /  __/ | | |_| |___) |
//# |___/\___|_|    \_/ \___|_|  \___/|____/ 
//#                                          

app.get("/checkUrl/:url(*)", function (req, res) {
  var url = req.params.url;
  if (!(url.startsWith("http://") || url.startsWith("https://"))) {
    url = "http://" + url;
  }
  urlExists(url).then(result => {
    res.send(result)
    return
  })
})

app.get("/checkUrlSafety/:url(*)", function (req, res) {
  var url = req.params.url;
  console.log("server js checking safety of ", url)
  if (url === undefined || url === ''){
    return res.status(400).json({
      status: 'error',
      error: 'cowardly refusing to check safety of empty url',
    });
  } else {
    if (url !== undefined & url !== ''){
      console.log("Checking Safe browsing API for  ->  ", url)
      var child = childProcess.execFile("python3", ["mttl/scripts/backend/check_link.py", url],
      function(err, stdout, stderr){
        console.log(stdout)
        console.log(err)
        console.log(stderr)
        res.json(stdout.toString());
      })
      /*const pyFetch = spawn('python3', ["mttl/scripts/backend/check_link.py", url]);
      let response = 'error'; 
      try{
      pyFetch.stdout.on('data', function(data) {
        console.log("Standard out received from Python - check link:", data.toString());
         // response = data.toString(); 
        
      return   res.json(data.toString());
      });
      
    } catch {
      console.log ("Prevented fatal error on check link");
      return res.status(400).json({
        status: 'error',
        error: 'Prevented fatal error on check link',
      });
    }*/
    } else {
      return res.status(400).json({
        status: 'error',
        error: 'cowardly refusing to check empty URL',
      });
    }
  }
 
})


app.get("/modifyKSAT/:ksat/:desc/:token", async function (req, res) {
  const username = await usernameFromCookie(req.params.token);
  if (username.length > 0) {
    console.log ("KSAT modification submitted from: ", username);
    var child = await childProcess.execFile('python3', ["mttl/scripts/backend/modify_ksat.py", req.params.ksat, req.params.desc, username],
      function(err, stdout, stderr){
        res.send(stdout.toString());
        //console.log("Stdout pulled from function:", stdout);
        console.log(stdout);
        console.log(err);
        console.log(stderr);
      });
    console.log("Express JS Sent modification request");
  } else {
    console.log("Modification not authorized");
    res.status(401).send("Unauthorized");
  }
})


app.get("/newLink/:ksat/:type/:token/:safety/:url(*)", async function(req, res) {
  const username = await usernameFromCookie(req.params.token);
  if (username.length > 0){
    console.log ("Ksat resource submitted from: ", username);
    var child = await childProcess.execFile("python3", ["mttl/scripts/backend/ksat_link.py", req.params.ksat, req.params.type, req.params.url, username, req.params.safety],
      function(err, stdout, stderr){
        res.send(stdout.toString());
        //will log the url to console log
        console.log(stdout);
        console.log(err);
        console.log(stderr);
      });
    console.log("Express JS Sent new link request");
    //spawn('python3', ["mttl/scripts/backend/ksat_link.py", req.params.ksat, req.params.type, req.params.url, username, req.params.safety]);
    //pull the response from here. 
    //res.send("Sent request")
  } else {
    console.log("new link unauthorized");
    res.status(401).send("Unauthorized");
  }
})

app.get("/newKsat/:name/:topic/:workrole/:owner/:source/:parents/:children/:trainingLink/:trainingRef/:evals/:desc/:token(*)",
async function (req, res) {
  console.log("express JS calling new_ksat.py")
  const username = await usernameFromCookie(req.params.token);
  if (username.length > 0) {
    console.log ("New Ksat request from: ", username)
    var child = await childProcess.execFile('python3', ["mttl/scripts/backend/new_ksat.py", req.params.name,
      req.params.topic, req.params.workrole, req.params.owner, req.params.source, req.params.parents,
      req.params.children, req.params.trainingLink, req.params.trainingRef, req.params.evals, req.params.desc, username],
      function(err, stdout, stderr){
        res.send(stdout.toString())
        console.log(err)
        console.log(stderr)
      });
    console.log("Express JS Sent new KSAT request")
    return
  } else {
    console.log("insufficient username received")
    res.status(401).send("Unauthorized")
    return
  }
})

// customize as necessary
app.get("/bugReport/:title/:desc/:token", async function(req, res){
  const username = await usernameFromCookie(req.params.token);
  if (username.length > 0) {
    console.log ("Bug report submitted from: ", username)
    var child = childProcess.execFile("python3", ["mttl/scripts/backend/bug_report.py", req.params.title, req.params.desc, username],
    function(err, stdout, stderr){
      console.log(stdout)
      console.log(err)
      console.log(stderr)
      res.send(stdout)
    })
  } else {
    res.status(401).send("Unauthorized")
  }
})

app.get("/newContact/:desc/:topic/:token/:notes(*)", async function (req, res) {
  const username = await usernameFromCookie(req.params.token);
  if (username.length > 0) {
    console.log ("Contact form submitted from: ", username)
    var child = childProcess.execFile("python3", ["mttl/scripts/backend/new_contact.py", req.params.desc, req.params.topic, username, req.params.notes],
    function(err, stdout, stderr){
      console.log(stdout)
      console.log(err)
      console.log(stderr)
      res.send(stdout)
    })
  } else {
    res.status(401).send("Unauthorized")
  }
})



app.get("/newJSON/:token/:json(*)", async function (req, res) {
  const username = await usernameFromCookie(req.params.token);
  if (username.length > 0) {
    console.log ("Module submitted from: ", username)
    const spawn = require("child_process").spawn;
    spawn('python3', ["mttl/scripts/backend/new_module.py", username, req.params.json]);
    res.send("Sent Module request")
  } else {
    res.status(401).send("Unauthorized")
  }
})

app.get("/documentation", function (req, res) {
  res.sendFile(__dirname + '/documentation/index.html');
})

app.get("/documentation/index.html", function (req, res) {
  res.redirect('/documentation');
  
})

app.get("/documentation/:file", function (req, res) {
  res.sendFile(__dirname + '/documentation/' + req.params.file);
})

app.get("/documentation/:subdirectory(*)/:file", function (req, res) {
  if (req.params.subdirectory == "documentation") {
    res.sendFile(__dirname + '/documentation/' + req.params.file);
  } else {
    res.sendFile(__dirname + '/documentation/' + req.params.subdirectory + "/" + req.params.file);
  }
})

//enabling cors just for the kumu directory
app.get("/kumu/:file", cors(), function (req, res) {
  res.sendFile(__dirname + '/kumu/' + req.params.file);
})
} catch {
  console.log("Something went wrong.")
  return
}

//MTTL.min.json call
app.get("/megajson", cors(), function (req, res) {
  res.sendFile(__dirname + '/mttl/src/data/MTTL.min.json');
})

// app.get('/kumu/kumu-data.json', (req, res) => {
//   res.sendFile('./kumu/kumu-data.json', { root: __dirname });
// });


// Default route
app.use(function (req, res) {
  res.sendFile(html + 'index.html');
})
// Start server
app.listen(port, function () {
  console.log('Port: ' + port);
  console.log('Html: ' + html);
});

