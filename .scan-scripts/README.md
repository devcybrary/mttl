### Security Scans 
This directory shall include the configuration files for the security and performance scans.
Including: 
 - DAST.gitlab-ci.yml                
 - Browser-Performance-Testing.gitlab-ci.yml  
 - Dependency-Scanning.gitlab-ci.yml   
 - SAST.gitlab-ci.yml
- Container-Scanning.gitlab-ci.yml  // this is not used - it is configured in gitlab-ci.yml
- License-Scanning.gitlab-ci.yml             
- Secret-Detection.gitlab-ci.yml    // this is not used - it is configured in gitlab-ci.yml