#!/bin/bash

RED='\033[0;31m'
ORANGE='\033[0;33m'
PURPLE='\033[0;35m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
tput setaf 2
tput setab 0
echo " ____        _ _     _   __  __ _____ _____ _     "
echo "| __ ) _   _(_) | __| | |  \/  |_   _|_   _| |    "
echo "|  _ \| | | | | |/ _\` | | |\/| | | |   | | | |    "
echo "| |_) | |_| | | | (_| | | |  | | | |   | | | |___ "
echo "|____/ \__,_|_|_|\__,_| |_|  |_| |_|   |_| |_____|"
echo "                                                  "
tput sgr0


# Preventing port in use error
printf "\n${ORANGE}Looking for and shutting down all Node servers\n"
printf "\n${RED}WARNING: This script is meant to be run in a Docker container\n"
printf "\n${PURPLE}All Node servers will be shut down!\n"
for i in $(ps -aux | grep "node server.js" | awk '{ print $2 }')
do
    kill $i
done
# Run data scripts
tput dim 
bash ./mttl/scripts/before_script.sh
printf "${NC}Creating temporary database\n"
cp mttl/database mttl/database_tmp -r

echo "Generating KSAT links and children"
python3 ./mttl/scripts/pre_build/create_mttl_children.py
python3 ./mttl/scripts/pre_build/create_mttl_links.py

echo "Exporting mongo"
bash ./mttl/scripts/after_script.sh

echo "Generating datasets"
echo "Generating mttl"
python3 ./mttl/scripts/build/create_mttl_dataset.py
echo "Generating TTL"
python3 ./mttl/scripts/build/create_ttl_dataset.py
echo "Generating extra datasets"
python3 ./mttl/scripts/build/create_extra_datasets.py
echo "Generating metrics"
python3 ./mttl/scripts/build/create_metrics.py
echo "Generating IDF"
python3 ./mttl/scripts/build/create_IDF_dataset.py
echo "Generating roadmap"
python3 ./mttl/scripts/build/roadmap_data.py
cp roadmap.json mttl/src/data/

echo "Creating Autocomplete Data"
python3 ./mttl/scripts/pre_build/create_autocomplete.py

echo "Restoring database"
rm -rf mttl/database
cp -r mttl/database_tmp mttl/database
rm -rf mttl/database_tmp

echo "Postgres"
bash mttl/scripts/postgres/import_mttl_to_local_postgres.sh

echo "Creating Gitlab DB"
export DATABASE_URL="postgres://mttl_user@postgres:5432/mttl"
export PGCONN="postgres://mttl_user@postgres:5432/mttl"
export GLCONN="postgres://mttl_user@postgres:5432/gitlab"
python3 ./mttl/scripts/oauth/create_gitlab_db.py 

tput smso
printf "${RED} Note - to use oauth - you will need to set the PART_TWO environment variables:\n"

printf "${ORANGE}  e.g. ${PURPLE} export PART_TWO='your part two'\n"
[[ ! -z "$PART_TWO" ]] && printf "${GREEN}PART_TWO provided\n" || printf "${RED} please - export PART_TWO \n"

# export REDIRECT_URI='http%3A%2F%2Flocalhost%3A5000%2Fredirect'
export REDIRECT_URI='http%3A%2F%2Flocalhost%3A5000%2Fmttl'


echo "Building Angular"
# Build node
cd mttl
export NG_CLI_ANALYTICS=ci
# more options to disable prompt here: https://stackoverflow.com/questions/56355499/stop-angular-cli-asking-for-collecting-analytics-when-i-use-ng-build
npm install
ng build

cd ..

echo "Starting expressJS server"
npm install
npm audit fix
npm start &
sleep 5

# export redirect_uri='http%3A%2F%2Flocalhost%3A4200%2Fredirect'
[[ ! -z "$PART_TWO" ]] && printf "${GREEN}PART_TWO provided\n" || printf "${RED} Oauth will not work\n${PURPLE}Please - ${RED}export PART_TWO \n"
[[ ! -z "$ROBOTOKEN" ]] && printf "${GREEN}ROBOTOKEN provided\n" || printf "${RED} Oauth will not work\n${PURPLE}Please - ${RED}export ROBOTOKEN \n"
[[ ! -z "$GOOGLE_TOKEN" ]] && printf "${GREEN}GOOGLE_TOKEN provided\n" || printf "${RED} Link safety checks will not work\n${PURPLE}Please - ${RED}export GOOGLE_TOKEN \n"
echo "You may also want to export ROBOTOKEN= for checking statuses"
echo "or GOOGLE_TOKEN= for testing link safety"

printf "${GREEN}Frontend built!\n"
printf "${ORANGE}Visit http://localhost:5000\n"
tput sgr0