Please make sure to search the MTTL prior to submitting a new KSAT.
For best practices concerning KSAT creation, see <here>

### Why does the requirement(s) need to be added?:
Further defining the job
Adding a new Item to the Job
I'm a program owner and need people trained on something
I want to further refine a parent KSAT

### KSAT Description(s):
This is a single sentence description of the requirement.  It's helpful to think of having "After training the student should be able to... **put your language here**"

Examples
"List the flight commanders in the 90th." OR
"Write pseudocode to solve a programming problem."

### Related KSATs
Does this KSAT relate to any others?

Parent KSATs: Please list KSATs that this KSAT helps to achieve below.

Child KSAT: Please list KSAT's that help implement this KSAT below.

### Training Reference (optional)
Do you have one or more references which explains how to accomplish this correctly?
e.g. 90COS Org Chart (Reference to a list of flight commanders)

### Workrole alignment
Which work roles does this requirement apply to?:



### Related Requirements/Documentation
Is this requirement driven by an external document such as an AFI, Law, etc  If so, please list it/them.
e.g. 17-202v2 says all SEE's must be trained on objectivity.


/label ~customer ~"mttl::ksat" ~"office::CYT" ~"CYT::MTTL" ~"backlog::idea"
/milestone %"CYT Backlog"
/confidential
