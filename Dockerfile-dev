FROM ubuntu:20.04

# Avoid warnings by switching to noninteractive
ENV DEBIAN_FRONTEND=noninteractive

ARG USERNAME=node
ARG HOME=/home/$USERNAME
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN groupadd --gid $USER_GID $USERNAME \
    && useradd -s /bin/bash --uid $USER_UID -g $USERNAME --create-home $USERNAME

#RUN echo virtualbox-ext-pack virtualbox-ext-pack/license select true | debconf-set-selections

#virtualbox
#virtualbox virtualbox-ext-pack
#  && apt-get -y install postgresql-client \

RUN apt-get clean && apt-get -y update \
    && apt-get -y install --no-install-recommends apt-utils iputils-ping 2>&1 \
    && apt-get -y install sudo python3-dev python3-pip git curl wget mongodb-clients apt-transport-https \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

RUN sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt focal-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
RUN sudo apt-get update -y 
# If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':
RUN sudo apt-get -y install postgresql-client

# Kubectl, Kompose and helm
#RUN curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
#RUN sudo touch /etc/apt/sources.list.d/kubernetes.list 
#RUN echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
#RUN sudo apt-get update
#RUN sudo apt-get install -y kubectl
#RUN curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.28.2/minikube-linux-amd64
#RUN chmod +x minikube && sudo mv minikube /usr/local/bin/
#RUN echo "curling get_helm.sh" 
#RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
#RUN chmod +x get_helm.sh
#RUN bash get_helm.sh
#RUN curl -L https://github.com/kubernetes-incubator/kompose/releases/download/v0.7.0/kompose-linux-amd64 -o kompose
#RUN chmod +x kompose
#RUN sudo mv ./kompose /usr/local/bin/kompose

RUN curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash - \
    && sudo apt install -y nodejs
USER $USERNAME
WORKDIR $HOME

ENV PATH=node_modules/.bin:$HOME/node_modules/.bin:$HOME/.npm-global/bin:$HOME/.local/bin/:$PATH

RUN mkdir ~/.npm-global \
    && npm config set prefix $HOME/.npm-global

COPY requirements.txt .
RUN pip3 install -r requirements.txt

RUN curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash -o ~/.git-completion.bash && \
    echo "if [ -f ~/.git-completion.bash ]; then . ~/.git-completion.bash; fi" >> ~/.bashrc

ENV DEBIAN_FRONTEND=interactive
ENV SHELL /bin/bash
